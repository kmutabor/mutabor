<?php

/* @var $this \yii\web\View */
use yii\helpers\Html;

/* @var $content string */
//echo Url::to(['post/index']);
?>
<!--
TABLE TO FIX IE/6 ISSUE
The one where 23% margin + 100% table = 123% in IE6 o_O
-->

<table cellspacing="0" cellpadding="0" width='100%'>
    <tr>
        <td id="ucpmenu" valign="top">
            <div class='maintitle'>Меню</div>
            <!--IBF.TSLLINKS-->
            <!-- Topic Tracker -->
            <div class='pformstrip'>Мои подписки</div>
            <p>
                &middot; <?= Html::a('Темы',['user/themes']);?><br />
                &middot; <?= Html::a('Форумы',['user/forums']);?><br />
            </p>
            <!-- Profile -->
            <div class='pformstrip'>Ваш профиль</div>
            <p>
                &middot; <?= Html::a('Личные данные',['user/change-info']);?><br />
                &middot; <?= Html::a('Редактировать подпись',['user/change-subscribtion']);?><br />
                &middot; <?= Html::a('Настройки аватара',['user/change-avatar']);?><br />
                &middot; <?= Html::a('Изменить фотографию',['user/change-photo']);?><br />
            </p>
            <!-- Options -->
            <div class='pformstrip'>Настройки</div>
            <p>
                <!--IBF.OPTION_LINKS-->
                &middot; <?= Html::a('Настройки e-mail',['user/change-email-settings']);?><br />
                &middot; <?= Html::a('Настройки форума',['user/change-forum-settings']);?><br />
                &middot; <?= Html::a('Изменить e-mail адрес',['user/change-email']);?><br />
                &middot; <?= Html::a('Изменить пароль',['user/change-password']);?><br />
            </p>
        </td>
        <td style='padding:2px'><!-- --></td>
        <!-- Start main CP area -->
        <td id="ucpcontent" valign="top">
            <div class="maintitle">Добро пожаловать в Вашу Панель управления </div>
            <?php
                echo $content;
            ?>
        </td>

    </tr>
</table>
