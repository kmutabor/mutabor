<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\assets\FontAwesomeAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;

FontAwesomeAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="Сайт ни о чем.">
    <meta name="keywords"
          content="логика, спор, искусство, диалектика, вселенная, силлогизм, гроб, дельфин, летов, гражданская, оборона,"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="scroll-top" onclick="scrollA();return false;">
    <div class="scroll-top"></div>
</div>
<div class="wrap">
    <?= Html::a('<i class="fa fa-bug fa-4x" aria-hidden="true"></i>', Url::to(['site/only-random']),
        ['class' => 'main_bug']); ?>
    <?php
    NavBar::begin([
        'brandLabel' => 'Мутабор',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [
//            ['label' => 'Регистрация', 'url' => ['/user/registration'], 'visible'=> Yii::$app->user->isGuest,],
//            Yii::$app->user->isGuest ?
//                ['label' => 'Login', 'url' => ['/site/login']] :
//                [
//                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
//                    'url' => ['/site/logout'],
//                    'linkOptions' => ['data-method' => 'post'],
//                ],
            ['label' => '<i class="fa fa-cube" aria-hidden="true"></i>', 'url' => ['/site/kidala'],],
            ['label' => '<i class="fa fa-paw" aria-hidden="true"></i>', 'url' => ['/site/menschheit'],],
            ['label' => '<i class="fa fa-space-shuttle" aria-hidden="true"></i>', 'url' => ['/site/player'],],
            ['label' => '<i class="fa fa-diamond" aria-hidden="true"></i>', 'url' => ['/site/fwgnl'],],
            ['label' => '<i class="fa fa-object-group" aria-hidden="true"></i>', 'url' => ['/logic/uchebnik'],],
            ['label' => '<i class="fa fa-fire" aria-hidden="true"></i>', 'url' => ['/logic/spor'],],
//            ['label' => '<i class="fa fa-child" aria-hidden="true"></i>', 'url' => ['/logic/spor'],],

//            ['label' => 'ЛК', 'url' => ['/user/settings'], 'visible'=> !Yii::$app->user->isGuest,],

            ['label' => '<i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i>', 'url' => ['/logic/sillogism'],],
//            ['label' => '<i class="fa fa-bug" aria-hidden="true"></i>', 'url' => ['/site/bug'],],
            ['label' => '<i class="fa fa-bug" aria-hidden="true"></i>', 'url' => ['/site/only-random'],],
//            ['label' => '<i class="fa fa-bomb" aria-hidden="true"></i>', 'url' => ['/site/random','id'=>'latin_aphorisms'],],
//            ['label' => '<i class="fa fa-graduation-cap" aria-hidden="true"></i>', 'url' => ['/site/random','id'=>'latin_aphorisms'],],
//            ['label' => '<i class="fa fa-fire" aria-hidden="true"></i>', 'url' => ['/site/random','id'=>'latin_aphorisms'],],


            ['label' => '<i class="fa fa-puzzle-piece" aria-hidden="true"></i>', 'url' => ['/site/puzzle'],],
            [
                'label' => '<i class="fa fa-braille" aria-hidden="true"></i>',
                'url' => ['/site/random', 'id' => 'latin_aphorisms'],
            ],
//            ['label' => 'Ингридиенты', 'url' => [''],'visible'=> Yii::$app->user->can('admin'),
//                'items' => [
//                    ['label' => 'Добавить', 'url' => ['/nsign/component/create']],
//                    ['label' => 'Активация/Деактивация', 'url' => ['/nsign/component/view-all']],
//                    ['label' => 'Таблица/поиск', 'url' => ['/nsign/component/index']],
//                ]
//            ],
//            ['label' => 'Блюда', 'url' => [''],'visible'=> Yii::$app->user->can('admin'),
//                'items' => [
//                    ['label' => 'Добавить', 'url' => ['/nsign/course/create']],
//                    ['label' => 'Посмотреть все', 'url' => ['/nsign/course/index']],
//                ]
//            ],
//            ['label' => 'Поиск блюд', 'url' => ['/nsign/course/filt'], 'visible'=> !Yii::$app->user->isGuest],
//            ['label' => 'Миграции', 'url' => [''],'visible'=> Yii::$app->user->can('admin'),
//                'items' => [
//                    ['label' => 'Накатить', 'url' => ['/site/migrate-up']],
//                    ['label' => 'Откатить', 'url' => ['/site/migrate-down']],
//                ]
//            ],
//            ['label' => '<i class="fa fa-key" aria-hidden="true"></i>', 'url' => ['/site/contact'],],
            ['label' => 'О', 'url' => ['/site/about'],],

        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; mutabor <?= date('Y') ?></p>

        <p class="pull-left">

        </p>

        <p class="pull-left">
            <!-- counter.1gb.ru -->
            <script language="javascript" type="text/javascript">
                cgb_js = "1.0";
                cgb_r = "" + Math.random() + "&r=" +
                escape(document.referrer) + "&pg=" +
                escape(window.location.href);
                document.cookie = "rqbct=1; path=/";
                cgb_r += "&c=" +
                (document.cookie ? "Y" : "N");
            </script>
            <script language="javascript1.1" type="text/javascript">
                cgb_js = "1.1";
                cgb_r += "&j=" +
                (navigator.javaEnabled() ? "Y" : "N")</script>
            <script language="javascript1.2" type="text/javascript">
                cgb_js = "1.2";
                cgb_r += "&wh=" + screen.width +
                'x' + screen.height + "&px=" +
                (((navigator.appName.substring(0, 3) == "Mic")) ?
                    screen.colorDepth : screen.pixelDepth)</script>
            <script language="javascript1.3" type="text/javascript">
                cgb_js = "1.3"</script>
            <script language="javascript"
                    type="text/javascript">cgb_r += "&js=" + cgb_js;
                document.write("<a href='http://www.1gb.ru?cnt=92873'>" +
                "<img src='http://counter.1gb.ru/cnt.aspx?" +
                "u=92873&" + cgb_r +
                "&' border=0 width=88 height=31 " +
                "alt='1Gb.ru counter'><\/a>")</script>
        <noscript><a href='http://www.1gb.ru?cnt=92873'>
                <img src="http://counter.1gb.ru/cnt.aspx?u=92873"
                     border=0 width="88" height="31" alt="1Gb.ru counter"></a>
        </noscript>
        <!-- /counter.1gb.ru -->
        </p>
        <p>
            <!-- Yandex.Metrika informer -->
            <a href="https://metrika.yandex.ru/stat/?id=37956070&amp;from=informer"
               target="_blank" rel="nofollow"><img
                    src="https://informer.yandex.ru/informer/37956070/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                    style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                    title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                    onclick="try{Ya.Metrika.informer({i:this,id:37956070,lang:'ru'});return false}catch(e){}"/></a>
            <!-- /Yandex.Metrika informer -->

            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function () {
                        try {
                            w.yaCounter37956070 = new Ya.Metrika({
                                id: 37956070,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true,
                                webvisor: true
                            });
                        } catch (e) {
                        }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else {
                        f();
                    }
                })(document, window, "yandex_metrika_callbacks");
            </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/37956070" style="position:absolute; left:-9999px;" alt=""/></div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
        </p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
