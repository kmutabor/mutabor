<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(); ?>



    <div class="pformstrip">Изменение e-mail адреса</div>
    <p><b>Ваш текущий e-mail адрес:</b> fatrominov@gmail.com<br><br>ПРИМЕЧАНИЕ: После изменения e-mail адреса, Вы должны будете переактивировать Ваш аккаунт.<br>На Ваш e-mail адрес будет отправлено письмо с подробными инструкциями о дальнейших действиях.<br><br>Вы должны будете переавторизоваться. Настоятельно рекомендуем Вам, перед изменением e-mail адреса, воспользоваться функцией восстановления пароля, если Вы его не помните.</p>
    <table width="100%" style="padding:6px">
        <tbody><tr>
            <td width="40%"><strong>Введите новый e-mail адрес</strong></td>
            <td align="left"><input type="text" name="in_email_1" value="" class="forminput"></td>
        </tr>
        <tr>
            <td><strong>Повторите новый e-mail адрес</strong></td>
            <td align="left"><input type="text" name="in_email_2" value="" class="forminput"></td>
        </tr>
        <tr>
            <td><strong>Ваш текущий пароль</strong></td>
            <td align="left"><input type="password" name="password" value="" class="forminput"></td>
        </tr>
        </tbody></table>
    <div class="pformstrip">Код безопасности</div>
    <table width="100%" style="padding:6px">
        <tbody><tr>
            <td width="40%"><b>Ваш уникальный код безопасности</b><br>Если Вы не видите никакие числа или видите разбитые изображения, обратитесь к Администратору сайта для устранения проблемы.</td>
            <td>
                <input type="hidden" name="regid" value="f041cd17013ab812a3c1e5bcc4b1fe32">
                <img src="http://www.yaplakal.com/?act=UserCP&amp;CODE=show_image&amp;rc=f041cd17013ab812a3c1e5bcc4b1fe32" border="0" alt="Code Bit">
            </td>
        </tr>
        <tr>
            <td width="40%"><b>Подтвердите код безопасности</b><br>Для гарантии Вашей безопасности, введите 6 числовых знаков кода, отображённых выше в форме изображения.<br>Примечание: Это только в численном формате, к примеру '0' является нулём, а не буквой 'O'.</td>
            <td><input type="text" size="32" maxlength="32" name="reg_code" class="forminput"></td>
        </tr>
        </tbody></table>
    <div align="center" class="pformstrip"><?= Html::submitButton('Сохранить изменения', ['class' => 'forminput']) ?></div>


<?php ActiveForm::end(); ?>