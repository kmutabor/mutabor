        <?php
        use yii\helpers\Html;
        use yii\widgets\ActiveForm;


        $form = ActiveForm::begin(); ?>

    <div class="pformstrip">Необходимая Информация, эта секция должна быть заполнена</div>
            <table width="100%">
                <tbody>
                <tr>

                    <td class="pformleft" valign="top"><b>Фамилия</b><br></td>
                    <td class="pformright">
                <?= $form->field($model, 'sur_name')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                    </td>
                </tr>
                <tr>

                    <td class="pformleft" valign="top"><b>Имя</b><br></td>
                    <td class="pformright">
                        <?= $form->field($model, 'name')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                    </td>
                </tr>
                <tr>

                    <td class="pformleft" valign="top"><b>Отчество</b><br></td>
                    <td class="pformright">
                        <?= $form->field($model, 'mid_name')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                    </td>
                </tr>
                <tr>

                    <td class="pformleft" valign="top"><b>Телефон</b><br></td>
                    <td class="pformright">

                <?= $form->field($model, 'phone')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                    </td>
                </tr>
                <tr>

                    <td class="pformleft" valign="top"><b>Дополнительный e-mail</b><br></td>
                    <td class="pformright">
                <?= $form->field($model, 'advanced_email')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                    </td>
                </tr>
                <tr>

                    <td class="pformleft" valign="top"><b>Половая принадлежность</b><br></td>
                    <td class="pformright">
                        <?= $form->field($model, 'sex')->dropDownList(['m'=>'Мужчина','f'=>'Женщина','u'=>'Не сообщаю',],['class'=>'forminput',])->label(false) ?>
                        </td>
                </tr> </tbody></table>
            <div class="pformstrip">Дополнительная секция Вашего профиля</div>
            <table width="100%">
                <!--{MEMBERTITLE}-->
                <tbody><tr>
                    <td class="pformleft"><b>Дата рождения</b></td>
                    <td class="pformright">
                        <?= $form->field($model, 'birth_day')->widget(\yii\jui\DatePicker::classname(), [
                            'model'=>$model,
                            'attribute'  => 'birth_day',
                            'dateFormat' => 'dd/MM/yyyy',
                            'clientOptions' =>[
                                'value'  => '',
                                'language' => 'ru',
                                'changeMonth'=> true,
                                'changeYear'=> true,
                                'yearRange'=> '1945:+1',
                                'class'=>'forminput',
                                'showAnim'=>'fade',
                            ],
                            'options' => [
                                'class'=>'forminput',
                            ],
                        ])->label(false);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="pformleft"><b>Адрес Вашего вебсайта</b></td>
                    <td class="pformright">
                        <?= $form->field($model, 'web')->textInput(['placeholder'=>'http://mySite.home','class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                </tr>
                <tr>
                    <td class="pformleft"><b>Номер ICQ</b></td>
                    <td class="pformright">
                        <?= $form->field($model, 'ICQ')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'20',])->label(false) ?>
                    </td>
                </tr>
                <tr>
                    <td class="pformleft"><b>Место жительства</b></td>
                    <td class="pformright">
                        <?= $form->field($model, 'city')->textInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'20',])->label(false) ?>
                    </td>
                </tr>
                <tr>
                    <td class="pformleft" valign="top"><b>Ваши увлечения</b></td>
                    <td class="pformright">
                        <?= $form->field($model, 'hobby')->textarea(['class'=>'forminput', 'rows'=>'10', 'cols'=>'60',])->label(false) ?>
                    </td>
                </tr>

                <tr>
                    <td class="pformleft" valign="top"><b>Откуда вы о нас узнали</b><br></td>
                    <td class="pformright">
                        <?= $form->field($model, 'about_us')->textarea(['placeholder'=>'от коллеги', 'class'=>'forminput', 'cols'=>'60', 'rows'=>'5',])->label(false) ?>
                    </td>
                </tr>
                <tr>
                    <td class="pformstrip" align="center" colspan="2">
                        <?= Html::submitButton('Сохранить изменения', ['class' => 'forminput']) ?>
                    </td>
                </tr>
                </tbody>
            </table>


<!--</div>-->
    <?php ActiveForm::end(); ?>