<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Регистрация';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-registration">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_registration', [
        'model' => $model,
    ]) ?>

</div>
