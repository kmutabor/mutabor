<?php

use app\assets\UserAsset;

UserAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */

//$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@app/views/layouts/user_lk.php');
?>
<?= $this->render('_user_password_form', [
    'model' => $model,
    'model_new1' => $model_new1,
    'model_new2' => $model_new2,
]) ?>
<?php $this->endContent(); ?>
