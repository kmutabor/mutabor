<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$session = Yii::$app->session;
$success_msg = $session->getFlash('success_msg');
$error_msg = $session->getFlash('error_msg');
if(isset($success_msg)){
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => $success_msg,
]);
}else if(isset($error_msg)){
    echo Alert::widget([
        'options' => ['class' => 'alert-danger'],
        'body' => $error_msg,
    ]);
}
$form = ActiveForm::begin(); ?>



        <div class="pformstrip">Ваш пароль</div>
        <p>Здесь Вы можете изменить Ваш пароль. <br>
            <br>После успешного изменения пароля, форум попытается обновить Ваш сеанс. При возникновении каких-либо проблем, попробуйте переавторизоваться. Если ничего не получится, обратитесь к Администратору сайта для разрешения проблемы.</p>

        <table width="100%" style="padding:6px">
            <tbody><tr>
                <td><b>Введите Ваш ТЕКУЩИЙ пароль</b></td>
                <td>
                    <?= $form->field($model,'[p]password')->passwordInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                </td>
            </tr>
            <tr>
                <td><b>Введите Ваш НОВЫЙ пароль (не менее 8 символов)</b></td>
                <td>
                    <?= $form->field($model_new1,'[p1]password')->passwordInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                </td>
            </tr>
            <tr>
                <td><b>Повторите Ваш НОВЫЙ пароль</b></td>
                <td>
                    <?= $form->field($model_new2,'[p2]password')->passwordInput(['class'=>'forminput', 'size'=>'40', 'maxlength'=>'1200',])->label(false) ?>
                </td>
            </tr>
            </tbody></table>
        <div align="center" class="pformstrip">
            <?= Html::submitButton('Изменить пароль', ['class' => 'forminput']) ?>
        </div>


<?php ActiveForm::end(); ?>
