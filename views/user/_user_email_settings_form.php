<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin(); ?>

    <div class="pformstrip">Скрытые настройки</div>
    <br>
    <table width="100%">
        <tbody><tr>
            <td align="right" valign="top">
                <?= $form->field($model, 'hide_email')->checkbox(['class'=>'checkbox'], FALSE)->label(false) ?>
            </td>
            <td align="left" width="100%"><b>Скрыть мой адрес e-mail от других пользователей</b><br>При включении этой функции, пользователи форума не смогут отправлять Вам письма из форума.</td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <?= $form->field($model, 'admin_newsletter',['template'=>'{input}'])->checkbox(['class'=>'checkbox'], FALSE)->label(false) ?>
            </td>
            <td align="left" width="100%"><b>Сообщать мне обо всех изменениях, проводимых администратором форума</b><br>При включении этой функции, Ваш e-mail адрес будет добавлен к списку e-mail адресов Администратора и Вы, всякий раз будете уведомлены по e-mail, о любых изменениях на форуме.</td>
        </tr>
        </tbody></table>
    <br>
    <div class="pformstrip">Настройки Форума</div>
    <br>
    <table width="100%">
        <tbody><tr>
            <td align="right" valign="top">
                <?= $form->field($model, 'add_message_copy_to_answer')->checkbox(['class'=>'checkbox'], FALSE)->label(false) ?>
            </td>
            <td align="left" width="100%"><b>Добавлять копию сообщения из подписанных тем в отправляемое письмо-уведомление</b><br>При включении этой функции, копия нового сообщения, в простом текстовом формате, будет выслана Вам на e-mail.</td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <?= $form->field($model, 'pm_alert')->checkbox(['class'=>'checkbox'], FALSE)->label(false) ?>
            </td>
            <td align="left" width="100%"><b>Уведомлять меня по e-mail при получении новых личных сообщений</b><br>При включении этой функции, Вы будете уведомлены на e-mail каждый раз при получении нового Личного сообщения.</td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <?= $form->field($model, 'get_new_and_updated_themes')->checkbox(['class'=>'checkbox'], FALSE)->label(false) ?>
            </td>
            <td align="left" width="100%"><b>Включить 'E-mail Уведомление' по умолчанию?</b><br>При включении, Вы будете автоматически подписаны на все новые и обновлённые темы форума.</td>
        </tr>
        </tbody></table>
    <br>
    <div class="pformstrip" align="center"><?= Html::submitButton('Сохранить изменения', ['class' => 'forminput']) ?></div>

<?php ActiveForm::end(); ?>