<?php
use yii\helpers\Html;

$this->title = 'Краткая история человечества';
?>
<style>
    #mensch div {
        margin: 10px 0 10px;
    }
    #mensch div img{
        width: 100%;
    }
</style>
<div id="mensch" class="book_site centered site_a">
    <div>Краткая история человечества</div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/11.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/12.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/13.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/14.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/15.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/16.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/17.jpg'); ?>
    </div>
    <div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/menschheit/18.jpg'); ?>
    </div>
</div>
