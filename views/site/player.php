<?php

/* @var $this yii\web\View */


use yii\helpers\Html;

$this->title = 'join';
?>

<style>
    #playback{
        padding-bottom: 10px;
    }
    #playback i{
        cursor:pointer;
        padding: 0 10px;
    }
</style>

<div class='book_site centered'>
    <div id="playback">
        <i id="playpause" class="fa fa-pause-circle-o fa-2x" aria-hidden="true"></i>
        <i id="volumePlus" class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
        <i id="volumeMinus" class="fa fa-minus-circle fa-2x" aria-hidden="true"></i>
        <i id="mute" class="fa fa-bell-o fa-2x" aria-hidden="true"></i>
    </div>
    <?= Html::img(Yii::getAlias('@web') . '/images/site/other/kashalot.jpg'); ?>
</div>


<script type="text/javascript">
    $(function() {
        var aud = new Audio('http://mds.planeset.ru:8000/mds.mp3');

        $('#playpause').on('click', function(){
            var t = $(this);
            if(aud.paused == true){
                aud.play();
                t.removeClass('fa-play-circle-o');
                t.addClass('fa-pause-circle-o');
            }else{
                aud.pause();
                t.removeClass('fa-pause-circle-o');
                t.addClass('fa-play-circle-o');

            }
        });

        $('#mute').on('click', function(){
            var t = $(this);
            if(aud.muted == true){
                aud.muted = false;
                t.removeClass('fa-bell');
                t.addClass('fa-bell-o');
            }else{
                aud.muted = true;
                t.removeClass('fa-bell-o');
                t.addClass('fa-bell');
            }
        });

        var volumeP =  $('#volumePlus');
        volumeP.on('mousedown', function(){
            volumeP.removeClass('fa-plus-circle');
            volumeP.addClass('fa-circle');
            aud.volume += aud.volume<1?.1:0;
        });
        volumeP.on('mouseup', function(){
            volumeP.removeClass('fa-circle');
            volumeP.addClass('fa-plus-circle');
        });

        var volumeM = $('#volumeMinus');
        volumeM.on('mousedown', function(){
            volumeM.removeClass('fa-minus-circle');
            volumeM.addClass('fa-circle');
            aud.volume -= aud.volume>0?.1:0;
        });
        volumeM.on('mouseup', function(){
            volumeM.removeClass('fa-circle');
            volumeM.addClass('fa-minus-circle');
        });

        aud.volume = .5;
        aud.play();
    });
</script>