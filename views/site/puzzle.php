<?php

/* @var $this yii\web\View */

use app\components\Actions;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Puzzle';
?>
<div id="live_menu" class="book_site">
                <?php
                    $arr = Actions::$actionNames;
                    shuffle($arr);
                    foreach($arr as $action){
                ?>
                    <div>
                        &nbsp;&nbsp;<?= Html::a(Actions::getRandomLinkTitle(), Url::to(['site/random', 'id' => $action]),[]); ?>&nbsp;&nbsp;
                    </div>
                <?php
                }
                ?>
            </div>