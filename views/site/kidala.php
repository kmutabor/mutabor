<?php


$this->title = 'Андрей Кидала - Дочь Дона Кихота.';
?>
<style>
    #kidala audio{
        display: block;
        margin: 0 auto 30px;
    }
</style>
<div id="kidala" class="book_site centered site_a">
    <div>Андрей Кидала - Дочь Дона Кихота.</div>
    <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/kyLLMAomaJI" frameborder="0" allowfullscreen></iframe>
        <audio controls preload="none">
            <source src="<?= Yii::getAlias('@web') . '/audio/site/kidalaf/part1.mp3'; ?>" type="audio/mpeg">
        </audio>
    </div>

    <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/1rC5aOum_Wo" frameborder="0" allowfullscreen></iframe>
        <audio controls preload="none">
            <source src="<?= Yii::getAlias('@web') . '/audio/site/kidalaf/part2.mp3'; ?>" type="audio/mpeg">
        </audio>
    </div>


    <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/rwi9seq-j-s" frameborder="0" allowfullscreen></iframe>
        <audio controls preload="none">
            <source src="<?= Yii::getAlias('@web') . '/audio/site/kidalaf/part3.mp3'; ?>" type="audio/mpeg">
        </audio>
    </div>


    <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Ii5ucStwbrw" frameborder="0" allowfullscreen></iframe>
        <audio controls preload="none">
            <source src="<?= Yii::getAlias('@web') . '/audio/site/kidalaf/part4.mp3'; ?>" type="audio/mpeg">
        </audio>
    </div>

    <div>
        Для того, чтобы скачать видео файл или его mp3 версию, можно указать <b>to</b> в ссылке, что бы получилось,
        например, так: <b>https://www.youtubeto.com/watch?v=kyLLMAomaJI<b/>
    </div>
</div>
