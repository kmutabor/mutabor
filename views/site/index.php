<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Mutabor/Мутабор';
?>
<div class="site-index">



    <div class="body-content">

        <div class="jumbotron">
            <div id="first_p" ">
                <div>
                <?= Html::img(Yii::getAlias('@web') . '/images/site/a/moya_oborona.jpg', ['class'=>'img']); ?>
                </div>
                <div>
                <audio controls>
                    <source src="<?= Yii::getAlias('@web') . '/audio/site/a/moya_oborona.mp3'; ?>" type="audio/mpeg">
                </audio>
                </div>
            </div>
            <div id="second_p">
                <div>
                <?= Html::img(Yii::getAlias('@web') . '/images/site/a/vintovka_eto_prazdnik.jpg', ['class'=>'img']); ?>
                </div>
                <div>
                <audio controls>
                    <source src="<?= Yii::getAlias('@web') . '/audio/site/a/vintovka_eto_prazdnik.mp3'; ?>" type="audio/mpeg">
                </audio>
                </div>
            </div>

            <div>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/eG3d2H9Fc4A" frameborder="0" allowfullscreen></iframe>
            </div>

    </div>
    </div>
</div>
