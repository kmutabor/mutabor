
<?php

use yii\helpers\Html;

$this->title = 'Наука логики';
?>
		<div class="book_site centered">
            <div>Наука логики</div>
            <div>
            <?= Html::a('Том 1(pdf)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_1_Filosofskoe_nasledie_1970.pdf', ['target'=>'_blank']); ?> /
            <?= Html::a('Том 1(djvu)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_1_Filosofskoe_nasledie_1970.djvu'); ?>
            </div>
            <div>
            <?= Html::a('Том 2(pdf)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_2_Filosofskoe_nasledie_1970.pdf', ['target'=>'_blank']); ?> /
                <?= Html::a('Том 2(djvu)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_2_Filosofskoe_nasledie_1970.djvu'); ?>
            </div>
            <div>
            <?= Html::a('Том 3(pdf)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_3_Filosofskoe_nasledie_1970.pdf', ['target'=>'_blank']); ?> /
                <?= Html::a('Том 3(djvu)', Yii::getAlias('@web') . '/files/site/Gegel_G_V_F_ -_Nauka_logiki_t_3_Filosofskoe_nasledie_1970.djvu'); ?>
            </div>
        </div>