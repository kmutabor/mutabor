<?php

use app\components\Actions;
use yii\bootstrap\Html;

$this->title = Actions::getHash();
?>

<div class="book_site centered site_a">
    <?= Html::img(Yii::getAlias('@web') . '/images/site/a/poshli_vi_vse_na_huy.png'); ?>
    <audio controls autoplay>
        <source src="<?= Yii::getAlias('@web') . '/audio/site/a/poshli_vi_vse_na_huy.mp3'; ?>" type="audio/mpeg">
    </audio>
</div>
