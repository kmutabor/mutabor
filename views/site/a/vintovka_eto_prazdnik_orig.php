<?php

use app\components\Actions;
use yii\bootstrap\Html;

$this->title = Actions::getHash();
?>
<div class="book_site centered site_a">
    <?= Html::img(Yii::getAlias('@web') . '/images/site/a/vintovka_eto_prazdnik_orig.jpg'); ?>
    <audio controls autoplay>
        <source src="<?= Yii::getAlias('@web') . '/audio/site/a/vintovka_eto_prazdnik_orig.mp3'; ?>" type="audio/mpeg">
    </audio>
</div>