<?php

use app\components\Actions;
use yii\bootstrap\Html;

$this->title = Actions::getHash();
?>

<div class="book_site centered site_a">
    <?= Html::img(Yii::getAlias('@web') . '/images/site/a/vse_kak_u_ludey_fingerstyle.jpg'); ?>
    <audio controls autoplay>
        <source src="<?= Yii::getAlias('@web') . '/audio/site/a/vse_kak_u_ludey_fingerstyle.mp3'; ?>" type="audio/mpeg">
    </audio>
</div>
