<?php

use app\components\Actions;

$this->title = Actions::getHash();
?>

<div class="book_site centered site_a">
    <?= \yii\helpers\Html::img(Yii::getAlias('@web/images/site/other/alone.jpg'),
        [
            'width' => 360,
        ]
    ); ?>
    <p>Самое худшее одиночество — это остаться среди тех, кто не понимает тебя. &copy; Руми</p>

</div>
