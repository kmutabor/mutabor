<?php

use app\components\Actions;

$this->title = Actions::getHash();
?>
<style>

    dl {
        text-align: left;
        padding-left: 90px;
    }
    dt {
        padding-top: 15px;
    }

    dd {
        padding-left: 50px;
    }

    dl div {
        display: none;
    }

    h3 {
        margin-left: -20px;
        border-bottom: 1px dashed #808080;
    }

    dl a h3 {
        cursor: pointer;
        color: #7f4c80;
        width: 90%;
    }
    .colored {
        padding: 10px 0 10px 0;
        background-color: rgba(192, 224, 192, 0.45);
        border-radius: 5px;
    }
</style>
<script>
    $(document).on('ready', function () {
        $('#list a').on('click', function () {
            $(this).next('div').slideToggle('slow', 'linear');
        });
    });

</script>
<div class="book_site centered site_a">
<?= \yii\helpers\Html::img(Yii::getAlias('@web/images/site/other/latin_aphorisms.jpg'),
    [
        'width' => 560,
    ]
); ?>
<div>
<div>
    <a href="#A">A</a>
    <a href="#B">B</a>
    <a href="#C">C</a>
    <a href="#D">D</a>
    <a href="#E">E</a>
    <a href="#F">F</a>
    <a href="#G">G</a>
    <a href="#H">H</a>
    <a href="#I">I</a>
    <a href="#J">J</a>
    <a href="#L">L</a>
    <a href="#M">M</a>
    <a href="#N">N</a>
    <a href="#O">O</a>
    <a href="#P">P</a>
    <a href="#Q">Q</a>
    <a href="#R">R</a>
    <a href="#S">S</a>
    <a href="#T">T</a>
    <a href="#U">U</a>
    <a href="#V">V</a>
    <a href="#US">US</a>
</div>
<div class="colored">
<dl id="list">
<a id="A"><h3>A</h3></a>

<div>
<dt>A capite ad calcem.</dt>
<dd>С ног до головы.</dd>
<dt>A casu ad casum.</dt>
<dd>От случая к случаю.</dd>
<dt>A contrario.</dt>
<dd>От противного (доказывать).</dd>
<dt>A limine.</dt>
<dd>С порога.</dd>
<dt>A maximis ad minima.</dt>
<dd>От большого к малому.</dd>
<dt>A posteriori.</dt>
<dd>Исходя из опыта.</dd>
<dt>A priori.</dt>
<dd>Заранее, без проверки.</dd>
<dt>A probis probari, ab improbis improbari aequa laus est.</dt>
<dd>Одинаково почетны и похвала достойных людей и осуждение недостойных.</dd>
<dt>Ab abusu ad usum non valet consequentia.</dt>
<dd>Злоупотребление не довод против употребления.</dd>
<dt>Ab alteri expectes, alteri quod feceris.</dt>
<dd>Жди от другого того, что ты сделал ему сам.</dd>
<dt>Ab equis ad asinos.</dt>
<dd>Из коня в ослы (пойти на понижение).</dd>
<dt>Ab exterioribus ad interiora.</dt>
<dd>От внешнего к внутреннему.</dd>
<dt>Ab imo pectore.</dt>
<dd>От души, с полной искренностью.</dd>
<dt>Ab incunabulis.</dt>
<dd>С колыбели.</dd>
<dt>Ab initio.</dt>
<dd>С возникновения.</dd>
<dt>Ab origine.</dt>
<dd>С самого начала.</dd>
<dt>Ab ovo usque ad mala.</dt>
<dd>С начала до конца.</dd>
<dt>Ab uno disce omnes.</dt>
<dd>По одному суди о других.</dd>
<dt>Absentum laedit, qui cum ebrio litigat.</dt>
<dd>Кто спорит с пьяным, тот воюет с тенью.</dd>
<dt>Absit invidia.</dt>
<dd>Да не будет зависти и злобы.</dd>
<dt>Absit invidia verbo.</dt>
<dd>Пусть сказанное не вызовет неприязни.</dd>
<dt>Absit omen</dt>
<dd>Да не послужит это дурной приметой!</dd>
<dt>Absque omni exceptione.</dt>
<dd>Без всякого исключения.</dd>
<dt>Abstractum pro concreto.</dt>
<dd>Абстрактное вместо конкретного.</dd>
<dt>Acceptissima semper munera sunt, auctor quae pretiosa facit.</dt>
<dd>Милее всего те подарки, что дарит дорогой нам человек.</dd>
<dt>Accidit in puncto, quod non speratur in anno.</dt>
<dd>В один миг случается то, на что не надеешься и годами.</dd>
<dt>Acta diurna.</dt>
<dd>Происшествия дня, хроника.</dd>
<dt>Acta est fabula.</dt>
<dd>Представление окончено.</dd>
<dt>Actis testantibus.</dt>
<dd>По документам.</dd>
<dt>Actum est, i licet.</dt>
<dd>Дело закончено, можно расходиться.</dd>
<dt>Actum ne agas.</dt>
<dd>С чем покончено, к тому не возвращайся.</dd>
<dt>Ad acta.</dt>
<dd>В архив.</dd>
<dt>Ad aeternam rei memoriam.</dt>
<dd>На вечную память о деле.</dd>
<dt>Ad augusta per angusta.</dt>
<dd>К высокому через трудное.</dd>
<dt>Ad captandum benevolentiam.</dt>
<dd>Для снискания расположения.</dd>
<dt>Ad cogitandum et agendum homo natus.</dt>
<dd>Для мысли и деяния рожден человек.</dd>
<dt>Ad disputandum.</dt>
<dd>Для обсуждения.</dd>
<dt>Ad exemplum.</dt>
<dd>По образцу.</dd>
<dt>Ad extra.</dt>
<dd>До крайней степени.</dd>
<dt>Ad fontes.</dt>
<dd>К источникам, оригиналам.</dd>
<dt>Ad futurun memoriam.</dt>
<dd>На долгую память.</dd>
<dt>Ad gloriam.</dt>
<dd>Во славу.</dd>
<dt>Ad impossibilia nemo obligatur.</dt>
<dd>К невозможному никого не обязывают.</dd>
<dt>Ad infinitum.</dt>
<dd>До бесконечности.</dd>
<dt>Ad libitum.</dt>
<dd>По желанию, по усмотрению.</dd>
<dt>Ad litteram.</dt>
<dd>Дословно, буквально.</dd>
<dt>Ad meliora tempora.</dt>
<dd>До лучших времен.</dd>
<dt>Ad memorandum.</dt>
<dd>Для памяти.</dd>
<dt>Ad modum.</dt>
<dd>По образцу.</dd>
<dt>Ad multos annos.</dt>
<dd>На многие годы.</dd>
<dt>Ad narrandum, non ad probandum.</dt>
<dd>Для рассказывания, а не для доказывания.</dd>
<dt>Ad notam.</dt>
<dd>К сведению.</dd>
<dt>Ad notata.</dt>
<dd>Примечание.</dd>
<dt>Ad patres.</dt>
<dd>К праотцам (отправиться), умереть.</dd>
<dt>Ad poenitendum properat, cito qui judicat.</dt>
<dd>Быстрое решение таит в себе быстрое раскаяние.</dd>
<dt>Ad referendum.</dt>
<dd>К докладу.</dd>
<dt>Ad rem.</dt>
<dd>По существу дела.</dd>
<dt>Ad rem nihil facit.</dt>
<dd>К делу не относится.</dd>
<dt>Ad turpia nemo obligatur.</dt>
<dd>К постыдному никого не принуждают.</dd>
<dt>Ad unum omnes.</dt>
<dd>Все до одного.</dd>
<dt>Ad usum.</dt>
<dd>Для употребления.</dd>
<dt>Ad usum proprium.</dt>
<dd>Для личного пользования.</dd>
<dt>Ad utrumque paratus.</dt>
<dd>Готов к тому и другому.</dd>
<dt>Ad verbum.</dt>
<dd>Слово в слово.</dd>
<dt>Ad virtutem via ardua est.</dt>
<dd>К мужеству дорога терниста.</dd>
<dt>Ad vocem.</dt>
<dd>К слову (заметить).</dd>
<dt>Adhibenda est in jocando moderatio.</dt>
<dd>В шутках следует знать меру.</dd>
<dt>Aditum nocendi perfido praestat fides.</dt>
<dd>Доверие, оказанное вероломному, дает ему возможность вредить.</dd>
<dt>Adversa fortuna.</dt>
<dd>При неблагоприятных обстоятельствах.</dd>
<dt>Aequo animo.</dt>
<dd>Равнодушно, терпеливо.</dd>
<dt>Aestas non semper durabit: condite nidos.</dt>
<dd>Лето не вечно: вейте гнезда.</dd>
<dt>Aetate fruere, mobili cursu fugit.</dt>
<dd>Пользуйся жизнью, она так быстротечна.</dd>
<dt>Aeterna nox.</dt>
<dd>Вечная ночь.</dd>
<dt>Agnosco veteris vestigia flammae.</dt>
<dd>Узнаю следы былого пламени.</dd>
<dt>Alias.</dt>
<dd>Иначе, по-другому.</dd>
<dt>Aliena vitia in oculis habemus, a tergo nostra sunt.</dt>
<dd>Чужие грехи у нас на глазах, а свои за спиной.</dd>
<dt>Alienos agros irrigas, tuis sitientibus.</dt>
<dd>Орошаешь чужие поля, а твои стоят сухими.</dd>
<dt>Aliis inserviendo consumor.</dt>
<dd>Служа другим, сгораю.</dd>
<dt>Aliis ne feceris, quod tibi fieri non vis.</dt>
<dd>Не делай другим того, чего не желаешь себе.</dd>
<dt>Alit lectio ingenium.</dt>
<dd>Чтение обогащает разум.</dd>
<dt>Aliud ex alio malum.</dt>
<dd>Одно зло вытекает из другого.</dd>
<dt>Alma mater.</dt>
<dd>Питающая мать (уважительно об учебном заведении).</dd>
<dt>аltеr ego.</dt>
<dd>Второе "я".</dd>
<dt>Altissima quaeque flumina minimo sono labuntur.</dt>
<dd>Наиболее глубокие реки текут с наименьшим шумом.</dd>
<dt>Amantes amentes.</dt>
<dd>Влюбленные-безумцы.</dd>
<dt>Amantium irae amoris integratio.</dt>
<dd>Гнев влюбленных - это возобновление любви.</dd>
<dt>Amat victoria curam.</dt>
<dd>Победа любит терпение.</dd>
<dt>Ambo meliores.</dt>
<dd>Оба хороши.</dd>
<dt>Amicitia inter pocula contracta plerumque vitrea est.</dt>
<dd>Дружба, заключенная за рюмкой, хрупка, как стекло.</dd>
<dt>Amicitia nisi inter bonos esse non potest.</dt>
<dd>Дружба может быть только между хорошими людьми.</dd>
<dt>Amicitia semper prodest, amor et nocet.</dt>
<dd>Дружба всегда полезна, а любовь может и навредить.</dd>
<dt>Amicitiam natura ipsa peperit.</dt>
<dd>Дружбу создала сама природа.</dd>
<dt>Amicos res secundae parant, adversae probant.</dt>
<dd>Счастье дает друзей, несчастье испытывает их.</dd>
<dt>Amicum laedere ne joco quidem licet.</dt>
<dd>Не обижай друга даже шуткой.</dd>
<dt>Amicum perdere est damnorum maximum.</dt>
<dd>Потеря друга - наибольшая потеря.</dd>
<dt>Amicus Plato, sed magis amica veritas.</dt>
<dd>Платон мне друг, но истина дороже.</dd>
<dt>Amicus certus in re incerta cernitur.</dt>
<dd>Настоящий друг познается в беде.</dd>
<dt>Amicus cognoscitur amore, more, ore, re.</dt>
<dd>Друг познается по любви, нраву, лицу, деянию.</dd>
<dt>Amittit merito proprium, qui alienum appetit.</dt>
<dd>Свое добро теряет тот, кто желает чужое.</dd>
<dt>Amor caecus.</dt>
<dd>Любовь слепа.</dd>
<dt>Amor et tussis non celatur.</dt>
<dd>Любовь и кашель не скрыть.</dd>
<dt>Amor magister optimus.</dt>
<dd>Любовь - лучший учитель.</dd>
<dt>Amor non est medicabilis herbis.</dt>
<dd>Нет от любви лекарства.</dd>
<dt>Amor omnibus idem.</dt>
<dd>В любви все одинаковы.</dd>
<dt>Amor patriae.</dt>
<dd>Любовь к родине.</dd>
<dt>Amor vincit omnia.</dt>
<dd>Любовь все побеждает.</dd>
<dt>Amor, ut lacrima, ab oculo oritur, in cor cadit.</dt>
<dd>Любовь, как слеза - из глаз рождается, на сердце падает.</dd>
<dt>Animos labor nutrit.</dt>
<dd>Труд - пища для ума.</dd>
<dt>Animus aequus optimum est aerumnae condimentum.</dt>
<dd>Равновесие духа - надежное средство от печали.</dd>
<dt>Anni currentis.</dt>
<dd>Сего года.</dd>
<dt>Annum novum bonum felicem faustum fortunatumque</dt>
<dd>Пусть сопутствуют новому году счастье, успех и благополучие!</dd>
<dt>Ante annum.</dt>
<dd>В прошлом году.</dd>
<dt>Ante faciem populi.</dt>
<dd>У всех на виду.</dd>
<dt>Ante victoriam ne canas triumhum.</dt>
<dd>Не кричи о триумфе до победы.</dd>
<dt>Antiqua, quae nunc sunt, fuerunt olim nova.</dt>
<dd>И нынешнее старое было когда-то новым.</dd>
<dt>Antiquus amor cancer est.</dt>
<dd>Старая любовь возвращается.</dd>
<dt>Aperto libro.</dt>
<dd>Без подготовки, с листа.</dd>
<dt>Aquila non captat muscas.</dt>
<dd>Орел не ловит мух.</dd>
<dt>Arcus nimium tensus rumpitur.</dt>
<dd>Слишком натянутая струна лопается.</dd>
<dt>Argumentum a contrario.</dt>
<dd>Доказательство от противного.</dd>
<dt>Argumentum ad ignorantiam.</dt>
<dd>Довод, рассчитанный на неосведомленность собеседника.</dd>
<dt>Argumentum ad misericordiam.</dt>
<dd>Аргумент к милосердию (т.е.
</dt>
<dd>рассчитанный на жалость).</dd>
<dt>Argumentum ad oculos.</dt>
<dd>Наглядное доказательство.</dd>
<dt>Argumentum ad verecundiam.</dt>
<dd>Обращение к совести.</dd>
<dt>Argumentum argentarium.</dt>
<dd>Денежный довод; взятка; подкуп.</dd>
<dt>Argumentum baculinum.</dt>
<dd>Палочный довод; доказательство с помощью насилия.</dd>
<dt>Astra inclinant, non necessitant.</dt>
<dd>Звезды склоняют, а не принуждают.</dd>
<dt>Audentes fortuna juvat.</dt>
<dd>Смелым покровительствует удача.</dd>
<dt>Audi multa, loquere pauca.</dt>
<dd>Слушай много, говори мало.</dd>
<dt>Audi, vide, sile.</dt>
<dd>Слушай, смотри, молчи.</dd>
<dt>Audiatur et altera pars.</dt>
<dd>Следует выслушать и другую сторону.</dd>
<dt>Audire disce, si nescis loqui.</dt>
<dd>Учись слушать, если не умеешь говорить.</dd>
<dt>Aurea mediocritas.</dt>
<dd>Золотая середина.</dd>
<dt>Auri sacra fames</dt>
<dd>О, проклятая жажда золота!</dd>
<dt>Ausculta et perpende.</dt>
<dd>Слушай и разумей.</dd>
<dt>Aut bene, aut nihil.</dt>
<dd>Или хорошо, или ничего.</dd>
<dt>Aut cum scuto, aut in scuto.</dt>
<dd>Со щитом или на щите.</dd>
<dt>Aut non tentaris, aut perfice.</dt>
<dd>Или не берись, или доводи до конца.</dd>
<dt>Aut viam inveniam, aut faciam.</dt>
<dd>Или найду дорогу, или проложу ее сам.</dd>
<dt>Aut vincere, aut mori.</dt>
<dd>Или победить, или умереть.</dd>
<dt>Avarus animus nullo satiatur lucro.</dt>
<dd>Скупого не насытят никакие богатства.</dd>
</div>

<a id="B"><h3>B</h3></a>

<div>
    <dt>Barba philosophum non facit.</dt>
    <dd>Борода не делает философом.</dd>
    <dt>Beata solitudo.</dt>
    <dd>Благословенное одиночество.</dd>
    <dt>Beate vivere est honeste vivere.</dt>
    <dd>Жить счастливо - значит жить красиво.</dd>
    <dt>Beati pacifici.</dt>
    <dd>Блаженны миротворцы.</dd>
    <dt>Beati pauperes spiritu.</dt>
    <dd>Блаженны нищие духом.</dd>
    <dt>Beatus ille, qui procul negotiis.</dt>
    <dd>Блажен тот, кто вдали от дел.</dd>
    <dt>Bellum frigidum.</dt>
    <dd>Холодная война.</dd>
    <dt>Bene dignoscitur, bene curatur.</dt>
    <dd>Хорошо распознается - хорошо вылечивается.</dd>
    <dt>Bene placito.</dt>
    <dd>По доброй воле.</dd>
    <dt>Bene vertat!</dt>
    <dd>Успехов тебе!</dd>
    <dt>Bene vincit, qui se vincit in victoria.</dt>
    <dd>Дважды побеждает тот, кто властвует над собой.</dd>
    <dt>Beneficia non obtruduntur.</dt>
    <dd>Благодеяний не навязывают.</dd>
    <dt>Beneficia plura recipit, qui scit reddere.</dt>
    <dd>Вдвойне получает тот, кто умеет благодарить.</dd>
    <dt>Bis ad eundem lapidem offendere.</dt>
    <dd>Дважды споткнуться о тот же камень.</dd>
    <dt>Bis dat, qui cito dat.</dt>
    <dd>Дважды дает тот, кто дает быстро.</dd>
    <dt>Bis pueri senes.</dt>
    <dd>Старики - второй раз дети.</dd>
    <dt>Bona causa triumphat.</dt>
    <dd>Доброе дело побеждает.</dd>
    <dt>Bona fama divitiis est potior.</dt>
    <dd>Добрая слава лучше богатства.</dd>
    <dt>Bona fide.</dt>
    <dd>Добросовестно, доверчиво.</dd>
    <dt>Bona mente.</dt>
    <dd>С добрыми намерениями.</dd>
    <dt>Bona opinio hominum tutior pecunio est.</dt>
    <dd>Хорошее мнение людей надежнее денег.</dd>
    <dt>Bona valetudo melior est quam maximae divitiae.</dt>
    <dd>Хорошее здоровье лучше самого большого богатства.</dd>
    <dt>Bona venia vestra.</dt>
    <dd>С вашего позволения.</dd>
    <dt>Bonis avibus.</dt>
    <dd>В добрый час.</dd>
    <dt>Bono sensu.</dt>
    <dd>В хорошем смысле.</dd>
    <dt>Bonorum vita vacua est metu.</dt>
    <dd>Жизнь честных (людей) свободна от страха.</dd>
    <dt>Bonos mores corrumpunt congressus mali.</dt>
    <dd>Плохие связи портят хорошие нравы.</dd>
    <dt>Bonum ad virum cito moritur iracundia.</dt>
    <dd>У хорошего человека гнев проходит быстро.</dd>
    <dt>Bonum initium est dimidium facti.</dt>
    <dd>Хорошее начало - половина дела.</dd>
</div>

<a id="C"><h3>C</h3></a>

<div>
    <dt>Calamitate doctus sum.</dt>
    <dd>Горе меня научило.</dd>
    <dt>Cantus cycneus.</dt>
    <dd>Лебединая песня.</dd>
    <dt>Calvitium non est vitium sed prudentiae indicium.</dt>
    <dd>Лысина не порок, а свидетельство мудрости.</dd>
    <dt>Cancrum recta ingredi doces.</dt>
    <dd>Учить рака ходить вперед (т.е.
    </dt>
    <dd>напрасный труд).</dd>
    <dt>Capienda rebus in malis praeceps via est.</dt>
    <dd>В беде следует принимать опасные решения.</dd>
    <dt>Carpe diem.</dt>
    <dd>Лови день.</dd>
    <dt>Castis omnia casta.</dt>
    <dd>Чистым (людям) все кажется чистым.</dd>
    <dt>Causa causarum.</dt>
    <dd>Причина причин (главная причина).</dd>
    <dt>Causa finita est.</dt>
    <dd>Вопрос решен.</dd>
    <dt>Cave illum semper, qui tibi imposuit semel.</dt>
    <dd>Остерегайся того, кто обманул тебя хотя бы однажды.</dd>
    <dt>Cave ne cadas.</dt>
    <dd>Берегись, чтобы не упасть.</dd>
    <dt>Cave tibi a cano muto et aqua silente.</dt>
    <dd>Берегись тихого пса и спокойной воды.</dd>
    <dt>Cave, quid dicas, quando et cui.</dt>
    <dd>Смотри, что говоришь, когда и кому.</dd>
    <dt>Cavete a falsis amicis.</dt>
    <dd>Опасайтесь фальшивых друзей.</dd>
    <dt>Cedo majori.</dt>
    <dd>Уступай старшему.</dd>
    <dt>Cessante causa cessat effectus.</dt>
    <dd>С устранением причины исчезает и следствие.</dd>
    <dt>Cetera desiderantur.</dt>
    <dd>Об остальном остается желать.</dd>
    <dt>Ceteris paribus.</dt>
    <dd>При прочих равных условиях.</dd>
    <dt>Charta non erubescit.</dt>
    <dd>Бумага не краснеет.</dd>
    <dt>Circulus vitiosus.</dt>
    <dd>Порочный круг.</dd>
    <dt>Cogitationes posteriores saepe sunt meliores.</dt>
    <dd>Более поздние мысли часто лучше.</dd>
    <dt>Cogitationis poenam nemo patitur.</dt>
    <dd>Никто не наказуем за мысли.</dd>
    <dt>Cogito, ergo sum.</dt>
    <dd>Я мыслю, следовательно существую.</dd>
    <dt>Cognosce te ipsum.</dt>
    <dd>Познай самого себя.</dd>
    <dt>Concordia res parvae crescunt.</dt>
    <dd>Согласием возвеличиваются и малые дела.</dd>
    <dt>Conditio sine qua non.</dt>
    <dd>Непременное условие.</dd>
    <dt>Conscia mens recti famae mendacia ridet.</dt>
    <dd>Чистая совесть смеется над ложными слухами.</dd>
    <dt>Consensu omnium.</dt>
    <dd>С общего согласия.</dd>
    <dt>Consideratio naturae.</dt>
    <dd>Созерцание природы.</dd>
    <dt>Consuetudinis magna vis est.</dt>
    <dd>Велика сила привычки.</dd>
    <dt>Consuetudo est altera natura.</dt>
    <dd>Привычка - вторая натура.</dd>
    <dt>Consultor homini tempus utilissimus.</dt>
    <dd>Время - лучший советчик.</dd>
    <dt>Consummatum est</dt>
    <dd>Свершилось!</dd>
    <dt>Contra bonos mores.</dt>
    <dd>Безнравственно.</dd>
    <dt>Contraria contrariis curantur.</dt>
    <dd>Противоположное лечится противоположным.</dd>
    <dt>Copia ciborum subtilitas animi impeditur.</dt>
    <dd>Избыток пищи мешает тонкости ума.</dd>
    <dt>Copia verborum.</dt>
    <dd>Многословие.</dd>
    <dt>Corrige praetertum, praesens rege, cerne futurum.</dt>
    <dd>Анализируй прошлое, руководи настоящим, предусматривай будущее.</dd>
    <dt>Corruptio optimi pessima.</dt>
    <dd>Совращение доброго - наибольший грех.</dd>
    <dt>Crede experto.</dt>
    <dd>Верь опытному.</dd>
    <dt>Credo quia verum.</dt>
    <dd>Верю, ибо это истина.</dd>
    <dt>Credo ut intelligam.</dt>
    <dd>Верую, чтобы познать.</dd>
    <dt>Credula res amor est.</dt>
    <dd>Любовь склонна к доверчивости.</dd>
    <dt>Crescentem pecuniam sequitur cura.</dt>
    <dd>С богатством растут и заботы.</dd>
    <dt>Crescit amor nummi, quantum ipsa pecunia crescit.</dt>
    <dd>Растет любовь к деньгам по мере того, как растет само богатство.</dd>
    <dt>Crudelitatis mater avaritia est.</dt>
    <dd>Жадность - мать жестокости.</dd>
    <dt>Cui prodest.?</dt>
    <dd>Кому выгодно?.</dd>
    <dt>Cuique proprius attributus error est.</dt>
    <dd>Каждый человек имеет какой-либо недостаток.</dd>
    <dt>Cuivis dolori remedium est patientia.</dt>
    <dd>Терпение - лекарство от любых страданий.</dd>
    <dt>Cujus regio, ejus religio.</dt>
    <dd>Чья область, того и религия.</dd>
    <dt>Culpa lata.</dt>
    <dd>Грубая ошибка.</dd>
    <dt>Cum bonis bonus eris, cum malis perverteris.</dt>
    <dd>С хорошими людьми будешь хорошим, с плохими - испортишься.</dd>
    <dt>Cum feriunt unum, non unum fulmina terrent.</dt>
    <dd>Молния поражает одного, а пугает многих.</dd>
    <dt>Cum grano salis.</dt>
    <dd>Иронически.</dd>
    <dt>Cum sancto spiritu.</dt>
    <dd>Со святым духом.</dd>
    <dt>Cuneus cuneum trudit.</dt>
    <dd>Клин клином вышибают.</dd>
    <dt>Cupido atque ira consultores pessimi.</dt>
    <dd>Страсть и гнев - наихудшие советчики.</dd>
    <dt>Curae leves loquuntur, ingentes stupent.</dt>
    <dd>Малые печали говорят, большие - безмолвны.</dd>
    <dt>Curriculum vitae.</dt>
    <dd>Жизненный путь.</dd>
    <dt>Custos morum.</dt>
    <dd>Блюститель нравственности.</dd>
    <dt>Da locum melioribus</dt>
    <dd>Уступи место лучшим!</dd>
    <dt>Damnant quod non intelligunt.</dt>
    <dd>Осуждают то, чего не понимают.</dd>
    <dt>Dant gaudea vires.</dt>
    <dd>Радость прибавляет силы.</dd>
</div>

<a id="D"><h3>D</h3></a>

<div>
    <dt>De auditu.</dt>
    <dd>По слухам.</dd>
    <dt>De caelo in caenum.</dt>
    <dd>С неба в грязь.</dd>
    <dt>De duobus malis minus est semper eligendum.</dt>
    <dd>Из двух зол выбирай меньшее.</dd>
    <dt>De gustibus non est disputandum.</dt>
    <dd>О вкусах не спорят.</dd>
    <dt>De lingua stulta veniunt incommoda multa.</dt>
    <dd>Глупый язык доставляет много неприятностей.</dd>
    <dt>De mortuis aut bene, aut nihil.</dt>
    <dd>О мертвых или молчи, или говори хорошее.</dd>
    <dt>De mortuis et absentibus nihil nisi bene.</dt>
    <dd>О мертвых или отсутствующих ничего, кроме хорошего.</dd>
    <dt>De nocte consilium.</dt>
    <dd>Ночь приносит совет.</dd>
    <dt>De principiis non est disputandum.</dt>
    <dd>О принципах не спорят.</dd>
    <dt>De proprio motu.</dt>
    <dd>Из собственного побуждения.</dd>
    <dt>De visu.</dt>
    <dd>Воочию, глазами очевидца.</dd>
    <dt>Debes, ergo potes.</dt>
    <dd>Ты должен, значит можешь.</dd>
    <dt>Deceptio visus.</dt>
    <dd>Оптический обман.</dd>
    <dt>Decipimur specie recti.</dt>
    <dd>Видимость совершенства обманчива.</dd>
    <dt>Decipit frons prima multos.</dt>
    <dd>Внешность обманчива.</dd>
    <dt>Deliberando discitur sapientia.</dt>
    <dd>Мудрости учатся размышлением.</dd>
    <dt>Deliberandum est diu, quod statuendum est semel.</dt>
    <dd>Обдумывай долго, решай один раз.</dd>
    <dt>Delicias habet omne suas et gaudia tempus.</dt>
    <dd>Всякое время имеет свои удовольствия и радости.</dd>
    <dt>Delirium tremens.</dt>
    <dd>Белая горячка.</dd>
    <dt>Delphinum natare doces.</dt>
    <dd>Учишь дельфина плавать.</dd>
    <dt>Desidia est initium omnium vitiorum.</dt>
    <dd>Безделье - начало всех пороков.</dd>
    <dt>Desipere in loco.</dt>
    <dd>Безумствовать там, где это уместно.</dd>
    <dt>Despice divitias, si vis animo esse beatus</dt>
    <dd>Если хочешь быть счастливым - презирай богатство!</dd>
    <dt>Destruam et aedificabo.</dt>
    <dd>Разрушу и воздвигну.</dd>
    <dt>Desunt inopiae multa, avaritiae omnia.</dt>
    <dd>Бедным не хватает многого, алчным - всего.</dd>
    <dt>Dicere non est facere.</dt>
    <dd>Сказать - еще не значит сделать.</dd>
    <dt>Dictis facta respondeant.</dt>
    <dd>Пусть дела соответствуют словам.</dd>
    <dt>Dicto die.</dt>
    <dd>В назначенный день.</dd>
    <dt>Dictum factum.</dt>
    <dd>Сказано - сделано.</dd>
    <dt>Diligentia in omnibus rebus valet.</dt>
    <dd>Усердие необходимо во всех делах.</dd>
    <dt>Dives est, qui sapiens est.</dt>
    <dd>Тот богат, кто мудр.</dd>
    <dt>Divide et impera.</dt>
    <dd>Разделяй и властвуй.</dd>
    <dt>Divitiae et honores incerta et caduca sunt.</dt>
    <dd>Богатство и почести неустойчивы и преходящи.</dd>
    <dt>Dixi et animam levavi.</dt>
    <dd>Я сказал и тем облегчил душу.</dd>
    <dt>Docendo discimus.</dt>
    <dd>Уча, учимся.</dd>
    <dt>Doctrina multiplex, veritas una.</dt>
    <dd>Учений много, истина одна.</dd>
    <dt>Doctus nemo nascitur.</dt>
    <dd>Ученым не рождаются.</dd>
    <dt>Domus propria domus optima.</dt>
    <dd>Свой дом - лучший дом.</dd>
    <dt>Donec eris felix, multos numerabis amicos.</dt>
    <dd>Пока ты удачлив, у тебя много друзей.</dd>
    <dt>Dubia plus torquet mala.</dt>
    <dd>Неизвестность тревожит больше самой беды.</dd>
    <dt>Dubitatio ad veritatem pervenimus.</dt>
    <dd>Путь к истине - через сомнения.</dd>
    <dt>Ducunt volentem fata, nolentem trahunt.</dt>
    <dd>Желающего судьба ведет, нежелающего - тащит.</dd>
    <dt>Dulce domum.</dt>
    <dd>Сладостно домой.</dd>
    <dt>Dulce et decorum est pro patria mori.</dt>
    <dd>Отрадна и почетна смерть за родину.</dd>
    <dt>Dulce laudari a laudato viro.</dt>
    <dd>Приятна похвала от того, кто сам ее достоин.</dd>
    <dt>Dulcia non novit, qui non gustavat amara.</dt>
    <dd>Тот не знает сладкого, кто не испытал горького.</dd>
    <dt>Dum spiro spero.</dt>
    <dd>Пока дышу-надеюсь.</dd>
    <dt>Dum vivimus vivamus.</dt>
    <dd>Давайте жить, пока живется.</dd>
    <dt>Duos qui lepores sequitur, neutrum capit.</dt>
    <dd>Кто гонится за двумя зайцами, не поймает ни одного.</dd>
    <dt>Dura lex, sed lex.</dt>
    <dd>Суров закон, но это закон.</dd>
    <dt>Dura necessitas.</dt>
    <dd>Жестокая необходимость.</dd>
</div>

<a id="E"><h3>E</h3></a>

<div>
    <dt>E cantu dignoscitur avis.</dt>
    <dd>Птица узнается по пению.</dd>
    <dt>E fructu arbor cognoscitur.</dt>
    <dd>По плоду узнают и дерево.</dd>
    <dt>Edimus ut vivamus, non vivimus ut edamus.</dt>
    <dd>Мы едим, чтобы жить, а не живем, чтобы есть.</dd>
    <dt>Ego nihil timeo, quia nihil habeo.</dt>
    <dd>Я ничего не боюсь, потому что ничего не имею.</dd>
    <dt>Emas non quod opus est, sed quod necesse est.</dt>
    <dd>Покупай не то, что нужно, а то, что необходимо.</dd>
    <dt>Eo benefaciendo.</dt>
    <dd>Иду, творя добро.</dd>
    <dt>Eo ipso.</dt>
    <dd>Тем самым.</dd>
    <dt>Eo rus.</dt>
    <dd>Еду в деревню.</dd>
    <dt>Eo, quocumque pedes ferunt.</dt>
    <dd>Иду, куда несут ноги.</dd>
    <dt>Eripitur persona, manet res.</dt>
    <dd>Человек погибает, дело остается.</dd>
    <dt>Errando discimus.</dt>
    <dd>Ошибки учат.</dd>
    <dt>Errare humanum est.</dt>
    <dd>Человеку свойственно ошибаться.</dd>
    <dt>Esse quam vederi.</dt>
    <dd>Быть, а не казаться.</dd>
    <dt>Est foculus proprius multo pretiosior auro.</dt>
    <dd>Домашний очаг намного ценнее золота.</dd>
    <dt>Est haec natura mortalium, ut nihil magis placet, quam quod amissum est.</dt>
    <dd>Ничто так более не желанно, чем то, что утеряно.</dd>
    <dt>Est modus in rebus.</dt>
    <dd>Есть мера в вещах.</dd>
    <dt>Est quodam prodire tenus, si non datur ultra.</dt>
    <dd>Если не дается далекое, возьми то, что под рукой.</dd>
    <dt>Et cetera (etc..)</dt>
    <dd>И так далее.</dd>
    <dt>Et fumus patriae dulcis.</dt>
    <dd>И дым отечества сладок.</dd>
    <dt>Etiam post malam segetem serendum est.</dt>
    <dd>И после плохого урожая надо сеять.</dd>
    <dt>Etiam sanato vulnere cicatrix manet.</dt>
    <dd>И зажившие раны оставляют рубцы.</dd>
    <dt>Ex aequo.</dt>
    <dd>Поровну.</dd>
    <dt>Ex animo.</dt>
    <dd>От души.</dd>
    <dt>Ex arena funiculum nectis.</dt>
    <dd>Плести веревку из песка.</dd>
    <dt>Ex auditu.</dt>
    <dd>На слух.</dd>
    <dt>Ex duobus malis minus est deligendum.</dt>
    <dd>Из двух зол выбирать меньшее.</dd>
    <dt>Ex fontibus.</dt>
    <dd>По первоисточникам.</dd>
    <dt>Ex gratia.</dt>
    <dd>Из любезности.</dd>
    <dt>Ex industria privata.</dt>
    <dd>По собственной инициативе.</dd>
    <dt>Ex minimis seminibus nascuntur ingentia.</dt>
    <dd>И малое семя рождает большое дерево.</dd>
    <dt>Ex more.</dt>
    <dd>По обычаям.</dd>
    <dt>Ex necessitate.</dt>
    <dd>По необходимости.</dd>
    <dt>Ex nihilo nihil.</dt>
    <dd>Из ничего ничего (не получится).</dd>
    <dt>Ex officio.</dt>
    <dd>По обязанности.</dd>
    <dt>Ex ore parvulorum veritas.</dt>
    <dd>Из уст младенцев - истина.</dd>
    <dt>Ex providentia majorum.</dt>
    <dd>По завету предков.</dd>
    <dt>Ex tempore.</dt>
    <dd>Без подготовки, экспромтом.</dd>
    <dt>Ex voto.</dt>
    <dd>По обещанию.</dd>
    <dt>Exceptio probat regulam.</dt>
    <dd>Исключение подтверждает правило.</dd>
    <dt>Excitare fluctus in simpulo.</dt>
    <dd>Поднять бурю в стакане воды.</dd>
    <dt>Exempla docent.</dt>
    <dd>Примеры учат.</dd>
    <dt>Exempli causa.</dt>
    <dd>Например.</dd>
    <dt>Exempli gratia (e.g..)</dt>
    <dd>Например.</dd>
    <dt>Exercitatio optimus est magister.</dt>
    <dd>Практика - лучший учитель.</dd>
    <dt>Exitus letalis.</dt>
    <dd>Смертельный исход.</dd>
    <dt>Experto credite.</dt>
    <dd>Верьте опыту.</dd>
    <dt>Extempore.</dt>
    <dd>Наспех; быстро.</dd>
    <dt>Extremis malis, extrema remedia.</dt>
    <dd>Особым случаям - особые меры.</dd>
</div>

<a id="F"><h3>F</h3></a>

<div>
    <dt>Faber est suae quisquie fortunae.</dt>
    <dd>Каждый кузнец своей судьбы.</dd>
    <dt>Fabricando fit faber.</dt>
    <dd>Мастер создается трудом.</dd>
    <dt>Fac et spera.</dt>
    <dd>Действуй и надейся.</dd>
    <dt>Fac simile.</dt>
    <dd>Сделай подобное.</dd>
    <dt>Facile dictu, difficile factu.</dt>
    <dd>Легко сказать, трудно сделать.</dd>
    <dt>Facile omnes, cum valemus, recta consilia aegrotis damus.</dt>
    <dd>Когда здоровы, мы легко даем советы больным.</dd>
    <dt>Facilius est apta dissolvere, quam dissipata connectere.</dt>
    <dd>Легче разделить связанное, чем соединить разделенное.</dd>
    <dt>Facilius est plus facere, quam idem.</dt>
    <dd>Легче сделать больше, чем то же самое.</dd>
    <dt>Facta loquuntur.</dt>
    <dd>Факты кричат.</dd>
    <dt>Facta notoria.</dt>
    <dd>Общеизвестные факты.</dd>
    <dt>Facta sunt verbis difficiliora.</dt>
    <dd>Легче сказать, чем сделать.</dd>
    <dt>Facta, non verba</dt>
    <dd>Дел, а не слов!</dd>
    <dt>Factum est factum.</dt>
    <dd>Что сделано, то сделано.</dd>
    <dt>Facundus est error, cum simplex sit veritas.</dt>
    <dd>Ложь - красноречива, правда - проста.</dd>
    <dt>Fallaces sunt rerum species.</dt>
    <dd>Внешний вид обманчив.</dd>
    <dt>Falsus in uno, falsus in omnibus.</dt>
    <dd>Фальшь в одном - фальшь во всем.</dd>
    <dt>Fama clamosa.</dt>
    <dd>Громкая слава.</dd>
    <dt>Fama crescit eundo.</dt>
    <dd>Молва растет на ходу.</dd>
    <dt>Fama nihil est celerius.</dt>
    <dd>Нет ничего быстрее молвы.</dd>
    <dt>Fama volat.</dt>
    <dd>Молва летает.</dd>
    <dt>Famam curant multi, pauci conscientiam.</dt>
    <dd>Многие заботятся о славе, но немногие - о совести.</dd>
    <dt>Fastidium est quies.</dt>
    <dd>Скука - отдохновение души.</dd>
    <dt>Fata viam invenient.</dt>
    <dd>От судьбы не уйти.</dd>
    <dt>Fatum est series causarum.</dt>
    <dd>Судьба - это ряд причин.</dd>
    <dt>Febris erotica.</dt>
    <dd>Любовная лихорадка.</dd>
    <dt>Feci quod potui, faciant meliora potentes.</dt>
    <dd>Я сделал все, что мог, кто может, пусть сделает лучше.</dd>
    <dt>Fecundi calices quem non fecere disertum</dt>
    <dd>Кого не делали красноречивым полные кубки!</dd>
    <dt>Felicitas humana numquam in eodem statu permanet.</dt>
    <dd>Человеческое счастье не бывает постоянным.</dd>
    <dt>Felicitas multa habet amicos.</dt>
    <dd>У счастья много друзей.</dd>
    <dt>Ferae naturae.</dt>
    <dd>Дикий нрав.</dd>
    <dt>Feriunt summos fulgura montes.</dt>
    <dd>Молнии попадают в самые высокие горы.</dd>
    <dt>Ferro ignique.</dt>
    <dd>Огнем и мечом.</dd>
    <dt>Ferro nocentius aurum.</dt>
    <dd>Золото преступнее железа.</dd>
    <dt>Fervet opus.</dt>
    <dd>Работа кипит.</dd>
    <dt>Festina lente.</dt>
    <dd>Торопись медленно.</dd>
    <dt>Festinatio tarda est.</dt>
    <dd>Торопливость задерживает.</dd>
    <dt>Festinationis comites sunt error et poenitentia.</dt>
    <dd>Спутники поспешности - ошибка и раскаяние.</dd>
    <dt>Fiat lux</dt>
    <dd>Да будет свет!</dd>
    <dt>Fiat</dt>
    <dd>Да будет!</dd>
    <dt>Fide, sed cui, vide.</dt>
    <dd>Доверяй, но смотри кому.</dd>
    <dt>Finis coronat opus.</dt>
    <dd>Конец - всему делу венец.</dd>
    <dt>Finis sanctificat media.</dt>
    <dd>Цель оправдывает средства.</dd>
    <dt>Flamma fumo est proxima.</dt>
    <dd>Где дым, там и огонь.</dd>
    <dt>Fons et origo.</dt>
    <dd>Первоисточник.</dd>
    <dt>Fontes ipsi sitiunt.</dt>
    <dd>Даже источники испытывают жажду.</dd>
    <dt>Fortes fortuna adjuvat.</dt>
    <dd>Смелым судьба помогает.</dd>
    <dt>Fortis imaginatio generat casum.</dt>
    <dd>Сильное воображение рождает событие.</dd>
    <dt>Fortiter malum qui patitur, idem post potitur bonum.</dt>
    <dd>Кто мужественно переносит горе, тот добивается счастья.</dd>
    <dt>Fortuna caeca est.</dt>
    <dd>Судьба слепа.</dd>
    <dt>Frons animi janua.</dt>
    <dd>Лицо - дверь души.</dd>
    <dt>Fugaces labuntur anni</dt>
    <dd>Скользят быстротечные годы!</dd>
    <dt>Furor scribendi.</dt>
    <dd>Рвение к писательству.</dd>
    <dt>Futura sunt in manibus deorum.</dt>
    <dd>Будущее в руках богов.</dd>
</div>

<a id="G"><h3>G</h3></a>

<div>
    <dt>Gaudium in litteris est.</dt>
    <dd>Утешение в науках.</dd>
    <dt>Generosi animi et magnifici est juvare et prodesse.</dt>
    <dd>Свойство благородного и великодушного - помогать и приносить пользу.</dd>
    <dt>Generosus animos labor nutrit.</dt>
    <dd>Труд питает благородные сердца.</dd>
    <dt>Gens una sumus.</dt>
    <dd>Мы одно племя.</dd>
    <dt>Grata superveniet, quae non sperabitur, hora.</dt>
    <dd>Приятное мгновение, которого не ждешь.</dd>
    <dt>Gratia gratiam parit.</dt>
    <dd>Благодарность рождает благодарность.</dd>
    <dt>Gratiora sunt, quae pluris emuntur.</dt>
    <dd>Милее то, что труднее досталось.</dd>
    <dt>Gratis.</dt>
    <dd>Даром.</dd>
    <dt>Graviora manet.</dt>
    <dd>Трудности впереди.</dd>
    <dt>Graviores et difficiliores animi sunt morbi, quam corporis.</dt>
    <dd>Болезни души тяжелее и труднее, чем болезни тела.</dd>
    <dt>Gravissimi sunt morsus irritatae necessitatis.</dt>
    <dd>Укусы разъяренной необходимости наиболее опасны.</dd>
    <dt>Gravissimum est imperium consuetudinis.</dt>
    <dd>Сила привычки очень устойчива.</dd>
    <dt>Grosso modo.</dt>
    <dd>В общих чертах.</dd>
    <dt>Gustus legibus non subjacet.</dt>
    <dd>Вкус не подчиняется законам.</dd>
    <dt>Gutta cavat lapidem non vi, sed saepe cadendo.</dt>
    <dd>Капля долбит камень не силой, а частым падением.</dd>
</div>

<a id="H"><h3>H</h3></a>

<div>
    <dt>Habeat sibi.</dt>
    <dd>Пусть себе владеет.</dd>
    <dt>Habent sua fata libelli.</dt>
    <dd>Книги имеют свою судьбу.</dd>
    <dt>Haec habui, quae dixi.</dt>
    <dd>Что имел, то сказал.</dd>
    <dt>Haec hactenus.</dt>
    <dd>На сей раз достаточно.</dd>
    <dt>Haud semper errat fama.</dt>
    <dd>Молва не всегда ошибается.</dd>
    <dt>Haurit aquam cribro, qui discere vult sine libro.</dt>
    <dd>Тот воду черпает решетом, кто хочет учиться без книги.</dd>
    <dt>Hic et nunc</dt>
    <dd>Здесь и сейчас!</dd>
    <dt>Hoc erat in fatis.</dt>
    <dd>Так суждено судьбой.</dd>
    <dt>Hoc erat in more majorum.</dt>
    <dd>Таков обычай предков.</dd>
    <dt>Hoc est in votis.</dt>
    <dd>Это мое желание.</dd>
    <dt>Hoc est vivere bis, vita posse priore frui.</dt>
    <dd>Уметь наслаждаться - значит жить дважды.</dd>
    <dt>Hoc fac et vinces.</dt>
    <dd>Сделай это, и ты победишь.</dd>
    <dt>Hoc mense (h.m..)</dt>
    <dd>В этом месяце.</dd>
    <dt>Hoc tibi proderit olim.</dt>
    <dd>Это когда-нибудь будет тебе на пользу.</dd>
    <dt>Hodie Caesar, cras nihil.</dt>
    <dd>Сегодня Цезарь, завтра ничто.</dd>
    <dt>Hominem ex operibus ejus cognoscere.</dt>
    <dd>Человека узнают по его делам.</dd>
    <dt>Hominem quaero.</dt>
    <dd>Ищу человека.</dd>
    <dt>Hominem, te esse memento.</dt>
    <dd>Помни, что ты человек.</dd>
    <dt>Homines caecos reddit cupiditas.</dt>
    <dd>Страсть делает людей слепыми.</dd>
    <dt>Homines nihil agendo discunt male agere.</dt>
    <dd>Ничего не делая, люди учатся делать дурное.</dd>
    <dt>Homines, quo plura habent, eo cupiunt ampliora.</dt>
    <dd>Чем больше люди имеют, тем больше жаждут иметь.</dd>
    <dt>Homini cibus utilissimus est simplex.</dt>
    <dd>Простая еда - самая полезная.</dd>
    <dt>Hominis est errare, insipientis perseverare.</dt>
    <dd>Человеку свойственно ошибаться, глупцу - упорствовать.</dd>
    <dt>Hominis mens discendo alitur cogitandoque.</dt>
    <dd>Человеческий разум питают наука и мышление.</dd>
    <dt>Homo doctus in se semper divitias habet.</dt>
    <dd>Ученый человек всегда богат.</dd>
    <dt>Homo homini amicus est.</dt>
    <dd>Человек человеку друг.</dd>
    <dt>Homo homini lupus est.</dt>
    <dd>Человек человеку волк.</dd>
    <dt>Homo improbus beatus non est.</dt>
    <dd>Нечестный человек не бывает счастливым.</dd>
    <dt>Homo locum ornat, non hominem locus.</dt>
    <dd>Не место красит человека, а человек место.</dd>
    <dt>Homo omnium horarum.</dt>
    <dd>Надежный человек.</dd>
    <dt>Homo sum, humani nihil a me alienum puto.</dt>
    <dd>Я человек, и ничто человеческое мне не чуждо.</dd>
    <dt>Homo totiens moritur, quotiens amittit suos.</dt>
    <dd>Человек столько раз умирает, сколько теряет своих близких.</dd>
    <dt>Homo, qui tacere nescit, nescit dicere.</dt>
    <dd>Человек, не умеющий молчать, не умеет и говорить.</dd>
    <dt>Honesta vita beata est.</dt>
    <dd>Честная жизнь - счастливая жизнь.</dd>
    <dt>Honeste vivere, alterum non laedere, suum tribuere.</dt>
    <dd>Живи честно, де делай зла другим, отдавай каждому свое.</dd>
    <dt>Honestus rumor alterum patrimonium est.</dt>
    <dd>Хорошая репутация заменяет наследство.</dd>
    <dt>Honores mutant mores, sed raro in meliores.</dt>
    <dd>Почести меняют нравы, но редко в лучшую сторону.</dd>
    <dt>Honoris causa.</dt>
    <dd>Ради почета.</dd>
    <dt>Honos est onus.</dt>
    <dd>Почет - бремя.</dd>
    <dt>Horribile dictu.</dt>
    <dd>Страшно сказать!</dd>
    <dt>Horror vacui.</dt>
    <dd>Боязнь пустоты.</dd>
    <dt>Hospes hospiti sacer.</dt>
    <dd>Гость - святое дело для хозяина.</dd>
    <dt>Hostis honori invidia.</dt>
    <dd>Зависть - враг чести.</dd>
    <dt>Humana non sunt turpia.</dt>
    <dd>Что человеческое, то не постыдно.</dd>
    <dt>Humanum est.</dt>
    <dd>Свойственно человеку.</dd>
</div>

<a id="I"><h3>I</h3></a>

<div>
    <dt>Ibi bene, ubi patria.</dt>
    <dd>Там хорошо, где отечество.</dd>
    <dt>Ibi semper victoria, ubi concordia est.</dt>
    <dd>Там победа, где согласие.</dd>
    <dt>Ibidem</dt>
    <dd>(сокращ. ibid..) Там же.</dd>
    <dt>Id est</dt>
    <dd>(сокращ. i.e..) То есть.</dd>
    <dt>Idea fixa.</dt>
    <dd>Навязчивая идея.</dd>
    <dt>Idem</dt>
    <dd>(сокращ. id..) То же самое.</dd>
    <dt>Ignavia est jacere, dum possis surgere.</dt>
    <dd>Малодушие - лежать, если можешь подняться.</dd>
    <dt>Ignoranti, quem portum petat, nullus ventus secundus est.</dt>
    <dd>Нет попутного ветра для того, кто не знает, к какому порту причалить.</dd>
    <dt>Ignorantia non est argumentum.</dt>
    <dd>Невежество - не аргумент.</dd>
    <dt>Ignoscas aliis multa, nihil tibi.</dt>
    <dd>Другим прощай многое, себе - ничего.</dd>
    <dt>Ignoscito saepe alteri, nunquam tibi.</dt>
    <dd>Другим прощай, себе - никогда.</dd>
    <dt>Ille dolet vere, qui sine teste dolet.</dt>
    <dd>Истинно скорбит тот, кто скорбит без свидетелей.</dd>
    <dt>Impavide progrediamur.</dt>
    <dd>Пойдем вперед без колебаний.</dd>
    <dt>Impedit ira animum, ne possit cernere verum.</dt>
    <dd>Гнев не позволяет разуму познать правду.</dd>
    <dt>Imperare sibi maximum imperium est.</dt>
    <dd>Повелевать собою - величайшая власть.</dd>
    <dt>Imperat aut servit collecta pecunia cuique.</dt>
    <dd>Для одних богатство - слуга, для других - господин.</dd>
    <dt>Imprimatur.</dt>
    <dd>Пусть печатается.</dd>
    <dt>In aeternum.</dt>
    <dd>Навек, навсегда.</dd>
    <dt>In bonam partem.</dt>
    <dd>В хорошем смысле.</dd>
    <dt>In commune bonum.</dt>
    <dd>Для общего блага.</dd>
    <dt>In compacto.</dt>
    <dd>В сжатом виде; вкратце.</dd>
    <dt>In corpore.</dt>
    <dd>В полном составе.</dd>
    <dt>In divitiis inopes, quod genus egestatis gravissimum est.</dt>
    <dd>Нужда в богатстве - самый тяжкий вид нищеты.</dd>
    <dt>In futuro.</dt>
    <dd>В будущем.</dd>
    <dt>In imo pectore.</dt>
    <dd>В глубине души.</dd>
    <dt>In indefinitum.</dt>
    <dd>На неопределенный срок.</dd>
    <dt>In integrum.</dt>
    <dd>В первоначальном виде.</dd>
    <dt>In legibus salus.</dt>
    <dd>В законе спасение.</dd>
    <dt>In magnis et voluisse sat est.</dt>
    <dd>В великих делах достаточно и одного желания.</dd>
    <dt>In medias res.</dt>
    <dd>В самую суть.</dd>
    <dt>In minimis maximus.</dt>
    <dd>В мелочах велик.</dd>
    <dt>In omnem eventum.</dt>
    <dd>На всякий случай.</dd>
    <dt>In omnia paratus.</dt>
    <dd>Готов ко всему.</dd>
    <dt>In optima forma.</dt>
    <dd>В лучшем виде.</dd>
    <dt>In pace leones, in proelio cervi.</dt>
    <dd>В мирное время - львы, в сражении - олени.</dd>
    <dt>In patria natus non est propheta vocatus.</dt>
    <dd>Нет пророка в своем отечестве.</dd>
    <dt>In perpetuum.</dt>
    <dd>На вечные времена.</dd>
    <dt>In persona.</dt>
    <dd>Персонально.</dd>
    <dt>In pleno.</dt>
    <dd>В полном составе.</dd>
    <dt>In praesenti.</dt>
    <dd>В настоящее время.</dd>
    <dt>In praxi.</dt>
    <dd>На практике.</dd>
    <dt>In propria persona.</dt>
    <dd>Собственной персоной.</dd>
    <dt>In recto virtus.</dt>
    <dd>В правде добродетель.</dd>
    <dt>In saecula saeculorum.</dt>
    <dd>Во веки веков.</dd>
    <dt>In spe.</dt>
    <dd>В надежде.</dd>
    <dt>In tempore opportuno.</dt>
    <dd>В удобное время.</dd>
    <dt>In vas pertusum congerere.</dt>
    <dd>Дырявого кувшина не наполнишь.</dd>
    <dt>In vino veritas.</dt>
    <dd>Истина в вине.</dd>
    <dt>Incertus animus dimidium sapientiae est.</dt>
    <dd>Сомнение - половина мудрости.</dd>
    <dt>Incipit vita nova.</dt>
    <dd>Начинается новая жизнь.</dd>
    <dt>Incognito.</dt>
    <dd>Тайно.</dd>
    <dt>Indigne vivit, per quem non vivit аltеr.</dt>
    <dd>Недостойно живет тот, кто не дает жить другим.</dd>
    <dt>Industriae nil impossibile.</dt>
    <dd>Для старательного нет ничего невозможного.</dd>
    <dt>Ingrato homine terra pejus nihil creat.</dt>
    <dd>Ничего худшего, чем неблагодарный человек, земля не рождает.</dd>
    <dt>Iniqua nunquam imperia retinentur diu.</dt>
    <dd>Несправедливая власть недолговечна.</dd>
    <dt>Iniquissimam pacem justissimo bello anteferro.</dt>
    <dd>Самый несправедливый мир предпочтительнее самой справедливой войны.</dd>
    <dt>Iniquum est quemque ex veste aestimare.</dt>
    <dd>Несправедливо оценивать кого-либо по одежде.</dd>
    <dt>Injuria realis.</dt>
    <dd>Оскорбление действием.</dd>
    <dt>Injuria solvit amorem.</dt>
    <dd>Обида разрушает любовь.</dd>
    <dt>Injuria verbalis.</dt>
    <dd>Оскорбление словом.</dd>
    <dt>Injuriarum nemo obliviscitur, beneficiorum autem multi.</dt>
    <dd>Об обидах не забывает никто, о благодеяниях же - многие.</dd>
    <dt>Injuriarum remedium est oblivio.</dt>
    <dd>Забвение - лекарство от несправедливостей.</dd>
    <dt>Innocens credit omni verbo.</dt>
    <dd>Простодушный верит любому слову.</dd>
    <dt>Insanus omnis furere credit.</dt>
    <dd>Сумасшедший считает, что все остальные - безумцы.</dd>
    <dt>Insperata accidit magis saepe quam qua speres.</dt>
    <dd>Неожиданное случается чаще ожидаемого.</dd>
    <dt>Instantia crucis.</dt>
    <dd>Решающее испытание.</dd>
    <dt>Instantia est mater doctrinae.</dt>
    <dd>Упорство - мать науки.</dd>
    <dt>Intelligenti pauca.</dt>
    <dd>Для понимающего достаточно и немногого.</dd>
    <dt>Intempestivq qui docet, ille nocet.</dt>
    <dd>Несвоевременные поучения вредят.</dd>
    <dt>Inter alia.</dt>
    <dd>(сокращ. int.al..)Между прочим.</dd>
    <dt>Inter bonos bene.</dt>
    <dd>Между хорошими - все по-хорошему.</dd>
    <dt>Inter nos.</dt>
    <dd>Между нами.</dd>
    <dt>Inter pares amicitia.</dt>
    <dd>Дружба между равными.</dd>
    <dt>Inter se.</dt>
    <dd>Между собой.</dd>
    <dt>Inter vepres rosae nascuntur.</dt>
    <dd>И среди терновника растут розы.</dd>
    <dt>Inutilis quaestio solvitur silentio.</dt>
    <dd>Неуместный вопрос освобождается от ответа.</dd>
    <dt>Invidia festos dies non agit.</dt>
    <dd>Зависть праздников не соблюдает.</dd>
    <dt>Ipsae res verba rapiunt.</dt>
    <dd>Дело говорит само за себя.</dd>
    <dt>Ipse fecit.</dt>
    <dd>Сделал собственноручно.</dd>
    <dt>Ipsissima verba.</dt>
    <dd>Слово в слово.</dd>
    <dt>Ira furor brevis est.</dt>
    <dd>Гнев - это кратковременное помешательство.</dd>
    <dt>Ira odium gignit, concordia nutrit amorem.</dt>
    <dd>Гнев порождает ненависть, согласие вскармливает любовь.</dd>
    <dt>Irreparabilium felix oblivio rerum</dt>
    <dd>Счастлив не сожалеющий о прошедшем!</dd>
    <dt>Ite, missa est.</dt>
    <dd>Идите, все кончено.</dd>
</div>

<a id="J"><h3>J</h3></a>

<div>
    <dt>Jactantius maerent, quae minus dolent.</dt>
    <dd>Скорбь напоказ - небольшая скорбь.</dd>
    <dt>Januis clausis.</dt>
    <dd>При закрытых дверях.</dd>
    <dt>Jucunda memoria est praeteritorum malorum.</dt>
    <dd>Приятно воспоминание о минувших невзгодах.</dd>
    <dt>Jucundi acti labores.</dt>
    <dd>Приятен оконченный труд.</dd>
    <dt>Juncta juvant.</dt>
    <dd>Единодушие помогает.</dd>
    <dt>Jus gentium.</dt>
    <dd>Международное право.</dd>
    <dt>Jus summam saepe summa malitia est.</dt>
    <dd>Высшее право часто есть высшее зло.</dd>
    <dt>Justitia nihil expetit praemii.</dt>
    <dd>Справедливость не требует никакой награды.</dd>
    <dt>Juvenes plura loquuntur, senes utiliora.</dt>
    <dd>Молодые говорят больше, старки - полезнее.</dd>
    <dt>Juventus ventus.</dt>
    <dd>Молодость ветрена.</dd>
</div>

<a id="L"><h3>L</h3></a>

<div>
    <dt>Labor callum obducit dolori.</dt>
    <dd>Труд притупляет боль.</dd>
    <dt>Labor et patientia omnia vincunt.</dt>
    <dd>Терпение и труд все побеждают.</dd>
    <dt>Labor ineptiarum.</dt>
    <dd>Бессмысленный труд.</dd>
    <dt>Labor non onus, sed beneficium.</dt>
    <dd>Труд не бремя, а благо.</dd>
    <dt>Labor omnia vincit improbus.</dt>
    <dd>Упорный труд все преодолевает.</dd>
    <dt>Laboremus.</dt>
    <dd>Будем трудиться.</dd>
    <dt>Lac gallinaceum.</dt>
    <dd>Птичье молоко.</dd>
    <dt>Laetitia garrula res est.</dt>
    <dd>Радость словоохотлива.</dd>
    <dt>Lapsus calami.</dt>
    <dd>Описка.</dd>
    <dt>Lapsus linguae.</dt>
    <dd>Обмолвка.</dd>
    <dt>Lapsus memoriae.</dt>
    <dd>Ошибка памяти.</dd>
    <dt>Larga manu.</dt>
    <dd>Щедро.</dd>
    <dt>Largitio fundum non habet.</dt>
    <dd>Щедрость не имеет пределов.</dd>
    <dt>Lato sensu.</dt>
    <dd>В широком смысле.</dd>
    <dt>Laudator temporis acti.</dt>
    <dd>Восхвалитель былых времен.</dd>
    <dt>Laudator temporis praesentis.</dt>
    <dd>Восхвалитель современности.</dd>
    <dt>Laus propria sordet.</dt>
    <dd>Похвала в свою пользу непристойна.</dd>
    <dt>Levius fit patientia quidquid corrigere est nefas Что нельзя изменить, то можно облегчить терпением.
    </dd>
    <dt>Libera nos a malo.</dt>
    <dd>Избави нас от зла.</dd>
    <dt>Liberum arbitrium indifferentiae.</dt>
    <dd>Полная свобода выбора.</dd>
    <dt>Litterarum radices amarae, fructus dulces sunt.</dt>
    <dd>Корни науки горьки, плоды - сладки.</dd>
    <dt>Locus minoris resistentiae.</dt>
    <dd>Место наименьшено сопротивления.</dd>
    <dt>Longum iter est per praecepta, breve et efficax per exempla.</dt>
    <dd>Долог путь поучений, краток же и успешен на примерах.</dd>
    <dt>Lucri bonus est odor ex re qualibet.</dt>
    <dd>Запах прибыли приятен, от чего бы он ни исходил.</dd>
    <dt>Lumen mundi.</dt>
    <dd>Светоч мира.</dd>
    <dt>Luna latrantem canem non curat.</dt>
    <dd>Луна не обращает внимания на лай собаки.</dd>
    <dt>Lupus pilum mutat, non mentem.</dt>
    <dd>Волк меняет шкуру, а не душу.</dd>
    <dt>Lusus naturae.</dt>
    <dd>Игра природы.</dd>
    <dt>Lux in tenebris.</dt>
    <dd>Свет во мраке.</dd>
    <dt>Lux veritatis.</dt>
    <dd>Свет истины.</dd>
    <dt>Luxuria inopiae mater.</dt>
    <dd>Роскошь - мать бедность.</dd>
</div>

<a id="M"><h3>M</h3></a>

<div>
    <dt>Macte animo!</dt>
    <dd>Мужайся!</dd>
    <dt>Magister bibendi.</dt>
    <dd>Мастер по части выпивки.</dd>
    <dt>Magna est veritas et praevalebit.</dt>
    <dd>Велика истина, и она восторжествует.</dd>
    <dt>Magni sunt, humanes tamen.</dt>
    <dd>Великие, но люди.</dd>
    <dt>Magnum ignotum.</dt>
    <dd>Великое неведомое.</dd>
    <dt>Mala fide.</dt>
    <dd>Неискренне, нечестно.</dd>
    <dt>Mala herba cito crescit.</dt>
    <dd>Сорная трава растет быстро.</dd>
    <dt>Male facere qui vult, nunquam non causam invenit.</dt>
    <dd>Желающий навредить всегда найдет причину.</dd>
    <dt>Malesuada fames.</dt>
    <dd>Голод - дурной советник.</dd>
    <dt>Mali principii malus finis.</dt>
    <dd>Плохому началу и плохой конец.</dd>
    <dt>Malitia supplet aetatem.</dt>
    <dd>Каждый век имеет свои пороки.</dd>
    <dt>Malo cum Platone errare, quam cum aliis recte sentire.</dt>
    <dd>Лучше ошибаться с Платоном, чем быть правым с другими.</dd>
    <dt>Malum consilium consultori pessimum est.</dt>
    <dd>Дурной умысел оборачивается против того, кто его замыслил.</dd>
    <dt>Malum est consilium, quod mutari non potest.</dt>
    <dd>Плохо то решение, которое нельзя изменить.</dd>
    <dt>Manet omnes una nox.</dt>
    <dd>Всех нас ждет одна и та же ночь.</dd>
    <dt>Mania grandiosa.</dt>
    <dd>Мания величия.</dd>
    <dt>Manifestum non eget probatione.</dt>
    <dd>Очевидное не нуждается в доказательстве.</dd>
    <dt>Manu intrepida.</dt>
    <dd>Недрогнувшей рукой.</dd>
    <dt>Manu propria.</dt>
    <dd>Собственной рукой.</dd>
    <dt>Manus manum lavat.</dt>
    <dd>Рука руку моет.</dd>
    <dt>Mare verborum gutta rerum.</dt>
    <dd>Море слов - капля дел.</dd>
    <dt>Materia tractanda.</dt>
    <dd>Предмет обсуждения.</dd>
    <dt>Maxima egestas avaritia.</dt>
    <dd>Скупость - наибольшая бедность.</dd>
    <dt>Maximis minimisque corporibus par est dolor vulneris.</dt>
    <dd>Боль от раны одинакова и для больших, и для маленьких тел.</dd>
    <dt>Maximum remedium irae mora sunt.</dt>
    <dd>Лучшее лекарство от гнева - время.</dd>
    <dt>Me judice.</dt>
    <dd>По моему суждению.</dd>
    <dt>Mea culpa.</dt>
    <dd>Моя вина.</dd>
    <dt>Mea memoria.</dt>
    <dd>На моей памяти.</dd>
    <dt>Mea mihi conscientia plures est quam omnium sermo.</dt>
    <dd>Моя совесть важнее мне, чем все пересуды.</dd>
    <dt>Medice, cura te ipsum.</dt>
    <dd>Врач, исцелись сам.</dd>
    <dt>Medicus curat, natura sanat.</dt>
    <dd>Врач лечит-природа исцеляет.</dd>
    <dt>Medio flumine quaerere aquam.</dt>
    <dd>Просить воды находясь в реке.</dd>
    <dt>Melioribus annis.</dt>
    <dd>В лучшие времена.</dd>
    <dt>Melius est nomen bonum, quam magnae divitiae.</dt>
    <dd>Лучше честное имя, чем большое богатство.</dd>
    <dt>Melius est prudenter tacere, quam inaniter loqui.</dt>
    <dd>Разумнее смолчать, чем сказать глупость.</dd>
    <dt>Melius est puero flere, quam senes.</dt>
    <dd>Лучше плакать в детстве, чем в старости.</dd>
    <dt>Melius non incipient, quam desinent.</dt>
    <dd>Лучше не начинать, чем останавливаться на полпути.</dd>
    <dt>Memento mori.</dt>
    <dd>Помни о смерти!</dd>
    <dt>Memento patriam.</dt>
    <dd>Помни о родине.</dd>
    <dt>Memoria est exercenda.</dt>
    <dd>Память нужно тренировать.</dd>
    <dt>Mendaci homini ne vera quidem dicenti credimus.</dt>
    <dd>Лжецу не верят даже тогда, когда он говорит правду.</dd>
    <dt>Mendax in uno, mendax in omnibus.</dt>
    <dd>Единожды солгавший, всегда лжет.</dd>
    <dt>Mens sana in corpore sano.</dt>
    <dd>В здоровом теле здоровый дух.</dd>
    <dt>Mens vertitur cum fortuna.</dt>
    <dd>Образ мыслей меняется с изменением положения.</dd>
    <dt>Meo voto.</dt>
    <dd>По моему мнению.</dd>
    <dt>Mihi desunt verba.</dt>
    <dd>У меня нет слов.</dd>
    <dt>Miles gloriosus.</dt>
    <dd>Хвастливый воин.</dd>
    <dt>Minima de malis.</dt>
    <dd>Из двух зол меньшее (выбирать).</dd>
    <dt>Minus habeo, quam speravi, sed fortasse plus speravi, quam debui.</dt>
    <dd>Я получил меньше, чем ожидал, но, возможно, я больше ожидал, чем следовало.</dd>
    <dt>Mixtura verborum.</dt>
    <dd>Словесная мешанина.</dd>
    <dt>Modus cogitandi.</dt>
    <dd>Образ мышления.</dd>
    <dt>Modus dicendi.</dt>
    <dd>Манера выражаться.</dd>
    <dt>Modus operandi.</dt>
    <dd>Способ действия.</dd>
    <dt>Modus vivendi.</dt>
    <dd>Образ жизни.</dd>
    <dt>Mollit viros otium.</dt>
    <dd>Безделье делает людей слабыми.</dd>
    <dt>Montes auri pollicens.</dt>
    <dd>Обещать золотые горы.</dd>
    <dt>Mores cuique sui fingunt fortunam.</dt>
    <dd>Наша судьба зависит от наших нравов.</dd>
    <dt>Mors immortalis.</dt>
    <dd>Бессмертная смерть.</dd>
    <dt>Multi multa sciunt, nemo omnia.</dt>
    <dd>Многие знают многое, но никто не знает всего.</dd>
    <dt>Multi sunt vocati, pauci vero electi.</dt>
    <dd>Много званых, но мало избранных.</dd>
    <dt>Multos timere debet, quem multi timent.</dt>
    <dd>Многих должен бояться тот, кого многие боятся.</dd>
    <dt>Multum legendum est, non multa.</dt>
    <dd>Читать следует не все, но лучшее.</dd>
    <dt>Multum sibi adicit virtus lacessita.</dt>
    <dd>Добродетель возрастает, если ее подвергают испытаниям.</dd>
    <dt>Multum vinum bibere, non diu vivere.</dt>
    <dd>Много пить вина, недолго жить.</dd>
    <dt>Mundus hic est quam optimus.</dt>
    <dd>Этот мир самый лучший.</dd>
    <dt>Mutantur tempora et nos mutamur in illis.</dt>
    <dd>Меняются времена, и мы меняемся с ними.</dd>
    <dt>Mutatis mutandis.</dt>
    <dd>С оговорками; изменив то, что надо изменить.</dd>
</div>

<a id="N"><h3>N</h3></a>

<div>
<dt>Nascentes morimur, finisque ab origine pendet.</dt>
<dd>Рождаясь, мы умираем, и конец обусловлен началом.</dd>
<dt>Natura abhorret vacuum.</dt>
<dd>Природа не терпит пустоты.</dd>
<dt>Natura est semper invicta.</dt>
<dd>Природа всегда непобедима.</dd>
<dt>Natura non facit saltus.</dt>
<dd>Природа не делает скачков.</dd>
<dt>Naturae vis medicatrix.</dt>
<dd>Целительная сила природы.</dd>
<dt>Naturalia non sunt turpia.</dt>
<dd>Естественное не безобразно.</dd>
<dt>Ne accesseris in consilium nisi vocatus.</dt>
<dd>Не ходи в совет, не будучи приглашенным.</dd>
<dt>Ne cede malis.</dt>
<dd>Не отступай перед несчастьями.</dd>
<dt>Ne differas in crastinum.</dt>
<dd>Не откладывай на завтра.</dd>
<dt>Ne malum alienum feceris tuum gaudium.</dt>
<dd>Не радуйся чужому несчастью.</dd>
<dt>Ne quid nimis.</dt>
<dd>Ничего лишнего.</dd>
<dt>Ne sit vitiosus sermo nutricibus.</dt>
<dd>Не должно быть пороков в речи тех, кто воспитывает.</dd>
<dt>Ne varietur.</dt>
<dd>Изменению не подлежит.</dd>
<dt>Nec plus ultra.</dt>
<dd>Дальше некуда.</dd>
<dt>Nec sibi, nec alteri.</dt>
<dd>Ни себе, ни другому.</dd>
<dt>Nemine contradicente.</dt>
<dd>Единогласно.</dd>
<dt>Neminem laedere.</dt>
<dd>Никому не вреди.</dd>
<dt>Neminem metuit innocens.</dt>
<dd>Невиновный не боится никого.</dd>
<dt>Neminem pecunia divitem fecit.</dt>
<dd>Деньги никому не приносят счастья.</dd>
<dt>Nemo amat, quos timet.</dt>
<dd>Никто не любит тех, кого боится.</dd>
<dt>Nemo liber est, qui corpori servit.</dt>
<dd>Тот невольник, кто раб своего тела.</dd>
<dt>Nemo mortalium omnibus horis sapis.</dt>
<dd>Никто не бывает мудрым всегда.</dd>
<dt>Nemo nascitur sapiens, sed fit.</dt>
<dd>Мудрым не рождаются, им становятся.</dd>
<dt>Nemo potest regere, nisi patiens.</dt>
<dd>Не набраться ума без терпения.</dd>
<dt>Nemo sine vitiis est.</dt>
<dd>Никто не лишен пороков.</dd>
<dt>Nescit vox missa reverti.</dt>
<dd>Сказанное слово не возвращается.</dd>
<dt>Nihil agendi dies est longus.</dt>
<dd>Долог день для ничего не делающего.</dd>
<dt>Nihil agendo male agere discimus.</dt>
<dd>Ничего не делая, мы учимся делать плохое.</dd>
<dt>Nihil aliud curo, quam ut bene vivam.</dt>
<dd>Ни о чем так не забочусь, как прожить достойно.</dd>
<dt>Nihil est difficile volenti.</dt>
<dd>Ничто не трудно для желающего.</dd>
<dt>Nihil est in religione, quod non fuerit in vita.</dt>
<dd>Нет ничего в религии, чего не было бы в жизни.</dd>
<dt>Nihil est incertus vulgo.</dt>
<dd>Нет ничего более непостоянного, чем толпа.</dd>
<dt>Nihil est jucundius lectulo domestico.</dt>
<dd>Нет ничего милее домашнего очага.</dd>
<dt>Nihil est pejus amico falso.</dt>
<dd>Ничего нет хуже ложного друга.</dd>
<dt>Nihil habeo, nihil curo.</dt>
<dd>Ничего не имею - ни о чем не забочусь.</dd>
<dt>Nil actum credens, dum quid superesset agendum.</dt>
<dd>Если осталось еще что-либо доделать, считай, что ничего не сделано.</dd>
<dt>Nil de nihilo fit.</dt>
<dd>Ничто не возникает из ничего.</dd>
<dt>Nil desperandum.</dt>
<dd>Никогда не отчаивайся.</dd>
<dt>Nil mortalibus arduum est.</dt>
<dd>Нет ничего недосягаемого.</dd>
<dt>Nil nisi bene.</dt>
<dd>Ничего, кроме хорошего.</dd>
<dt>Nil permanet sub sole.</dt>
<dd>Ничто не вечно под солнцем.</dd>
<dt>Nil sine magno vita labore dedit mortalibus.</dt>
<dd>Жизнь ничего не дает без упорного труда.</dd>
<dt>Nil volenti difficile est.</dt>
<dd>При желании все преодолимо.</dd>
<dt>Nimium altercando veritas amittitur.</dt>
<dd>В чрезмерном споре теряется истина.</dd>
<dt>Nimium ne crede colori.</dt>
<dd>Не слишком полагайся на внешний вид.</dd>
<dt>Nisi utile est quod facimus stulta est gloria.</dt>
<dd>Неразумно рвение, если бесполезно то, что мы делаем.</dd>
<dt>Nolens volens.</dt>
<dd>Волей-неволей.</dd>
<dt>Noli nocere.</dt>
<dd>Не навреди.</dd>
<dt>Nomen est omen.</dt>
<dd>Имя говорит само за себя.</dd>
<dt>Nomina sunt odiosa.</dt>
<dd>Об именах лучше умолчать.</dd>
<dt>Nomine et re.</dt>
<dd>На словах и на деле.</dd>
<dt>Non aliter vives in solitudine, aliter in foro.</dt>
<dd>Будь одинаков и наедине, и на людях.</dd>
<dt>Non annumerare verba sed appendere.</dt>
<dd>Слова следует не считать, а взвешивать.</dd>
<dt>Non bene pro toto libertas venditur auro.</dt>
<dd>Позорно продавать свободу за золото.</dd>
<dt>Non bis in idem.</dt>
<dd>Не дважды за одно и то же.</dd>
<dt>Non compos mentis.</dt>
<dd>Не в здравом уме.</dd>
<dt>Non de ponte cadit, qui cum sapientia vadit.</dt>
<dd>Не падает с моста тот, кто умно ходит.</dd>
<dt>Non esse cupidum pecunia est, non esse emacem vectigal est.</dt>
<dd>Не быть жадным - уже богатство, не быть расточительным - доход.</dd>
<dt>Non est culpa vini, sed culpa bibentis.</dt>
<dd>Виновато не вино, а пьющие.</dd>
<dt>Non est fumus absque igne.</dt>
<dd>Нет дыма без огня.</dd>
<dt>Non foliis, sed fructu arborem aestima.</dt>
<dd>Оценивай дерево по плоду, а не по листьям.</dd>
<dt>Non genus virum ornat, generi vir fortis loco.</dt>
<dd>Не происхождение украшает человека, а человек происхождение.</dd>
<dt>Non habet eventus sordida praeda bonos.</dt>
<dd>Нечестно приобретенное теряется попусту.</dd>
<dt>Non in loco ridere pergrave est malum.</dt>
<dd>Смеяться не вовремя - очень большой порок.</dd>
<dt>Non in omnes omnia conveniunt.</dt>
<dd>Не все подходит всем.</dd>
<dt>Non multum, sed multa.</dt>
<dd>Не много, но многое.</dd>
<dt>Non numeranda, sed ponderanda argumenta.</dt>
<dd>Ценно не количество, а качество доказательств.</dd>
<dt>Non omne est aurum, quod splendet.</dt>
<dd>Не все золото, что блестит.</dd>
<dt>Non omne quod licet honestum est.</dt>
<dd>Не все дозволенное достойно уважения.</dd>
<dt>Non omnia possumus omnes.</dt>
<dd>Не все мы на все способны.</dd>
<dt>Non omnis error stultitia est dicenda.</dt>
<dd>Не каждую ошибку следует называть глупостью.</dd>
<dt>Non omnis moriar.</dt>
<dd>Не весь я умру.</dd>
<dt>Non progredi est regredi.</dt>
<dd>Не продвигаться - значит отступать.</dd>
<dt>Non qui parum habet, sed qui plus cupit, pauper est.</dt>
<dd>Бедняк не тот, кто меньше имеет, а тот, кто большего хочет.</dd>
<dt>Non rebus me, sed mihi submittere conor.</dt>
<dd>Подчиняй вещи себе, а не себя вещам.</dd>
<dt>Non scholae, vitae discimus.</dt>
<dd>Мы учимся не для школы, но для жизни.</dd>
<dt>Non semper errar fama.</dt>
<dd>Не всегда ошибается молва.</dd>
<dt>Non sum qualis eram.</dt>
<dd>Уж не тот я, каким был.</dd>
<dt>Non tutae sunt cum regibus facetiae.</dt>
<dd>Шутки с царями небезопасны.</dd>
<dt>Non verbis sed actis.</dt>
<dd>Не на словах, а на деле.</dd>
<dt>Non, si male nunc, et olim erit.</dt>
<dd>Если плохо сейчас - не всегда же так будет.</dd>
<dt>Nondum adesse fatalem horam.</dt>
<dd>Еще не наступило роковое время.</dd>
<dt>Noscitur a sociis.</dt>
<dd>Узнаешь человека по его товарищам.</dd>
<dt>Nota bene.</dt>
<dd>Обрати внимание.</dd>
<dt>Novarum rerum cupidus.</dt>
<dd>Страстно жаждущий нового.</dd>
<dt>Novos amicos dum paras, veteres cole.</dt>
<dd>Новых друзей приобретай, а старых не забывай.</dd>
<dt>Novus rex.nova lex.</dt>
<dd>Новый царь, новый закон.</dd>
<dt>Nox cogitationum mater.</dt>
<dd>Ночь - мать мыслей.</dd>
<dt>Nox fert consilium.</dt>
<dd>Ночь приносит совет.</dd>
<dt>Nudis verbis.</dt>
<dd>Голословно.</dd>
<dt>Nulla aetas ad discendum sera.</dt>
<dd>Учиться никогда не поздно.</dd>
<dt>Nulla dies sine linia.</dt>
<dd>Ни дня без строчки.</dd>
<dt>Nulla fere causa est, in qua non femina litem moverit.</dt>
<dd>Нет такого дела, в котором не затеяла бы спор женщина.</dd>
<dt>Nulla regula sine exceptione, sed exceptio non impedit regulam.</dt>
<dd>Нет правила без исключения, но исключение не мешает правилу.</dd>
<dt>Nulla tempestas magna perdurat.</dt>
<dd>Большая буря не бывает продолжительной.</dd>
<dt>Nullis amor est sanabilis herbis.</dt>
<dd>Нет лекарства от любви.</dd>
<dt>Nullum malum sine aliquo bono.</dt>
<dd>Нет худа без добра.</dd>
<dt>Nullum periculum sine periculo vincitur.</dt>
<dd>Никакая опасность не преодолевается без риска.</dd>
<dt>Nullum verum infert falsum.</dt>
<dd>Никакая истина не ведет ко лжи.</dd>
<dt>Nullus est liber tam malus, ut non aliqua parte prosit.</dt>
<dd>Нет такой плохой книги, которая была бы совершенно бесполезной.</dd>
<dt>Nummis praestat carere quam amicis.</dt>
<dd>Лучше быть без денег, чем без друзей.</dd>
<dt>Nunc aut nunquam.</dt>
<dd>Теперь или никогда.</dd>
<dt>Nunc et in saecula.</dt>
<dd>Ныне и навеки.</dd>
<dt>Nusquam est qui ubique est.</dt>
<dd>Кто везде, тот нигде.</dd>
<dt>Nusquam nec malum malo, nec vulnus curatur vulnere.</dt>
<dd>Зло не лечат злом, а рану раной.</dd>
<dt>Nutritur vento, vento restinguitur ignis.</dt>
<dd>Огонь ветром поддерживается, от ветра и гаснет.</dd>
</div>

<a id="O"><h3>O</h3></a>

<div>
    <dt>O diem praeclarum</dt>
    <dd>О славный день!</dd>
    <dt>O fallacem hominum spem.</dt>
    <dd>О обманчивая человеческая надежда.</dd>
    <dt>O miratores</dt>
    <dd>О поклонники!</dd>
    <dt>O nomen dulce libertas</dt>
    <dd>О сладкое слово свобода!</dd>
    <dt>O pessimum periculum, quod opertum latet.</dt>
    <dd>Страшней всего скрытая опасность.</dd>
    <dt>O quantum est in rebus inane</dt>
    <dd>О сколько в делах пустого!</dd>
    <dt>O sancta simplicitas.</dt>
    <dd>О святая простота!</dd>
    <dt>O tempora, o mores</dt>
    <dd>О времена, о нравы!</dd>
    <dt>Obiter dictum.</dt>
    <dd>Сказанное мимоходом.</dd>
    <dt>Obscurum per obscurius.</dt>
    <dd>Неясное еще более неясным (объяснять).</dd>
    <dt>Occasio aegre offertur, facile amittitur.</dt>
    <dd>Удобный случай редко представляется, но легко теряется.</dd>
    <dt>Oculis non manibus.</dt>
    <dd>Для глаз, не для рук!
    <dt>Omen bonum.</dt>
    <dd>Счастливое предзнаменование.</dd>
    <dt>Omne exit in fumo.</dt>
    <dd>Все пошло дымом.</dd>
    <dt>Omne ignotum pro magnifico.</dt>
    <dd>Все неизвестное кажется великим.</dd>
    <dt>Omne initium difficile est.</dt>
    <dd>Всякое начало трудно.</dd>
    <dt>Omne nimium nocet.</dt>
    <dd>Всякое излишество вредит.</dd>
    <dt>Omne verum omni vero consonat.</dt>
    <dd>Все истины согласны одна с другой.</dd>
    <dt>Omne vitium semper habet patrocinium.</dt>
    <dd>Любой порок находит себе оправдание.</dd>
    <dt>Omnes quantum potest juva.</dt>
    <dd>Всем, сколько можешь, помогай.</dd>
    <dt>Omnes salvos volo.</dt>
    <dd>Желаю всем здоровья.</dd>
    <dt>Omnes una manet nox.</dt>
    <dd>Всех ожидает одна ночь.</dd>
    <dt>Omni casu.</dt>
    <dd>Во всех случаях.</dd>
    <dt>Omnia fert aetas animum quoque.</dt>
    <dd>Годы уносят все, даже память.</dd>
    <dt>Omnia mea mecum porto.</dt>
    <dd>Все свое ношу с собой.</dd>
    <dt>Omnia moderata aeterna.</dt>
    <dd>Все умеренное долговечно.</dd>
    <dt>Omnia mors aequat.</dt>
    <dd>Для смерти все равны.</dd>
    <dt>Omnia mutantur, nihil interit.</dt>
    <dd>Все меняется, ничего не пропадает.</dd>
    <dt>Omnia mutantur, nos et mutamur in illis.</dt>
    <dd>Все меняется, и мы меняемся.</dd>
    <dt>Omnia non pariter rerum sunt omnibus apta.</dt>
    <dd>Не всем одно и то же одинаково полезно.</dd>
    <dt>Omnia praeclara rara.</dt>
    <dd>Все прекрасное редко.</dd>
    <dt>Omnia tempus revelat.</dt>
    <dd>Время разоблачает все.</dd>
    <dt>Omnia vanitas.</dt>
    <dd>Все суета.</dd>
    <dt>Omnia vincit amor, et nos cedamus amori.</dt>
    <dd>Все побеждает любовь, и мы покоряемся любви.</dd>
    <dt>Omnibus rebus.</dt>
    <dd>Обо всем.</dd>
    <dt>Omnis comparatio claudicat.</dt>
    <dd>Всякое сравнение хромает.</dd>
    <dt>Omnis imitatio ficta est.</dt>
    <dd>Всякое подражание неестественно.</dd>
    <dt>Omnium rerum vicissitudo est.</dt>
    <dd>Все подлежит изменению.</dd>
    <dt>Opera et studio.</dt>
    <dd>Трудом и старанием.</dd>
    <dt>Operae officiales.</dt>
    <dd>Служебные дела.</dd>
    <dt>Oportet vevere.</dt>
    <dd>Надо жить.</dd>
    <dt>Optima fide.</dt>
    <dd>С полным доверием.</dd>
    <dt>Optimum medicamentum quies est.</dt>
    <dd>Лучшее лекарство - покой.</dd>
    <dt>Optimus mundus.</dt>
    <dd>Лучший из миров.</dd>
    <dt>Otia dant vitia.</dt>
    <dd>Праздность рождает порок.</dd>
    <dt>Otium post negotium.</dt>
    <dd>Отдых после дела.</dd>
    <dt>Otium sine litteris mors est et hominis vivi sepultura.</dt>
    <dd>Досуг без книги - это смерть и погребение заживо.</dd>
</div>

<a id="P"><h3>P</h3></a>

<div>
<dt>Pabulum animi.</dt>
<dd>Пища души.</dd>
<dt>Pacta sunt servanda.</dt>
<dd>Договоры должны соблюдаться.</dd>
<dt>Par praemium labori.</dt>
<dd>Соответственно труду и вознограждение.</dd>
<dt>Parcus discordat avaro.</dt>
<dd>Бережливый - не значит скупой.</dd>
<dt>Pars pro toto.</dt>
<dd>Часть вместо целого.</dd>
<dt>Pars sanitatis velle sanari fuit.</dt>
<dd>Желание выздороветь - часть выздоровления.</dd>
<dt>Parva domus, magna quies.</dt>
<dd>Малое жилище - великий покой.</dd>
<dt>Parva domus, parva cura.</dt>
<dd>Малое хозяйство - малая забота.</dd>
<dt>Parva leves capiunt animos.</dt>
<dd>Мелочи прельщают легкомысленных.</dd>
<dt>Parva saepe scintilla contempta magnum excitat incendium.</dt>
<dd>Часто маленькая искра вызывает большой пожар.</dd>
<dt>Patientia patitur omnia.</dt>
<dd>Терпение все побеждает.</dd>
<dt>Patriae fumus igne alieno luculentior.</dt>
<dd>Дым отечества ярче огня чужбины.</dd>
<dt>Pauca sed bona.</dt>
<dd>Мало, но хорошо.</dd>
<dt>Paucis verbis.</dt>
<dd>В немногих словах.</dd>
<dt>Paulatim summa petuntur.</dt>
<dd>Не сразу достигаются вершины.</dd>
<dt>Paupertas non est probrum.</dt>
<dd>Бедность не порок.</dd>
<dt>Pax optima rerum.</dt>
<dd>Самое ценное - мир.</dd>
<dt>Pax tecum.</dt>
<dd>Мир с тобой.</dd>
<dt>Pax vobiscum.</dt>
<dd>Мир с вами.</dd>
<dt>Pecunia est ancilla, si scis uti;si nescis, domina.</dt>
<dd>Умеешь пользоваться деньгами - они служат тебе, а если нет - ты им.</dd>
<dt>Pedibus timor addit alas.</dt>
<dd>Страх придает ногам силы.</dd>
<dt>Pelle sub agnina latitat mens saepe lupina.</dt>
<dd>Под шкурой ягненка часто скрывается нрав волка.</dd>
<dt>Per aspera ad astra.</dt>
<dd>Через тернии - к звездам.</dd>
<dt>Per crucem ad lucem.</dt>
<dd>Через страдания к свету.</dd>
<dt>Per exclusionem.</dt>
<dd>В виде исключения.</dd>
<dt>Per expressum.</dt>
<dd>Дословно; буквально.</dd>
<dt>Per fas et nefas.</dt>
<dd>Правдами и неправдами.</dd>
<dt>Per procura.</dt>
<dd>По доверенности.</dd>
<dt>Per risum multum cognoscimus stultum.</dt>
<dd>По частому смеху узнаем глупца.</dd>
<dt>Per secreta vota.</dt>
<dd>Закрытым голосованием.</dd>
<dt>Per tacitum consensum.</dt>
<dd>По молчаливому согласию.</dd>
<dt>Per usum.</dt>
<dd>На практике.</dd>
<dt>Perfer et obdura.</dt>
<dd>Терпи и крепись.</dd>
<dt>Perfice te</dt>
<dd>Совершенствуй себя!</dd>
<dt>Periculum est in mora.</dt>
<dd>Опасность в промедлении.</dd>
<dt>Perigrinatio est vita.</dt>
<dd>Жизнь - это странствие.</dd>
<dt>Perpetuum mobile.</dt>
<dd>Вечный двигатель.</dd>
<dt>Persona grata.</dt>
<dd>Желательная особа.</dd>
<dt>Persona ingrata.</dt>
<dd>Нежелательная особа.</dd>
<dt>Persona sacrosancta.</dt>
<dd>Священная особа.</dd>
<dt>Persona suspecta.</dt>
<dd>Подозрительная личность.</dd>
<dt>Pia desideria.</dt>
<dd>Благие пожелания.</dd>
<dt>Pia fraus.</dt>
<dd>Благочестивая ложь.</dd>
<dt>Pigritia mater vitiorum.</dt>
<dd>Праздность - мать пороков.</dd>
<dt>Plerique ubi aliis maledicunt, faciunt convicium sibi.</dt>
<dd>Злословя, люди чернят самих себя.</dd>
<dt>Plures crapula quam gladius.</dt>
<dd>Пьянство губит сильнее меча.</dd>
<dt>Pluris est ocultus testis unus, quam auriti decem.</dt>
<dd>Лучше один раз увидеть, чем десять раз услышать.</dd>
<dt>Plus exempla quam peccata nocent.</dt>
<dd>Плохие примеры хуже ошибок.</dd>
<dt>Plus sonat, quam valet.</dt>
<dd>Больше звона, чем смысла.</dd>
<dt>Plus stricto mendax offendit lingua mucrone.</dt>
<dd>Язык лгуна сильнее обнаженного меча.</dd>
<dt>Plusve minusve.</dt>
<dd>Более-менее.</dd>
<dt>Pompa mortis magis terret quam mors ipsa.</dt>
<dd>Больше самой смерти устрашает то, что ее сопровождает.</dd>
<dt>Possum falli ut homo.</dt>
<dd>Как человек я могу ошибаться.</dd>
<dt>Post factum.</dt>
<dd>После совершившегося (факта).</dd>
<dt>Post hoc, non est propter hoc.</dt>
<dd>После этого не значит, что вследствие этого.</dd>
<dt>Post hominum memoriam.</dt>
<dd>С незапамятных времен.</dd>
<dt>Post mortem medicina.</dt>
<dd>После смерти медицина.</dd>
<dt>Post nubila sol.</dt>
<dd>После туч - солнце.</dd>
<dt>Post scriptum.</dt>
<dd>Постскриптум, приписка к письму.</dd>
<dt>Post tenebras lux.</dt>
<dd>После мрака свет.</dd>
<dt>Postumus.</dt>
<dd>Самый последний.</dd>
<dt>Potior visa est periculosa libertas quieto servitio.</dt>
<dd>Свобода в опасности лучше рабства в покое.</dd>
<dt>Potius sere, quam numquam.</dt>
<dd>Лучше поздно, чем никогда.</dd>
<dt>Praemonitus praemunitus.</dt>
<dd>Кто предупрежден, тот вооружен.</dd>
<dt>Praesentia minuit famam.</dt>
<dd>Присутствие уменьшает славу.</dd>
<dt>Praestat aliquid superesse quam deesse.</dt>
<dd>Лучше пусть останется, чем не хватит.</dd>
<dt>Praestat cum dignitate cadere quam cum ignominia vivere.</dt>
<dd>Лучше погибнуть с честью, чем жить в бесчестьи.</dd>
<dt>Praeterita mutare non possumus.</dt>
<dd>Мы не можем изменить прошлого.</dd>
<dt>Praeterita omittamus.</dt>
<dd>Давайте не будем говорить о прошлом.</dd>
<dt>Praevenire melius est, quam praeveniri.</dt>
<dd>Лучше опередить, чем опередят тебя.</dd>
<dt>Presente medico nihil nocet.</dt>
<dd>В присутствии врача ничего не вредно.</dd>
<dt>Prima facie.</dt>
<dd>На первый взгляд.</dd>
<dt>Primum discere, deinde docere.</dt>
<dd>Сначала учиться, потом учить.</dd>
<dt>Primum non nocere.</dt>
<dd>Прежде всего - не вредить.</dd>
<dt>Primum vivere.</dt>
<dd>Прежде всего - жить.</dd>
<dt>Primus clamor atque impetus rem decernit.</dt>
<dd>Первый натиск и первые крики решают дело.</dd>
<dt>Primus inter pares.</dt>
<dd>Первый среди равных.</dd>
<dt>Principes mortales, respublica aeterna.</dt>
<dd>Правители смертны, государство вечно.</dd>
<dt>Principium contradictionis.</dt>
<dd>Закон противоречия.</dd>
<dt>Principium dimidium totius.</dt>
<dd>Начало - половина всего.</dd>
<dt>Prius quam exaudis, ne judices.</dt>
<dd>Не суди, не выслушав.</dd>
<dt>Prius quam incipias, consulto opus est.</dt>
<dd>Прежде чем начать, обдумай.</dd>
<dt>Privata publicis postpone</dt>
<dd>Предпочитай общественное личному!</dd>
<dt>Privatim.</dt>
<dd>Частным образом.</dd>
<dt>Pro bono publico.</dt>
<dd>Для общей пользы.</dd>
<dt>Pro domo sua.</dt>
<dd>В своих интересах.</dd>
<dt>Pro et contra.</dt>
<dd>За и против.</dd>
<dt>Pro forma.</dt>
<dd>Ради формы.</dd>
<dt>Pro mundi beneficio.</dt>
<dd>На благо мира.</dd>
<dt>Pro patria et libertate.</dt>
<dd>За отечество и свободу.</dd>
<dt>Pro re nata.</dt>
<dd>Согласно обстоятельствам.</dd>
<dt>Pro tanto.</dt>
<dd>Пропорционально.</dd>
<dt>Pro tempore.</dt>
<dd>Вовремя; своевременно.</dd>
<dt>Probatum est.</dt>
<dd>Одобрено.</dd>
<dt>Proceras deicit arbores procella vehemens.</dt>
<dd>Сильная буря валит даже высокие деревья.</dd>
<dt>Procul dubio.</dt>
<dd>Без сомнения.</dd>
<dt>Procul ex oculis, procul ex mente.</dt>
<dd>С глаз долой - из головы вон.</dd>
<dt>Procul negotiis.</dt>
<dd>Вдали от суеты.</dd>
<dt>Pulsate et aperietur vobis.</dt>
<dd>Стучите, и вам откроют.</dd>
<dt>Punctum quaestionis.</dt>
<dd>Суть вопроса.</dd>
</div>

<a id="Q"><h3>Q</h3></a>

<div>
    <dt>QUi gladio ferit, gladio perit.</dt>
    <dd>Кто придет с мечом - от меча и погибнет.</dd>
    <dt>Quae fuerunt vicia, mores sunt.</dt>
    <dd>Что было пороками, теперь нравы.</dd>
    <dt>Quae non posuisti, ne tollas</dt>
    <dd>Чего не положил, того не бери!</dd>
    <dt>Quaerite et inveniete.</dt>
    <dd>Ищите и обрящите.</dd>
    <dt>Quaestio vixata.</dt>
    <dd>Мучительный вопрос.</dd>
    <dt>Quale opus est, tale est praemium.</dt>
    <dd>Каков труд, таково и вознаграждение.</dd>
    <dt>Quam semel errare, melius terve rogare.</dt>
    <dd>Лучше три раза спросить, чем один раз ошибиться.</dd>
    <dt>Quanta patimur.</dt>
    <dd>Сколько претерпеваем.</dd>
    <dt>Quantum satis.</dt>
    <dd>Вдоволь.</dd>
    <dt>Quem medicamenta non sanant, natura sanat.</dt>
    <dd>Если не лечат лекарства, вылечит природа.</dd>
    <dt>Qui multum habet, plus cupit.</dt>
    <dd>Имеющий многое хочет еще большего.</dd>
    <dt>Qui nescit tacere, nescit et loqui.</dt>
    <dd>Кто не умеет молчать, не умеет и разговаривать.</dd>
    <dt>Qui nimis propere, minus prospere.</dt>
    <dd>Кто слишком спешит, тот терпит неудачу.</dd>
    <dt>Qui nimium properat, serus absolvit.</dt>
    <dd>Кто слишком спешит, тот позже завершит.</dd>
    <dt>Qui nimium probat, nihil probat.</dt>
    <dd>Кто доказывает слишком много, тот ничего не доказывает.</dd>
    <dt>Qui non nobiscum, adversus nos est.</dt>
    <dd>Кто не с нами, тот против нас.</dd>
    <dt>Qui non proficit, deficit.</dt>
    <dd>Кто не движется вперед, тот отстает.</dd>
    <dt>Qui potest capere capiat.</dt>
    <dd>Кто может решать, пусть решает.</dd>
    <dt>Qui potest consilium fugere, sapere idem potest.</dt>
    <dd>Умный тот, кто может обойтись без совета.</dd>
    <dt>Qui pro quo.</dt>
    <dd>Путаница.</dd>
    <dt>Qui quae vult dicit, quae non vult audiet.</dt>
    <dd>Кто говорит, что хочет, услышит, чего и не хочет.</dd>
    <dt>Qui quaerit, inveniet, pulsanti aperietur.</dt>
    <dd>Кто ищет, тот найдет, кто стучится, тому откроют.</dd>
    <dt>Qui scribit, bis legit.</dt>
    <dd>Кто записывает, дважды читает.</dd>
    <dt>Qui seminat mala, metet mala.</dt>
    <dd>Кто сеет зло, тот зло и пожнет.</dd>
    <dt>Qui sibi semitam non sapiunt, alteri non monstrant viam.</dt>
    <dd>Кто себе тропу не проложит, тот никому не укажет путь.</dd>
    <dt>Qui sine peccato est.</dt>
    <dd>Кто без греха.</dd>
    <dt>Qui ventum seminat, turbinem metet.</dt>
    <dd>Кто сеет ветер, тот пожнет бурю.</dd>
    <dt>Quibuscumque viis.</dt>
    <dd>Любыми путями.</dd>
    <dt>Quid potui, feci.</dt>
    <dd>Что мог, я сделал.</dd>
    <dt>Quidquid agis, prudenter agas et respice finem.</dt>
    <dd>Что бы ты ни делал, делай разумно и имей в виду результат.</dd>
    <dt>Quidquid latet apparebit.</dt>
    <dd>Тайное становится явным.</dd>
    <dt>Quidquid praecipies, esto brevis.</dt>
    <dd>Чему бы ты ни учил, будь краток.</dd>
    <dt>Quidquid vides, currit cum tempore.</dt>
    <dd>Все изменяется со временем.</dd>
    <dt>Quisque suos patimur manes.</dt>
    <dd>Каждый несет свою кару.</dd>
    <dt>Quo quisque est doctor, eo est modestior.</dt>
    <dd>Насколько человек образован - настолько он скромен.</dd>
    <dt>Quo timoris minus est, eo minus ferme periculi est.</dt>
    <dd>Чем меньше страх, тем меньше опасность.</dd>
    <dt>Quo vadis.?</dt>
    <dd>Куда идешь?.</dd>
    <dt>Quod cito fit, cito perit.</dt>
    <dd>Что скоро делается, то скоро и гибнет.</dd>
    <dt>Quod dubitas, ne feceris</dt>
    <dd>Сомневаешься - не делай</dd>
    <dt>Quod erat demonstrandum (q.e.d..)</dt>
    <dd>Что и требовалось доказать.</dd>
    <dt>Quod erat faciendum.</dt>
    <dd>Что и следовало сделать.</dd>
    <dt>Quod hodie non est, id cras erit.</dt>
    <dd>Чего нет сегодня, будет завтра.</dd>
    <dt>Quod in corde sobrii, id in ore ebrii.</dt>
    <dd>Что у трезвого на уме, то у пьяного на языке.</dd>
    <dt>Quod nego.</dt>
    <dd>Отнюдь нет.</dd>
    <dt>Quod non est in actis, non est in mundo.</dt>
    <dd>Чего нет в документах, того нет и на свете.</dd>
    <dt>Quod non licet feminis, aeque non licet viris.</dt>
    <dd>Что не позволено женщинам, так же не позволено мужчинам.</dd>
    <dt>Quod non licet, acrius urit.</dt>
    <dd>Недозволенное притягивает сильнее.</dd>
    <dt>Quod probe notandum.</dt>
    <dd>Что следует хорошо заметить.</dd>
    <dt>Quod satis est, plus quam satis est.</dt>
    <dd>Чего досточно, того уже слишком.</dd>
    <dt>Quod tibi fieri non vis, alteri ne feceris.</dt>
    <dd>Не делай другому того, чего не желаешь себе.</dd>
    <dt>Quod tollere velles.</dt>
    <dd>Что следует вычеркнуть.</dd>
    <dt>Quod uni dixeris, omnibus dixeris.</dt>
    <dd>Сказать одному - значит сказать всем.</dd>
    <dt>Quos ego</dt>
    <dd>Я вас!</dd>
    <dt>Quot capita, tot sensus.</dt>
    <dd>Сколько голов - столько умов.</dd>
    <dt>Quot homines tot sententiae.</dt>
    <dd>Сколько людей, столько и мнений.</dd>
    <dt>Quovis modo.</dt>
    <dd>Во что бы то ни стало.</dd>
</div>

<a id="R"><h3>R</h3></a>

<div>
    <dt>Radices litterarum amarae sunt, fructus dulces.</dt>
    <dd>Корни наук горьки, плоды сладки.</dd>
    <dt>Rara avis.</dt>
    <dd>Редкая птица.</dd>
    <dt>Re succumbere non oportebat verbis gloriantem.</dt>
    <dd>Не следовало сдаваться на деле, если на словах был героем.</dd>
    <dt>Re, non verbis.</dt>
    <dd>Делом, а не словами.</dd>
    <dt>Rebus dictantibus.</dt>
    <dd>Под диктовку вещей.</dd>
    <dt>Rebus in adversis melius sperare memento.</dt>
    <dd>В несчастье надейся на лучшее.</dd>
    <dt>Recte facti fecisse merces est.</dt>
    <dd>Наградой за доброе дело служит его свершение.</dd>
    <dt>Recto tono.</dt>
    <dd>Спокойным голосом.</dd>
    <dt>Redde, quod debes.</dt>
    <dd>Отдай, что должен.</dd>
    <dt>Reductio ad absurdum.</dt>
    <dd>Доведение до нелепости.</dd>
    <dt>Rem verba sequentur.</dt>
    <dd>За словом - дело.</dd>
    <dt>Repetitio est mater studiorum.</dt>
    <dd>Повторение - мать учения.</dd>
    <dt>Reprehensio calumnia vacare debet.</dt>
    <dd>Критика должна быть свободной от клеветы.</dd>
    <dt>Rerum cognoscere causas.</dt>
    <dd>Познавать причины вещей.</dd>
    <dt>Rerum natura nullam nobis dedit cognitionem finium.</dt>
    <dd>Самой природой не дано нам познать пределы вещей.</dd>
    <dt>Rerum novarum studium.</dt>
    <dd>Стремление к новизне.</dd>
    <dt>Res ipsa loquitur.</dt>
    <dd>Дело само говорит.</dd>
    <dt>Res omnis aetatis.</dt>
    <dd>Дело всей жизни.</dd>
    <dt>Res tua agitur.</dt>
    <dd>Речь о тебе.</dd>
    <dt>Restitutio ad integrum.</dt>
    <dd>Полное восстановление.</dd>
    <dt>Reti ventos venari.</dt>
    <dd>Сетью ловить ветер.</dd>
    <dt>Ridendo dicere severum.</dt>
    <dd>Смеясь говорить о серьезном.</dd>
    <dt>Risu emorior.</dt>
    <dd>Умираю от смеха.</dd>
    <dt>Risu inepto res ineptior nulla est.</dt>
    <dd>Нет ничего глупее глупого смеха.</dd>
</div>

<a id="S"><h3>S</h3></a>

<div>
<dt>Sacrae ruinae.</dt>
<dd>Священные руины.</dd>
<dt>Saeculi vitia, non hominis.</dt>
<dd>Недостатки века, а не человека.</dd>
<dt>Saeculorum novus nascitur ordo.</dt>
<dd>Время рождает новый порядок.</dd>
<dt>Saepe est sub pallio sordido sapientia.</dt>
<dd>Часто под грязным рубищем скрывается мудрость.</dd>
<dt>Saepe mora est melior.</dt>
<dd>Часто промедление полезно.</dd>
<dt>Saepe summa ingenia in occulto latent.</dt>
<dd>Часто большие таланты скрыты.</dd>
<dt>Sagitta interdum resiliens percuit dirigentem.</dt>
<dd>Иногда отраженная стрела убивает выпустившего ее.</dd>
<dt>Salus patriae suprema lex.</dt>
<dd>Благо отечества - высший закон.</dd>
<dt>Salus populi suprema lex.</dt>
<dd>Благо народа - высший закон.</dd>
<dt>Salutis gratia.</dt>
<dd>Для безопасности.</dd>
<dt>Salvavi animam meam.</dt>
<dd>Я спас свою душу.</dd>
<dt>Salve</dt>
<dd>Здравствуй</dd>
<dt>Salvo honore.</dt>
<dd>Без вреда для чести.</dd>
<dt>Sancta sanctorum.</dt>
<dd>Святая святых.</dd>
<dt>Sapere aude.</dt>
<dd>Решись на мудрость.</dd>
<dt>Sapiens animus numquam tumet.</dt>
<dd>Умный никогда не зазнается.</dd>
<dt>Sapiens bonum fert modice, fortiter malum.</dt>
<dd>Мудрый переносит счастье сдержанно, а несчастье - мужественно.</dd>
<dt>Sapiens dominabitur astris.</dt>
<dd>Мудрый будет властвовать над звездами.</dd>
<dt>Sapiens est mutare consilium.</dt>
<dd>Умный не стыдится изменить свое мнение.</dd>
<dt>Sapiens ipse fingit fortunam sibi.</dt>
<dd>Мудрый сам создает собственное счастье.</dd>
<dt>Sapiens nil affirmat, quod non probet.</dt>
<dd>Умный ничего не утверждает без доказательств.</dd>
<dt>Sapiens semper beatus est.</dt>
<dd>Мудрец всегда счастлив.</dd>
<dt>Sapienti sat.</dt>
<dd>Для мудрого достаточно.</dd>
<dt>Sapientia felicitas.</dt>
<dd>Мудрость - это счастье.</dd>
<dt>Sapientia vino obumbratur.</dt>
<dd>Ум помрачается вином.</dd>
<dt>Satis eloquentiae, sapientiae parum.</dt>
<dd>Красноречия достаточно, мудрости мало.</dd>
<dt>Satis superque.</dt>
<dd>Более чем достаточно.</dd>
<dt>Satis verborum.</dt>
<dd>Довольно слов.</dd>
<dt>Satius est sero te quam numquam discere.</dt>
<dd>Лучше учиться поздно, чем никогда.</dd>
<dt>Satius est supervacua discere quam nihil.</dt>
<dd>Лучше изучить лишнее, чем ничего не изучить.</dd>
<dt>Satur venter non studet libenter.</dt>
<dd>Сытое брюхо к учению глухо.</dd>
<dt>Scientia difficilis sed fructuosa.</dt>
<dd>Наука трудна, но плодотворна.</dd>
<dt>Scientia nihil est quam veritatis imago.</dt>
<dd>Знание есть отражение истины.</dd>
<dt>Scientia potentia est.</dt>
<dd>Знание - это сила.</dd>
<dt>Scientia vinces.</dt>
<dd>Знание победит.</dd>
<dt>Scio me nihil scire.</dt>
<dd>Я знаю, что ничего не знаю.</dd>
<dt>Scopae recentiores semper meliores.</dt>
<dd>Новые метлы всегда лучше.</dd>
<dt>Scripta manent in saecula saeculorum.</dt>
<dd>Написанное останется во веки веков.</dd>
<dt>Secreto amicos admone, lauda palam.</dt>
<dd>Брани друзей наедине, а хвали публично.</dd>
<dt>Seditio civium hostium est occasio.</dt>
<dd>Несогласие граждан удобно для врагов.</dd>
<dt>Semper avarus eget.</dt>
<dd>Скупой всегда нуждается.</dd>
<dt>Semper fidelis.</dt>
<dd>Всегда верный.</dd>
<dt>Semper idem.</dt>
<dd>Всегда одно и то же.</dd>
<dt>Semper in motu.</dt>
<dd>Вечно в движении.</dd>
<dt>Semper paratus.</dt>
<dd>Всегда готов.</dd>
<dt>Seni desunt vires, juveni - scientiae.</dt>
<dd>Старику не хватает сил, юноше - знаний.</dd>
<dt>Sera nunquam est ad bonos mores via.</dt>
<dd>Никогда не поздно начать честную жизнь.</dd>
<dt>Sera parsimonia in fundo est.</dt>
<dd>Поздно беречь, коль видно дно.</dd>
<dt>Sereno quoque coelo aliquando tonat.</dt>
<dd>И в ясном небе бывает гром.</dd>
<dt>Sermo datur cunctis, animi sapientia paucis.</dt>
<dd>Язык дан всем, мудрость - немногим.</dd>
<dt>Sermo est imago animi.</dt>
<dd>Речь - образ души.</dd>
<dt>Sermone eo uti debemus, qui innatus est nobis.</dt>
<dd>Говорить следует на том языке, который является для нас врожденным.</dd>
<dt>Sero venientibus ossa.</dt>
<dd>Опоздавшему - кости.</dd>
<dt>Serva me, servabo te.</dt>
<dd>Выручи меня - я выручу тебя.</dd>
<dt>Servitus est postremum malorum omnium.</dt>
<dd>Неволя - наибольшее из всех несчастий.</dd>
<dt>Si etiam omnes, ego non.</dt>
<dd>Даже если все, я - нет.</dd>
<dt>Si finis bonus, laudabile totum.</dt>
<dd>При хорошем конце похвально и все дело.</dd>
<dt>Si melius quid habes, acresse vel imperium fer.</dt>
<dd>Имеешь - поделись, не имеешь - бери, что дают.</dd>
<dt>Si quesiveris invenies.</dt>
<dd>Ищи и обрящешь.</dd>
<dt>Si quid movendum est, move.</dt>
<dd>Если что-либо нужно подвинуть, двигай.</dd>
<dt>Si vis amari, ama.</dt>
<dd>Хочешь быть любимым - люби.</dd>
<dt>Si vis pacem, para bellum!</dt>
<dd>Хочешь мира - готовся к войне!</dd>
<dt>Si vis pacem, para pacem!</dt>
<dd>Хочешь мира - готовь мир!</dd>
<dt>Si vis vincere, disce pati.</dt>
<dd>Хочешь побеждать - учись терпению.</dd>
<dt>Sibi bene facit, qui bene facit amico.</dt>
<dd>Себе делает хорошо тот, кто делает хорошо другим.</dd>
<dt>Sic et simpliciter.</dt>
<dd>Так, и именно так.</dd>
<dt>Sic fata voluerunt.</dt>
<dd>Так угодно судьбе.</dd>
<dt>Sic itur ad astra.</dt>
<dd>Так идут к звездам.</dd>
<dt>Sic transit gloria mundi.</dt>
<dd>Так проходит мирская слава.</dd>
<dt>Sic transit tempus.</dt>
<dd>Так проходит время.</dd>
<dt>Signum temporis.</dt>
<dd>Знамение времени.</dd>
<dt>Sile et spera.</dt>
<dd>Молчи и надейся.</dd>
<dt>Silendo nemo peccat.</dt>
<dd>Кто молчит, тот не грешит.</dd>
<dt>Silentium videtur confessio.</dt>
<dd>Молчит - значит соглашается.</dd>
<dt>Simila similibus curantur.</dt>
<dd>Подобное лечится подобным.</dd>
<dt>Similia similibus destruuntur.</dt>
<dd>Подобное разрушается подобным.</dd>
<dt>Simplex sigillum veri.</dt>
<dd>Простота - признак искренности.</dd>
<dt>Simul consilium cum re amisisti.?</dt>
<dd>Неужели с деньгами ты потерял рассудок?.</dd>
<dt>Simulans amicum inimicus inimicissimus.</dt>
<dd>Самый опасный враг тот, кто притворяется другом.</dd>
<dt>Sine amicitia vita nulla est.</dt>
<dd>Без дружбы нет жизни.</dd>
<dt>Sine die.</dt>
<dd>Без даты.</dd>
<dt>Sine doctrina vita quasi mortis imago.</dt>
<dd>Без науки жизнь подобна смерти.</dd>
<dt>Sine ira et studio.</dt>
<dd>Без гнева и пристрастия.</dd>
<dt>Sine labore non erit panis in ore.</dt>
<dd>Без труда не будет хлеба во рту.</dd>
<dt>Sine metu mortis.</dt>
<dd>Без страха смерти.</dd>
<dt>Sine mora.</dt>
<dd>Без задержки.</dd>
<dt>Sine reservatione mentali.</dt>
<dd>Без задней мысли.</dd>
<dt>Sint ut sunt, aut non sint.</dt>
<dd>Пусть будут так, как есть, или пусть не будут вовсе.</dd>
<dt>Sit ut est.</dt>
<dd>Пусть останется как есть.</dd>
<dt>Sol lucet omnibus.</dt>
<dd>Солнце светит для всех.</dd>
<dt>Sola virtute armatus.</dt>
<dd>Одной лишь доблестю вооруженный.</dd>
<dt>Somni, cibi, potus, venus omnia moderata sint.</dt>
<dd>Сон, еда, питье, любовь - в меру.</dd>
<dt>Spe vivimus.</dt>
<dd>Живем надеждой.</dd>
<dt>Species decipit.</dt>
<dd>Внешность обманчива.</dd>
<dt>Species facti.</dt>
<dd>Обстоятельства дела.</dd>
<dt>Sperare contra spem.</dt>
<dd>Без надежды надеяться.</dd>
<dt>Spero meliora.</dt>
<dd>Надеюсь на лучшее.</dd>
<dt>Spero, sic moriar, ut mortuus non erubescam.</dt>
<dd>Надеюсь, я так умру, что мертвый не постыжусь.</dd>
<dt>Spes est ultimum adversarium rerum solatium.</dt>
<dd>Надежда - последнее утешение в несчастье.</dd>
<dt>Spes sibi quisque.</dt>
<dd>Каждый верит в себя.</dd>
<dt>Sponte sua, sine lege.</dt>
<dd>Добровольно, без принуждения.</dd>
<dt>Status naturalis.</dt>
<dd>Естественное состояние.</dd>
<dt>Status quo.</dt>
<dd>Существующее положение.</dd>
<dt>Status rerum.</dt>
<dd>Положение дел.</dd>
<dt>Sua cuique fortuna in manu est.</dt>
<dd>Собственное счастье у каждого в руках.</dd>
<dt>Sua sponte.</dt>
<dd>Сам собой, по своей воле.</dd>
<dt>Sua sunt cuique vitia.</dt>
<dd>Каждый имеет свои недостатки.</dd>
<dt>Suae quisque fortunae faber est.</dt>
<dd>Каждый кузнец своего счастья.</dd>
<dt>Suavis laborum est praetorium memoria.</dt>
<dd>Приятно вспомнить о былых трудах.</dd>
<dt>Suaviter in modo, fortiter in re.</dt>
<dd>Будь мягким в обхождении, но твердым в достижении цели.</dd>
<dt>Sub conditione.</dt>
<dd>При условии.</dd>
<dt>Sub fide nobili.</dt>
<dd>Под честное слово.</dd>
<dt>Sub rosa.</dt>
<dd>Секретно.</dd>
<dt>Sub rosa dicta tacenda.</dt>
<dd>Сказанное по секрету не подлежит разглашению.</dd>
<dt>Sub specie aeternitatis.</dt>
<dd>С точки зрения вечности.</dd>
<dt>Sub specie utilitatis.</dt>
<dd>С точки зрения пользы.</dd>
<dt>Sub sua propria specie.</dt>
<dd>Под особым углом зрения.</dd>
<dt>Subdore et sanguine.</dt>
<dd>Потом и кровью.</dd>
<dt>Sublata causa tollitur effectus.</dt>
<dd>С устранением причины устраняется и следствие.</dd>
<dt>Sublata causa, tollitur morbus.</dt>
<dd>С устранением причины устраняется болезнь.</dd>
<dt>Sufficit.</dt>
<dd>Достаточно.</dd>
<dt>Sum totus vester.</dt>
<dd>Я весь ваш.</dd>
<dt>Summa cum pietate.</dt>
<dd>С величайшим уважением.</dd>
<dt>Summa summarum.</dt>
<dd>В конце концов.</dd>
<dt>Summa virtus.</dt>
<dd>Высшая доблесть.</dd>
<dt>Summam nec metuas diem, nec optes.</dt>
<dd>Не страшись последнего дня, но и не призывай его.</dd>
<dt>Summum bonum.</dt>
<dd>Высшее благо.</dd>
<dt>Sunt certi denique fines.</dt>
<dd>Всему, однако, существуют границы.</dd>
<dt>Sunt delicta quibus ignovisse velimus.</dt>
<dd>Некоторые проступки желательно не замечать.</dd>
<dt>Suo periculo.</dt>
<dd>На свой страх и риск.</dd>
<dt>Super omnia veritas.</dt>
<dd>Выше всего истина.</dd>
<dt>Suppressio veri.</dt>
<dd>Сокрытие истины.</dd>
<dt>Surge et age.</dt>
<dd>Встань и действуй.</dd>
<dt>Sustine et abstine.</dt>
<dd>Выдержи и воздержись.</dd>
<dt>Suum cuique mos.</dt>
<dd>У каждого свой нрав.</dd>
<dt>Suum cuique placet.</dt>
<dd>Каждому нравится свое.</dd>
</div>

<a id="T"><h3>T</h3></a>

<div>
    <dt>Tace, sed memento</dt>
    <dd>Молчи, но помни!</dd>
    <dt>Taceamus</dt>
    <dd>Помолчим!</dd>
    <dt>Tacent, satis laudant.</dt>
    <dd>Молчат - уже похвально.</dd>
    <dt>Tacet sed loquitur.</dt>
    <dd>Молчанием говорит.</dd>
    <dt>Tacito consensu.</dt>
    <dd>С молчаливого согласия.</dd>
    <dt>Taedium vitae.</dt>
    <dd>Отвращение к жизни.</dd>
    <dt>Talis qualis.</dt>
    <dd>Такой, какой есть.</dd>
    <dt>Tamdiu discendum est, quamdiu vivis.</dt>
    <dd>Сколько живешь, столько и учись.</dd>
    <dt>Tandem aliquando.</dt>
    <dd>Наконец-то.</dd>
    <dt>Tanta vis probitatis est, ut eam etiam in hoste diligamus.</dt>
    <dd>Сила неподкупности такова, что мы ее ценим даже у врага.</dd>
    <dt>Tantum doluerunt, quantum doloribus se inseruerunt.</dt>
    <dd>Страдают настолько, насколько поддаются страданиям.</dd>
    <dt>Tantum possumus, quantum scimus.</dt>
    <dd>Столько можем, сколько знаем.</dd>
    <dt>Tantum scimus, quantum memoria tenemus.</dt>
    <dd>Столько знаем, сколько в памяти удерживаем.</dd>
    <dt>Te tua, me delectant mea.</dt>
    <dd>Тебе приятно твое, мне - мое.</dd>
    <dt>Temperantia est custos vitae.</dt>
    <dd>Умеренность - страж жизни.</dd>
    <dt>Tempora mutantur et nos mutamur in illis.</dt>
    <dd>Времена меняются, и мы меняемся с ними.</dd>
    <dt>Tempore et loco.</dt>
    <dd>В свое время и на своем месте.</dd>
    <dt>Tempore felici multi numerantur amici.</dt>
    <dd>В счастливые времена бывает много друзей.</dd>
    <dt>Tempori parce</dt>
    <dd>Береги время!</dd>
    <dt>Temporis filia veritas.</dt>
    <dd>Истина - дочь времени.</dd>
    <dt>Tempus edax rerum.</dt>
    <dd>Всепоглощающее время.</dd>
    <dt>Tempus fugit.</dt>
    <dd>Время летит.</dd>
    <dt>Teneas tuis te.</dt>
    <dd>Держись своих.</dd>
    <dt>Tentanda omnia.</dt>
    <dd>Надо все испробовать.</dd>
    <dt>Terra incognita.</dt>
    <dd>Неведомая страна.</dd>
    <dt>Terrae filius.</dt>
    <dd>Дитя природы.</dd>
    <dt>Tertium non datur.</dt>
    <dd>Третьего не дано.</dd>
    <dt>Testimonium maturitatis.</dt>
    <dd>Свидетельство зрелости.</dd>
    <dt>Tibi et igni.</dt>
    <dd>Тебе и огню (прочти и сожги).</dd>
    <dt>Tibi gratias.</dt>
    <dd>По твоей милости.</dd>
    <dt>Timeo hominem unius libri.</dt>
    <dd>Боюсь человека одной книги.</dd>
    <dt>Totis viribus.</dt>
    <dd>Всеми силами.</dd>
    <dt>Tractu temporis.</dt>
    <dd>С течением времени.</dd>
    <dt>Tranquillas etiam naufragus horret aquas.</dt>
    <dd>Потерпевший кораблекрушение боится спокойной воды.</dd>
    <dt>Transeat a me calix iste</dt>
    <dd>Да минует меня чаша сия!</dd>
    <dt>Tribus verbis.</dt>
    <dd>В трех словах.</dd>
    <dt>Tristis est anima mea.</dt>
    <dd>Печальна душа моя.</dd>
    <dt>Trita via recedere periculosum.</dt>
    <dd>Опасно отступать от проторенной дороги.</dd>
    <dt>Truditur dies die.</dt>
    <dd>День спешит за днем.</dd>
    <dt>Tu ne cede malis, sed contra audentior ito</dt>
    <dd>Не покоряйся беде, а смело иди ей навстречу!</dd>
    <dt>Tu quoque, Brute!</dt>
    <dd>И ты, Брут!</dd>
    <dt>Tuo commodo.</dt>
    <dd>По твоему усмотрению.</dd>
    <dt>Turpe est aliud loqui, aliud sentire.</dt>
    <dd>Стыдно говорить одно, а думать другое.</dd>
    <dt>Turpia corrumpunt teneras spectacula mentes.</dt>
    <dd>Непристойные зрелища портят молодые души.</dd>
    <dt>Turpis fuga mortis est omni morte pejor.</dt>
    <dd>Позорное бегство от смерти хуже любой смерти.</dd>
</div>

<a id="U"><h3>U</h3></a>

<div>
    <dt>Uberrima fides.</dt>
    <dd>Самым честным образом.</dd>
    <dt>Ubi amici, ibi opes.</dt>
    <dd>Где друзья, там и богатство.</dd>
    <dt>Ubi concordia, ibi victoria.</dt>
    <dd>Где согласие, там и победа.</dd>
    <dt>Ubi emolumentum, ibi onus.</dt>
    <dd>Где выгода, там и бремя.</dd>
    <dt>Ubi jus, ibi remedium.</dt>
    <dd>Где закон, там и защита.</dd>
    <dt>Ubi rerum testimonia adsunt, quid opus est verbis?</dt>
    <dd>Где дело говорит само за себя, к чему слова?.</dd>
    <dt>Ubi uber, ibi tuber.</dt>
    <dd>Где богатство, там и хлопоты.</dd>
    <dt>Ulcera animi sananda magis, quam corporis.</dt>
    <dd>Душевные раны лечатся труднее телесных.</dd>
    <dt>Ultima forsan.</dt>
    <dd>Последняя возможность.</dd>
    <dt>Ultima ratio.</dt>
    <dd>Последний довод.</dd>
    <dt>Ultimum refugium.</dt>
    <dd>Последнее убежище.</dd>
    <dt>Ultimum terminus.</dt>
    <dd>Крайний срок.</dd>
    <dt>Ultra posse nemo obligatur.</dt>
    <dd>Никто не обязан сверх невозможного.</dd>
    <dt>Ultra vires.</dt>
    <dd>За пределами сил.</dd>
    <dt>Umbram suam timet.</dt>
    <dd>Боится собственной тени.</dd>
    <dt>Una hirundo non facit ver.</dt>
    <dd>Одна ласточка не делает весны.</dd>
    <dt>Una salus victis nullam sperare salutem.</dt>
    <dd>Одно спасение для побежденных - не надеяться на спасение.</dd>
    <dt>Una virtus, nulla virtus.</dt>
    <dd>Одно достоинство - значит ни одного достоинства.</dd>
    <dt>Una voce.</dt>
    <dd>Единогласно.</dd>
    <dt>Unde venis et quo vadis.?</dt>
    <dd>Откуда идешь и куда направляешься?.</dd>
    <dt>Uno animo.</dt>
    <dd>Единодушно.</dd>
    <dt>Unum habemus os et duas aures, ut minus dicamus et plus audiamus.</dt>
    <dd>Мы имеем один рот и два уха, чтобы меньше говорить и больше слушать.</dd>
    <dt>Unus pro omnibus et omnes pro uno.</dt>
    <dd>Один за всех и все за одного.</dd>
    <dt>Unusquisque sua noverit ire via.</dt>
    <dd>Пусть каждый идет своим путем.</dd>
    <dt>Urbes constituit aetas, hora dissolvit.</dt>
    <dd>Города создаются столетиями, а разрушаются в один час.</dd>
    <dt>Usus est magister optimus.</dt>
    <dd>Практика - лучший учитель.</dd>
    <dt>Usus fructus.</dt>
    <dd>Право пользования.</dd>
    <dt>Usus practicus.</dt>
    <dd>Практическая необходимость.</dd>
    <dt>Usus tyrannus.</dt>
    <dd>Обычай - деспот.</dd>
    <dt>Ut ameris, amabilis esto.</dt>
    <dd>Чтобы тебя любили, будь достойным любви.</dd>
    <dt>Ut desint vires, tamen est laudanda voluntas.</dt>
    <dd>Пусть не хватит сил, но похвально само желание.</dd>
    <dt>Ut salutas, ita salutaberis.</dt>
    <dd>Как поздороваешься, так тебе и ответят.</dd>
    <dt>Ut sementem feceris, ita metes.</dt>
    <dd>Как посеешь, так и пожнешь.</dd>
    <dt>Ut supra (u.s..)</dt>
    <dd>Как сказано выше.</dd>
    <dt>Utere et abutere.</dt>
    <dd>Пользуйся и используй.</dd>
    <dt>Uti, non abuti.</dt>
    <dd>Употреблять, но не злоупотреблять.</dd>
    <dt>Utile dulci.</dt>
    <dd>Приятное с полезным.</dd>
    <dt>Utile dulci miscere.</dt>
    <dd>Совмещай полезное с приятным.</dd>
    <dt>Utile non debet per inutile vitiari.</dt>
    <dd>Правильное не следует искажать посредством неправильного.</dd>
    <dt>Utrumque paratus.</dt>
    <dd>Готов для любого дела.</dd>
    <dt>Utrumque vitium est - et omnibus credere, et nulli.</dt>
    <dd>И то и другое плохо: и верить всем, и никому не верить.</dd>
</div>

<a id="V"><h3>V</h3></a>

<div>
    <dt>Vacua vasa plurimum sonant.</dt>
    <dd>Пустая посуда громко звенит.</dd>
    <dt>Vacuum horrendum.</dt>
    <dd>Наводящая ужас пустота.</dd>
    <dt>Vae soli.</dt>
    <dd>Горе одинокому.</dd>
    <dt>Vae victis.</dt>
    <dd>Горе побежденным.</dd>
    <dt>Vae victoribus.</dt>
    <dd>Горе победителям.</dd>
    <dt>Vale et me ama.</dt>
    <dd>Прощай и люби меня.</dd>
    <dt>Vale et memor sis mei.</dt>
    <dd>Прощай и помни обо мне.</dd>
    <dt>Vale!</dt>
    <dd>Прощай!</dd>
    <dt>Valeant curae!</dt>
    <dd>Прощайте, заботы!</dd>
    <dt>Valetudo bonum optimum.</dt>
    <dd>Здоровье - наибольшее из благ.</dd>
    <dt>Vana est sapientia nostra.</dt>
    <dd>Бесполезна наша мудрость.</dd>
    <dt>Vanitas vanitatum.</dt>
    <dd>Суета сует.</dd>
    <dt>Variatio delectat.</dt>
    <dd>Разнообразие радует.</dd>
    <dt>Varium et mutabile semper femina.</dt>
    <dd>Женщина всегда изменчива и непостоянна.</dd>
    <dt>Vaticinia post eventum.</dt>
    <dd>Предсказание задним числом.</dd>
    <dt>Vel sapientissimus errare potest.</dt>
    <dd>И самый мудрый может ошибаться.</dd>
    <dt>Velle non discitur.</dt>
    <dd>Нельзя научиться хотеть.</dd>
    <dt>Velox consilium sequitur poenitentia.</dt>
    <dd>За поспешным решением следует раскаяние.</dd>
    <dt>Veni, vidi, vici.</dt>
    <dd>Пришел, увидел, победил.</dd>
    <dt>Venienti occurrite morbo.</dt>
    <dd>Торопитесь лечить болезнь вовремя.</dd>
    <dt>Ventis loqueris.</dt>
    <dd>Говоришь напрасно.</dd>
    <dt>Ver hiemem sequitur, sequitur post triste serenum.</dt>
    <dd>После зимы - весна, после печали - радость.</dd>
    <dt>Vera ornamenta matronarum pudicitia, non vestes.</dt>
    <dd>Лучшее украшение женщины не одежда, а скромность.</dd>
    <dt>Verba veritatis.</dt>
    <dd>Прямое, откровенное мнение.</dd>
    <dt>Verba volant, scripta manent.</dt>
    <dd>Слова летучи, письмена живучи.</dd>
    <dt>Verbatim.</dt>
    <dd>Слово в слово.</dd>
    <dt>Verbatim et litteratim.</dt>
    <dd>Дословно и буквально.</dd>
    <dt>Verbi gratia.</dt>
    <dd>Например.</dd>
    <dt>Verbo in verbum.</dt>
    <dd>Слово за словом.</dd>
    <dt>Verbum nobile.</dt>
    <dd>Честное слово.</dd>
    <dt>Vere scire est per causas scire.</dt>
    <dd>Подлинное знание - в познании причины.</dd>
    <dt>Veritas in medio est.</dt>
    <dd>Истина находится посредине.</dd>
    <dt>Veritas nimis saepe laborat, exstinguitur numquam.</dt>
    <dd>Истина часто бывает безпомощной, не никогда не гибнет.</dd>
    <dt>Veritas odium parit, obsequium amicos.</dt>
    <dd>Откровенность создает врагов, а лесть - друзей.</dd>
    <dt>Veritas simplex oratio est.</dt>
    <dd>Правдивая речь проста.</dd>
    <dt>Veritas una, error multiplex.</dt>
    <dd>Истина едина - заблуждение многообразно.</dd>
    <dt>Veritas veritatum.</dt>
    <dd>Истина из истин.</dd>
    <dt>Veritas vincit.</dt>
    <dd>Истина побеждает.</dd>
    <dt>Veritatem dies aperit.</dt>
    <dd>Время обнаруживает правду.</dd>
    <dt>Veritatis simplex oratio.</dt>
    <dd>Речь истины проста.</dd>
    <dt>Verum amicum pecunia non parabis.</dt>
    <dd>Настоящего друга не приобретешь за деньги.</dd>
    <dt>Verum plus uno esse non potest.</dt>
    <dd>Больше одной истины быть не может.</dd>
    <dt>Vestigia semper adora.</dt>
    <dd>Всегда благоговей перед следами прошлого.</dd>
    <dt>Vetus amor non sentit rubiginem.</dt>
    <dd>Старая любовь не ржавеет.</dd>
    <dt>Via lactea.</dt>
    <dd>Млечный путь.</dd>
    <dt>Via sacra.</dt>
    <dd>Святой путь.</dd>
    <dt>Via trita via tuta.</dt>
    <dd>Проторенный путь безопасен.</dd>
    <dt>Vice versa.</dt>
    <dd>Наоборот.</dd>
    <dt>Vicinus bonus ingens bonum.</dt>
    <dd>Хороший сосед - большое благо.</dd>
    <dt>Videas, quid agas.</dt>
    <dd>Думай о том, что делаешь.</dd>
    <dt>Videte et applaudite</dt>
    <dd>Смотрите и рукоплещите!</dd>
    <dt>Vile est quod licet.</dt>
    <dd>Мало ценится то, что легко доступно.</dd>
    <dt>Vincere aut mori.</dt>
    <dd>Победить или умереть.</dd>
    <dt>Vincula da linguae vel tibi vincla dabit.</dt>
    <dd>Свяжи язык, иначе он тебя свяжет.</dd>
    <dt>Vinculum matrimonii.</dt>
    <dd>Узы брака.</dd>
    <dt>Vinum locutum est.</dt>
    <dd>Говорило вино.</dd>
    <dt>Viribus unitis.</dt>
    <dd>Соединенными силами.</dd>
    <dt>Viribus unitis res parvae crescunt.</dt>
    <dd>С объединением усилий растут и малые дела.</dd>
    <dt>Virtus actuosa.</dt>
    <dd>Добродетель деятельна.</dd>
    <dt>Virtus nihil expetit praemii.</dt>
    <dd>Доблесть не ищет наград.</dd>
    <dt>Virtus nobilitat.</dt>
    <dd>Честность облагораживает.</dd>
    <dt>Virtus post nummos.</dt>
    <dd>Добродетель после денег.</dd>
    <dt>Virtus sola homines beatos reddit.</dt>
    <dd>Только честность делает людей счастливыми.</dd>
    <dt>Vis impotentiae.</dt>
    <dd>Сила бессилия.</dd>
    <dt>Vis inertiae.</dt>
    <dd>Сила инерции.</dd>
    <dt>Vis medicatrix naturae.</dt>
    <dd>Целительная сила природы.</dd>
    <dt>Vis vi repellitur.</dt>
    <dd>Насилие отражается силой.</dd>
    <dt>Vis vitalis.</dt>
    <dd>Жизненная сила.</dd>
    <dt>Vita brevis, ars longa.</dt>
    <dd>Жизнь коротка, искусство вечно.</dd>
    <dt>Vita contemplativa.</dt>
    <dd>Созерцательная жизнь.</dd>
    <dt>Vita sine libertate nihil.</dt>
    <dd>Без свободы нет жизни.</dd>
    <dt>Vita sine litteris mors est.</dt>
    <dd>Жизнь без книги мертва.</dd>
    <dt>Vita somnium breve.</dt>
    <dd>Жизнь - это краткий сон.</dd>
    <dt>Vitae, non scholae discimus.</dt>
    <dd>Для жизни, не для школы учимся.</dd>
    <dt>Vitam extendere factis.</dt>
    <dd>Продлить жизнь делами.</dd>
    <dt>Vitam impendere vero.</dt>
    <dd>Посвяти жизнь истине.</dd>
    <dt>Vitiis sine nemo nascitur.</dt>
    <dd>Никто не рождается без пороков.</dd>
    <dt>Vive ut vivas.</dt>
    <dd>Живи, чтобы жить.</dd>
    <dt>Vive valeque.</dt>
    <dd>Живи и здравствуй.</dd>
    <dt>Vivere est cogitare.</dt>
    <dd>Жить - значит мыслить.</dd>
    <dt>Vivere in diem.</dt>
    <dd>Жить одним днем.</dd>
    <dt>Vivit post funera virtus.</dt>
    <dd>Добродетель переживет смерть.</dd>
    <dt>Volens nolens.</dt>
    <dd>Волей-неволей.</dd>
    <dt>Volo, non valeo.</dt>
    <dd>Хочу, но не могу.</dd>
    <dt>Voluntas est superior intellectu.</dt>
    <dd>Воля стоит над мышлением.</dd>
    <dt>Voluntas, e difficili data dulcissima est.</dt>
    <dd>Удовлетворение, добытое с трудом, - самое приятное.</dd>
    <dt>Voluntatem potius quam verba considerari oportet.</dt>
    <dd>Следует больше обращать внимания на намерения, чем на слова.</dd>
    <dt>Votum separatum.</dt>
    <dd>Особое мнение.</dd>
    <dt>Vox clamantis in deserto.</dt>
    <dd>Глас вопиющего в пустыне.</dd>
    <dt>Vox unius, vox nullius.</dt>
    <dd>Один голос - ни одного голоса.</dd>
    <dt>Vox, vox, praeterea nihil.</dt>
    <dd>Слова, слова и ничего больше.</dd>
    <dt>Vulpes pilum mutat, non mores.</dt>
    <dd>Лиса меняет шерсть, но не нрав.</dd>
    <dt>Vultus est index animi.</dt>
    <dd>Лицо - зеркало души.</dd>
</div>

<a id="US"><h3>Не отсортированные</h3></a>

<div>

    <dt>Scientia potentia est.</dt>
    <dd>Знание - сила.</dd>
    <dt>Vita brevis, ars longa.</dt>
    <dd>Жизнь коротка, искусство - вечно.</dd>
    <dt>Volens - nolens.</dt>
    <dd>Волей - неволей.</dd>
    <dt>Historia est magistra vita.</dt>
    <dd>История - учительница жизни.</dd>
    <dt>Dum spiro, spero.</dt>
    <dd>Пока дышу - надеюсь.</dd>
    <dt>Per aspera ad astra!</dt>
    <dd>Через тернии - к звездам</dd>
    <dt>Terra incognita.</dt>
    <dd>Неизвестная земля.</dd>
    <dt>Homo sapiens.</dt>
    <dd>Человек разумный.</dd>
    <dt>Sina era est studio.</dt>
    <dd>Без гнева и пристрастия</dd>
    <dt>Cogito ergo sum.</dt>
    <dd>Мыслю, следовательно существую.</dd>
    <dt>Non scholae sed vitae discimus.</dt>
    <dd>Мы учимся не для школы, а для жизни.</dd>
    <dt>Bis dat qui cito dat.</dt>
    <dd>Дважды дает тот, кто дает быстро.</dd>
    <dt>Clavus clavo pellitur.</dt>
    <dd>Клин клином вышибают.</dd>
    <dt>Alter ego.</dt>
    <dd>Второе "я".</dd>
    <dt>Errare humanum est.</dt>
    <dd>Человеку свойственно ошибаться.</dd>
    <dt>Repetitio est mater studiorum.</dt>
    <dd>Повторенье - мать ученья.</dd>
    <dt>Nomina sunt odiosa.</dt>
    <dd>Имена ненавистны.</dd>
    <dt>Otium post negotium.</dt>
    <dd>Отдых после дела.</dd>
    <dt>Mens sana in corpore sano.</dt>
    <dd>В здоровом теле здоровый дух.</dd>
    <dt>Urbi et orbi.</dt>
    <dd>Городу и миру.</dd>
    <dt>Amicus Plato, sed magis amica veritas.</dt>
    <dd>Платон мне друг, но истина дороже.</dd>
    <dt>Finis coronat opus.</dt>
    <dd>Конец - делу венец.</dd>
    <dt>Homo locum ornat, non locus hominem.</dt>
    <dd>Не место красит человека, а человек - место.</dd>
    <dt>Ad majorem Dei gloriam.</dt>
    <dd>К вящей славе Божией.</dd>
    <dt>Una hirundo ver non facit.</dt>
    <dd>Одна ласточка весны не делает.</dd>
    <dt>Citius, altius, fortius.</dt>
    <dd>Быстрее, выше, сильнее.</dd>
    <dt>Sic transit gloria mundi.</dt>
    <dd>Так проходит земная слава.</dd>
    <dt>Aurora Musis amica.</dt>
    <dd>Аврора - подруга музам.</dd>
    <dt>Tempora mutantur et nos mutamur in illis.</dt>
    <dd>Времена меняются, и мы меняемся вместе с ними.</dd>
    <dt>Non multa, sed multum.</dt>
    <dd>Не много, но о многом.</dd>
    <dt>E fructu arbor cognoscitur.</dt>
    <dd>Дерево узнается по плоду.</dd>
    <dt>Veni, vidi, vici.</dt>
    <dd>Пришел, увидел, победил.</dd>
    <dt>Post scriptum.</dt>
    <dd>После написанного.</dd>
    <dt>Alea est jacta.</dt>
    <dd>Жребий брошен.</dd>
    <dt>Dixi et animam salvavi.</dt>
    <dd>Я сказал это и этим спас свою душу.</dd>
    <dt>Nulla dies sine linea.</dt>
    <dd>Ни дня без строчки.</dd>
    <dt>Quod licet Jovi, non licet bovi.</dt>
    <dd>Что позволено Юпитеру, не позволено Быку.</dd>
    <dt>Felix, qui potuti rerum cogoscere causas.</dt>
    <dd>Счастлив тот, кто познал причину вещей.</dd>
    <dt>Si vis pacem, para bellum.</dt>
    <dd>Хочешь мира, готовься к войне.</dd>
    <dt>Cui bono?</dt>
    <dd>Кому на пользу?</dd>
    <dt>Scio me nihil scire.</dt>
    <dd>Я знаю, что я ничего не знаю.</dd>
    <dt>Nosce te ipsum!</dt>
    <dd>Познай самого себя!</dd>
    <dt>Est modus in rebus.</dt>
    <dd>Есть мера в вещах.</dd>
    <dt>Jurare in verba magistri.</dt>
    <dd>Клясться словами учителя.</dd>
    <dt>Qui tacet, consentire videtur.</dt>
    <dd>Молчание - знак согласия.</dd>
    <dt>In hoc signo vinces!</dt>
    <dd>Под этим знаменем победишь.(Сим победиши!)</dd>
    <dt>Labor recedet, bene factum non abscedet.</dt>
    <dd>Трудности уйдут, а благое дело останется.</dd>
    <dt>Non est fumus absque igne.</dt>
    <dd>Нет дыма без огня.</dd>
    <dt>Duobus certantibus tertius gaudet.</dt>
    <dd>Когда двое дерутся - третий радуется.</dd>
    <dt>Divide et impera!</dt>
    <dd>Разделяй и властвуй!</dd>
    <dt>Corda nostra laudus est.</dt>
    <dd>Наши сердца больны от любви.</dd>
    <dt>O tempora</dt>
    <dd>O mores!
    </dt>
    <dd>О времена, о нравы!</dd>
    <dt>Homo est animal sociale.</dt>
    <dd>Человек есть общественное животное.</dd>
    <dt>Homo homini lupus est.</dt>
    <dd>Человек человеку - волк.</dd>
    <dt>Dura lex, sed lex.</dt>
    <dd>Закон суров, но справедлив.</dd>
    <dt>O sancta simplicitas!</dt>
    <dd>Святая простота!</dd>
    <dt>Hominem quaero</dt>
    <dd>(Dioqines)
    </dt>
    <dd>Ищу человека
    </dt>
    <dd>(Диоген)</dd>
    <dt>At Kalendas Graecas.</dt>
    <dd>К греческим календам(После дождичка в четверг)</dd>
    <dt>Quo usque Catlina, abuter patientia nostra?</dt>
    <dd>До каких пор, Катилина, ты будешь злоупотреблять нашим
        терпением?
    </dd>
    <dt>Vox populi - vox Dei.</dt>
    <dd>Голос народа - голос Бога.</dd>
    <dt>In vene veritas.</dt>
    <dd>Истина в вине.</dd>
    <dt>Qualis rex, talis grex.</dt>
    <dd>Каков поп, таков и приход.</dd>
    <dt>Qualis dominus, tales servi.</dt>
    <dd>Каков хозяин, таков и слуга.</dd>
    <dt>Si vox est - canta!</dt>
    <dd>Если у тебя есть голос - пой!</dd>
    <dt>I, pede fausto!</dt>
    <dd>Иди счастливой поступью!</dd>
    <dt>Tempus consilium dabet.</dt>
    <dd>Время покажет.</dd>
    <dt>Barba crescit, caput nescit.</dt>
    <dd>Волос долог, ум короток.</dd>
    <dt>Labores gigunt hanores.</dt>
    <dd>Труды порождают почести.</dd>
    <dt>Amicus cognoscitur in amore, more, ore, re.</dt>
    <dd>Друг познается в любви, нраве, речах, делах.</dd>
    <dt>Ecce homo!</dt>
    <dd>Вот человек!</dd>
    <dt>Homo novus.</dt>
    <dd>Новый человек, "выскочка".</dd>
    <dt>In pace litterae florunt.</dt>
    <dd>Во имя мира науки процветают.</dd>
    <dt>Fortes fortuna juiat.</dt>
    <dd>Фортуна помогает смелым.</dd>
    <dt>Carpe diem!</dt>
    <dd>Лови момент!</dd>
    <dt>Nostra victoria in concordia.</dt>
    <dd>Наша победа в согласии.</dd>
    <dt>Veritatis simplex est orato.</dt>
    <dd>Истинная речь проста.</dd>
    <dt>Nemo omnia potest scire.</dt>
    <dd>Никто не может знать все.</dd>
    <dt>Finis coronat opus.</dt>
    <dd>Конец - делу венец.</dd>
    <dt>Omnia mea mecum porto.</dt>
    <dd>Все свое ношу с собой.</dd>
    <dt>Sancta sanctorum.</dt>
    <dd>Святая святых.</dd>
    <dt>Ibi victoria ubi concordia.</dt>
    <dd>Там победа, где согласие.</dd>
    <dt>Experentia est optima magistra.</dt>
    <dd>Опыт есть лучший учитель.</dd>
    <dt>Amat victoria curam.</dt>
    <dd>Победа любит заботу.</dd>
    <dt>Vivere est cogitare.</dt>
    <dd>Жить значит мыслить.</dd>
    <dt>Epistula non erubescit.</dt>
    <dd>Бумага не краснеет.</dd>
    <dt>Festina lente!</dt>
    <dd>Поспешай медленней!</dd>
    <dt>Nota bene.</dt>
    <dd>Запомни хорошо.</dd>
    <dt>Elephantum ex musca facis.</dt>
    <dd>Делать из мухи слона.</dd>
    <dt>Ignorantia non est argumentum.</dt>
    <dd>Отрицание не есть доказательство.</dd>
    <dt>Lupus non mordet lupum.</dt>
    <dd>Волк волка не кусает.</dd>
    <dt>Vae victis!</dt>
    <dd>Горе побежденным!</dd>
    <dt>Medice, cura te ipsum!</dt>
    <dd>Врач, исцелися сам
    </dt>
    <dd>(От Луки 4:17)</dd>
    <dt>De te fabula narratur.</dt>
    <dd>О тебе сказочка рассказывается.</dd>
    <dt>Tertium non datur.</dt>
    <dd>Третьего не дано.</dd>
    <dt>Age, quod agis.</dt>
    <dd>Делай, что ты делаешь.</dd>
    <dt>Do ut des.</dt>
    <dd>Даю, чтобы и ты дал.</dd>
    <dt>Amantes - amentes.</dt>
    <dd>Влюбленные безумны.</dd>
    <dt>Alma mater.</dt>
    <dd>Университет.</dd>
    <dt>Amor vincit omnia.</dt>
    <dd>Любовь побеждает все.</dd>
    <dt>Aut Caesar, aut nihil.</dt>
    <dd>Или все, или ничего.</dd>
    <dt>Aut - aut.</dt>
    <dd>Или-или.</dd>
    <dt>Si vis amari, ama.</dt>
    <dd>Если хочешь быть любимым, люби.</dd>
    <dt>Ab ovo ad mala.</dt>
    <dd>С яйца и до яблока.</dd>
    <dt>Timeo danaos et dona ferentes.</dt>
    <dd>Бойтесь данайцев, дары приносящих.</dd>
    <dt>Sapienti sat est.</dt>
    <dd>Это сказано человеком.</dd>
    <dt>Periculum in mora.</dt>
    <dd>Опасность в промедлении.</dd>
    <dt>O fallacem hominum spem!</dt>
    <dd>О обманчивая надежда человеческая!</dd>
    <dt>Quoandoe bonus dormitat Homerus.</dt>
    <dd>Иногда и добрый наш Гомер дремлет.</dd>
    <dt>Sponte sua sina lege.</dt>
    <dd>По собственному побуждению.</dd>
    <dt>Pia desideria.</dt>
    <dd>Благие намерения.</dd>
    <dt>Ave Caesar, morituri te salutant.</dt>
    <dd>Идущие на смерть, Цезарь, приветствуют тебя!</dd>
    <dt>Modus vivendi.</dt>
    <dd>Образ жизни</dd>
    <dt>Homo sum: humani nihil a me alienum puto.</dt>
    <dd>Я человек, и ничто человеческое мне не чуждо.</dd>
    <dt>Ne quid nimis.</dt>
    <dd>Ничего сверх меры.</dd>
    <dt>De qustibus et coloribus non est disputantum.</dt>
    <dd>На вкус и цвет товарища нет.</dd>
    <dt>Ira furor brevis est.</dt>
    <dd>Гнев есть кратковременное исступленье.</dd>
    <dt>Feci quod potui faciant meliora potentes</dt>
    <dd>Я сделал все, что мог.
    </dt>
    <dd>Кто может, пусть сделает лучше.</dd>
    <dt>Nescio quid majus nascitur Iliade.</dt>
    <dd>Рождается нечто более великое, чем Илиада.</dd>
    <dt>In medias res.</dt>
    <dd>В середину вещей, в самую суть.</dd>
    <dt>Non bis in idem.</dt>
    <dd>Достаточно и одного раза.</dd>
    <dt>Non sum qualis eram.</dt>
    <dd>Я не тот, каким был прежде.</dd>
    <dt>Abussus abussum invocat.</dt>
    <dd>Беда никогда не приходит одна.</dd>
    <dt>Hoc volo sic jubeo sit pro ratione voluntas.</dt>
    <dd>Я так велю, пусть доводом будет моя воля.</dd>
    <dt>Amici diem perdidi!</dt>
    <dd>Друзья, я потерял день.</dd>
    <dt>Aquilam volare doces.</dt>
    <dd>Учить орла летать.</dd>
    <dt>Vive, valeque.</dt>
    <dd>Живи и здравствуй.</dd>
    <dt>Vale et me ama.</dt>
    <dd>Будь и здоров и люби меня.</dd>
    <dt>Sic itur ad astra.</dt>
    <dd>Так идут к звездам.</dd>
    <dt>Si taces, consentus.</dt>
    <dd>Кто молчит, соглашается.</dd>
    <dt>Littera scripta manet.</dt>
    <dd>Написанное остается.</dd>
    <dt>Ad meliora tempora.</dt>
    <dd>До лучших времен.</dd>
    <dt>Plenus venter non studet libenter.</dt>
    <dd>Сытое брюхо к учению глухо.</dd>
    <dt>Abussus non tollit usum.</dt>
    <dd>Злоупотребление не отменяет употребления.</dd>
    <dt>Ab urbe conita.</dt>
    <dd>От основания города.</dd>
    <dt>Salus populi summa lex.</dt>
    <dd>Благо народа есть высший закон.</dd>
    <dt>Vim vi repelllere licet.</dt>
    <dd>Насилие позволяется отражать силой.</dd>
    <dt>Sero (tarle) venientibus - ossa.</dt>
    <dd>Поздно приходящим достаются кости.</dd>
    <dt>Lupus in fabula.</dt>
    <dd>Легок на помине.</dd>
    <dt>Acta est fabula.</dt>
    <dd>Представление окончено.
    </dt>
    <dd>(Финита ля комедия!)</dd>
    <dt>Legem brevem esse oportet.</dt>
    <dd>Закон должен быть кратким.</dd>
    <dt>Lectori benevolo salutem.</dt>
    <dd>(L.B.S.) Привет благосклонному читателю.</dd>
    <dt>Aegri somnia.</dt>
    <dd>Сновидения больного.</dd>
    <dt>Abo in pace.</dt>
    <dd>Иди с миром.</dd>
    <dt>Absit invidia verbo.</dt>
    <dd>Да не осудят меня за эти слова.</dd>
    <dt>Abstractum pro concreto.</dt>
    <dd>Абстрактное вместо конкретного.</dd>
    <dt>Acceptissima semper munera sunt, auctor quae pretiosa facit.</dt>
    <dd>Лучше всего те подарки, ценность которых в
        самом дарителе.
    </dd>
    <dt>Ad impossibilia nemo obligatur.</dt>
    <dd>К невозможному никого не обязывают.</dd>
    <dt>Ad libitum.</dt>
    <dd>По желанию.</dd>
    <dt>Ad narrandum, non ad probandum.</dt>
    <dd>Для того, чтобы рассказать, а не доказать.</dd>
    <dt>Ad notam.</dt>
    <dd>К сведению.</dd>
    <dt>Ad personam.</dt>
    <dd>Лично.</dd>
    <dt>Advocatus Dei (Diavoli)</dt>
    <dd>Адвокат Бога.
    </dt>
    <dd>(Дьявола).</dd>
    <dt>Aeterna urbs.</dt>
    <dd>Вечный город.</dd>
    <dt>Aquila non captat muscas.</dt>
    <dd>Орел не ловит мух.</dd>
    <dt>Confiteor solum hoc tibi.</dt>
    <dd>Исповедуюсь в этом только тебе.</dd>
    <dt>Cras amet, qui nunquam amavit quique amavit cras amet.</dt>
    <dd>Пусть завтра полюбит тот, кто никогда не любил, а
        тот кто любил, пусть завтра полюбит.
    </dd>
    <dt>Credo, quia verum (absurdum).</dt>
    <dd>Верю, ибо это истина (это абсурдно).</dd>
    <dt>Bene placito.</dt>
    <dd>По доброй воле.</dd>
    <dt>Cantus cycneus.</dt>
    <dd>Лебединая песня.</dd>

</div>


</dl>
</div>
</div>

</div>