<?php
use app\assets\LogicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Учебник логики. Глава 10.';
$bundle = LogicAsset::register($this);
?>
<div class="book_site">
<h2>Глава X<br>О противоположении суждений</h2>
<P>Постановка вопроса. Мы видели, что существуют различные классы суждений в зависимости от того, какое им принадлежит
    количество и качество. Суждения, в которых одно и то же подлежащее и сказуемое, но которые имеют разные качества или
    количества или и то и другое, будут противоположными друг другу. Например, суждения А и I, суждения Е и А
    противоположны друг другу.</P>
<p align="center"><i>Рис.18</i></P>
<p align="center"><?= Html::img($bundle->baseUrl.'/icons/logic/u/20.gif',['width'=>338, 'height'=>232,]);?></P>
<P>Вопрос о противоположности суждений имеет важное значение. Если я, возражая кому-нибудь, не признаю истинности его
    утверждения, то я всё-таки нечто могу признавать истинным. Например, кто-нибудь утверждает, что все люди мудры, и я
    это отрицаю, то я в то же время сознаю, что я могу признать истинность суждения “некоторые люди мудры”. Эти два
    суждения совместимы друг с другом. Если я утверждаю, что люди смертны, то я не могу в то же время признавать, что
    некоторые люди не смертны. Одно суждение оказывается несовместимым с другим суждением. Отсюда возникает
    необходимость рассмотреть все суждения с точки зрения их противоположности, чтобы показать, какие суждения
    совместимы или не совместимы друг с другом.</P>
<P>Для выяснения этого вопроса мы воспользуемся схемой, известной под именем “логического квадрата” (рис. 18). Схема эта
    наглядно показывает взаимное отношение суждений всех четырёх классов.</P>
<P>Возьмём квадрат и проведём в нём диагонали. У вершин четырёх его углов поставим буквы А, Е, I, О, т. е. символы
    четырёх классов суждений. Возьмём какое-нибудь суждение и представим его в формах суждений всех четырёх классов: А —
    “все люди честны”, Е — “ни один человек не честен”, I — “некоторые люди честны”, О — “некоторые люди не суть
    честны”.</P>
<P>Между суждениями А и О, Е и 1 существует отношение, которое называется противоречием” Эти суждения отличаются и во
    количеству и по качеству.</P>
<P>Отношение между А и Е называется противностью. Эти общие суждения отличаются друг от друга по качеству.</P>
<P>Между А и I, Е и О есть отношения подчинения. Здесь суждения отличаются по количеству.</P>
<P>Между I и О — отношение подпротивности. Здесь два частных суждения отличаются по качеству.</P>
<P>Рассмотрим каждую пару этих суждений в отдельности.</P>
<P>Противоречие (А — О, Е — I). Я высказываю суждение А — “все люди искренни”. Вы находите, что это суждение ложно. В
    таком случае вы должны признать истинным суждение О— “некоторые люди не искренни”. Если вы не допустите истинности
    этого последнего суждения, то вы не можете признать ложности суждения А. Следовательно, при ложности суждения А,
    .суждение О должно быть истинным.</P>
<P>Возьмём суждение О — “некоторые люди не суть смертны”. Это суждение мы должны признать ложным, потому что мы
    признаём- истинным суждение А — “все люди смертны”. Следовательно, при ложности О суждение А — истинно.</P>
<P>Если я утверждаю, что все люди смертны, и вы со мной соглашаетесь, т. е. находите, что это суждение истинно, то вы
    должны будете признать, что при допущении истинности этого суждения нельзя признать истинности суждения О —
    “некоторые люди не смертны”, и, наоборот, если признать истинность суждения О — “некоторые люди не суть честны”, то
    никак нельзя будет признать истинности суждения А — “все люди честны”.</P>
<P>Таким образом, из двух противоречащих суждений при истинности одного суждения другое оказывается ложным, при ложности
    одного суждения другое является истинным. Из этого следует, что из противоречащих суждений одно должно быть
    истинным, а другое — ложным. Два противоречащих суждения не могут быть в одно и то же время оба истинными, но не
    могут быть и оба ложными.</P>
<P>Противность (А — Е). Если признать суждение А — “все металлы суть элементы” истинным, то никак нельзя допустить, что
    “ни один металл не есть элемент”. Следовательно, если А истинно, то Е ложно. Если мы признаём суждение Е — “ни один
    человек не всеведущ” истинным, то мы, конечно, не будем иметь никакого права утверждать суждение А — “все люди
    всеведущи”. Следовательно, если Е истинно, то А ложно. Таким образом, из истинности одного из противных суждений
    следует ложность другого.</P>
<P>Но следует ли из ложности А истинность Е или из ложности Е истинность А? Отнюдь нет. В этом мы можем убедиться из
    следующих примеров. Возьмём суждение А — “все бедняки порочны” — и признаем, что это суждение ложно. Можно ли в
    таком случае утверждать суждение Е — “ни один бедняк не порочен”? Конечно, нельзя, потому что в действительности
    может оказаться, что только некоторые бедняки не порочны, а некоторые — порочны. Если я выскажу суждение Е — “ни
    один алмаз не драгоценен” — и вы станете отрицать истинность этого :уждения, то сочтёте ли вы себя вправе
    утверждать, что “все алмазы драгоценны”? Конечно, нет. Отрицая моё утверждение, зы в свою очередь можете только
    утверждать, что “некоторые. элмазы драгоценны”, допуская в то же время, что “некоторые алмазы не драгоценны”.
    Следовательно, при ложности одного из. противных суждений нельзя признать истинность другого, потому что между ними
    всегда может быть нечто среднее.</P>
<P>Итак, в двух противных суждениях из истинности одного следует ложность другого, но из сложности одного не следует
    истинность другого; оба суждения не могут быть истинными (потому что если одно истинно, то другое ложно), но оба
    могут быть ложными (потому что при ложности одного ложным может быть другое).</P>
<P>Подчинение (А—I, Е—О). Если А истинно, то I тоже, истинно. Например, если суждение А — “все алмазы драгоценны” —
    истинно, то истинно суждение I — “некоторые алмазы драгоценны”. Если Е истинно, то О тоже истинно. Если “ни один
    человек не всеведущ”, то, конечно, это предполагает, что “некоторые люди не всеведущи”. От истинности общих
    суждений, следовательно, зависит истинность частных.</P>
<P>Но можно ли сказать, наоборот, что от истинности частных суждений зависит истинность общих суждений? Нельзя. В самом
    деле, если I истинно, то А может не быть истинно. Например, суждение I — “некоторые люди мудры” — истинно. Будет ли
    следствие этого истинным суждение А — “все люди мудры”? нет. Если О истинно, то Е может быть не истинно. Если мы
    признаём истинным О — “некоторые люди не искренни”, то можем и мы вследствие этого признать истинным суждение Е —
    “ни один человек не искренен”? Конечно, нет.</P>
<P>Ложность общего суждения оставляет неопределённой важность и истинность подчинённого частного. При отрицании
    истинности А мы не можем сказать, будет ли I истинным или ложным. При отрицании истинности Е мы не можем ни
    утверждать, ни отрицать истинности О. Если мы, например, отрицаем истинность А — “все люди честны”, то мы можем
    признавать тинным суждение I — “некоторые люди честны”. Если мы отрицаем суждение истинности Е — “ни один человек не
    есть мудр<I>”,</I> то мы можем признавать истинность О — “некоторые люди не суть мудры”. .</P>
<P>Но ложность частного приводит к ложности общего. Если южно, то А ложно. Если нельзя сказать “некоторые люди
    всеведущи”, потому что это ложно, то тем более нельзя сказать се люди всеведущи”. Если О ложно, то Е ложно. Если
    нельзя сказать “некоторые люди не суть смертны”, то нельзя сказать и один человек не есть смертей”, потому что если
    чего-нибудь нельзя утверждать относительно части класса, то этого же тем более нельзя утверждать относительно всего
    класса.</P>
<P>Таким образом, истинность частного суждения находится в зависимости от истинности общего суждения, но не наоборот;
    ложность частного приводит к ложности общего, но не наоборот.</P>
<P>Подпротивная противоположность (I—О). Если I истинно, то О может быть истинно. Если истинно суждение “некоторые люди
    мудры”, то что сказать о суждении “некоторые (другие) люди не суть мудры”? Это суждение может быть истинным, потому
    что одни люди могут быть мудрыми, а другие — немудрыми. Если О истинно, то I может быть истинно. Если мы скажем, что
    “некоторые люди не суть искренни”, то мы в то же время можем предполагать, что “некоторые люди суть искренни”; одно
    суждение не исключает другого. Таким образом, суждения I и О могут быть в одно и то же время истинными.</P>
<P>Если I ложно, то О истинно. Если нельзя сказать “некоторые люди всеведущи”, то это происходит оттого, что истинно
    противоречащее суждение Е — “ни один человек не есть всеведущ”, а если это суждение истинно, то истинно подчинённое
    суждение О — “некоторые люди не суть всеведущи”.</P>
<P>Если О ложно, то I истинно. Если ложно, что “некоторые люди не суть смертны”, то это происходит от истинности
    противоречащего суждения “все люди смертны”, а из истинности этого суждения следует истинность подчинённого суждения
    “некоторые люди смертны”.</P>
<P>Следовательно, оба подпротивных суждения могут быть в одно. и то же время истинными, но оба не могут быть ложными
    (потому что при ложности одного суждения другое является истинным).</P>
<P>Наибольшая противоположность. Мы рассмотрели пары суждений противных и противоречащих. Спрашивается: какие суждения
    представляют наибольшую противоположность? Нужно думать, что таковыми являются суждения А и Е; между этими
    суждениями возникает наибольшая противоположность, когда мы их сопоставляем друг с другом. Если кто-нибудь скажет,
    что “все книги содержат правду”, и мы на это замечаем, что “ни одна книга не содержит правды”, то противоположность
    между первым суждением и вторым чрезвычайно велика. Не так велика будет противоположность в том случае, если на
    утверждение “все книги содержат правду” мы скажем, что “некоторые книги не содержат правды”. Из этих примеров видно,
    что противоположность между А и Е больше, чем между А и О, т. е. несогласие больше в первом случае, чем во втором.
    Таким образом, наибольшая противоположность содержится в суждениях противных. Эта противоположность называется
    диаметральной.</P>
<P>Но хотя наибольшая противоположность существует между суждениями противными, однако при опровержении суждений
    обще-утвердительных и обще-отрицательных гораздо удобней пользоваться суждениями противоречащими, а не противными,
    потому что гораздо меньше риска в утверждении I или О, чем в утверждении А или Е. Предположим, кто-нибудь утверждает
    — “все книги полезны”. Это утверждение можно отвергнуть, показав, что “ни одна книга не полезна”, но можно
    отвергнуть, показав, что “некоторые книги не полезны”. Этот второй способ опровержения предпочтительнее по следующей
    причине. В самом деле, если мы покажем, что “некоторые книги не полезны”, то этого вполне достаточно для того, чтобы
    отвергнуть положение “все книги полезны”. Гораздо легче показать бесполезность только некоторых книг, чем показать,
    что ни одна книга не полезна. Гораздо меньше риска утверждать О, чем , утверждать Е. По этой причине мы редко
    опровергаем общеутвердительное суждение при помощи обще-отрицательного, но гораздо чаще при помощи противоречащего
    частно-отрицательного. То же самое справедливо относительно другой пары противоречащих суждений.</P>
<P>Всё сказанное выше об отношении суждений можно изобразить при помощи следующей таблицы:</P>

    <TABLE BORDER CELLSPACING=1 CELLPADDING=7 WIDTH=554>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>Если A истинно,&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>То E ложно,&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O ложно,&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I истинно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>E</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A ложно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I ложно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O истинно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>I</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>E ложно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>O</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>E неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A ложно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>Если A ложно,&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>E неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O истинно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>E</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A неопределенно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I истинно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O неопределенно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>I</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A ложно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>E истинно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>O истинно&nbsp;</TD>
        </TR>
        <TR>
            <TD WIDTH="24%" VALIGN="TOP">
                <P>O</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>A истинно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>E ложно&nbsp;</TD>
            <TD WIDTH="25%" VALIGN="TOP">
                <P>I истинно&nbsp;</TD>
        </TR>
    </TABLE>

<P>Эту таблицу учащийся не должен знать наизусть, но должен уметь её вывести.</P>
<h2>Вопросы для повторения</h2>
<P>Какие суждения называются противоположными? Изобразите логический квадрат. Какие суждения называются противоречащими?
    Какое отношение противоположения существует между противоречащими суждениями? Какие суждения называют противными?
    Какое отношение противоположения существует между противными суждениями? Какие суждения называют суждениями
    подчинения? Какое отношение противоположения существует между суждениями подчинения? Какие суждения называются
    суждениями подпротивными? Какое отношение противоположения существует между суждениями: подпротивными? Между какими
    суждениями существует наибольшая противоположность? Почему обще-утвердительное суждение лучше опровергать
    частно-отрицательным, чем обще-отрицательным?</P>

    <Table width=80%>
        <tr>
            <td align=left>
                <A href="<?php
                echo Url::to(['uchebnik']); ?>">Содержание</A>
            </td>
            <td align=right>
                <A href="<?php echo Url::to(['uchebnik-glava', 'id' => '11']); ?>">Дальше</A>
            </td>
        </tr>
    </table>

</div>