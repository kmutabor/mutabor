<?php
use yii\helpers\Url;

$this->title = 'Учебник логики. Глава 17.';
?>

<div class="book_site">

<h2>Глава XVII<br>Сокращённые и сложные силлогизмы</h2>
<P>Сокращённые силлогизмы. Перейдем к рассмотрению тех силлогизмов, которые называются сокращёнными и сложными
    силлогизмами; они по форме отличаются от обыкновенных. Некоторые утверждали, что мы в мышлении никогда не пользуемся
    силлогизмами. Но это неправильно, потому что в обиходной жизни мы пользуемся весьма часто силлогизмом, но только он
    не всегда бывает выражен полно, и именно оттого, что некоторые части его бывают выпущены. Эти силлогизмы называются
    сокращёнными, или также энтимемами. Это название происходит от греческого слова evqounua, Энтимема —это такой
    силлогизм, часть которого мы держим в уме (ev qvuw), а часть выражаем. Мы можем выбрасывать каждую- часть силлогизма
    и мыслить всё-таки силлогистически. Например, если мы относительно кого-нибудь употребим выражение: “нужно быть
    дурным человеком, чтобы делать подобные вещи”, то это выражение представляет собой силлогизм, который, если мы ему
    придадим полную форму, приобретёт следующий вид:</P>
<P>Все люди, которые делают подобные вещи, дурны. Этот человек делает подобные вещи. </P>
<P>Следовательно, он дурной человек</P>
<P>Для того чтобы пояснить, как происходит этот пропуск Частей силлогизма, возьмём какой-нибудь полный силлогизм,
    например:</P>
<P>Всякий порок заслуживает порицания. Скупость есть порок.</P>
<P>Следовательно, скупость заслуживает порицания,</P>
<P>Этим примером можно воспользоваться для того, чтобы иллюстрировать следующие три вида энтимемы</P>
<I><P>Вид 1:</P>
</I><P>Скупость заслуживает порицания, потому что она есть порок. (Здесь пропущена большая посылка.)</P>
<I><P>Вид 2:</P>
</I><P>Скупость заслуживает порицания, потому что всякий порок .заслуживает порицания. (Здесь пропущена меньшая
    посылка.)</P>
<I><P>Вид 3;</P>
</I><P>Всякий порок заслуживает порицания, скупость же есть порок..;</P>
<P>(Здесь пропущено заключение и именно потому, что оно очевидно.)</P>
<P>Эпихейрема. Есть, наконец, ещё один вид сокращённых силлогизмов, который называется эпихейремой. Это такой силлогизм,
    в обе посылки которого входят энтимемы.</P>
<P>Схема эпихейремы:</P>
<P><I>М</I> есть <I>Р,</I> так как оно есть <I>N. S</I> есть <I>М,</I> так как оно есть О.</P>
<P>Следовательно, <I>S</I> есть <I>Р</I>.</P>

<P>Первая посылка должна была бы быть выражена так:</P>
<P>Все <I>N</I> суть <I>Р</I>.</P>
<P>Все М<I> суть </I>N.</P>
<P><I>Следовательно, </I>М<I> есть </I>Р.</P>
<P>Вторая посылка должна была бы быть выражена так:</P>
<P>Все <I>О суть </I>М. <I>Все S суть О</I>.</P>
<P>Следовательно, все S суть <I>М</I>.</P>

<I><P>Пример:</P>
</I><P>Ложь; заслуживает презрения, так как она безнравственна.</P>
<P>Лесть есть ложь, так как она есть умышленное извращение истины.</P>
<P>Следовательно, лесть должна быть презираема.</P>
<P>В этом силлогизме, как это легко видеть, каждая из посылок есть суждение, которое представляет собой заключение со
    средним термином; если же дать заключение со средним термином, то этого вполне достаточно, для того чтобы
    восстановить весь силлогизм.</P>
<P>Теперь рассмотрим те силлогизмы, которые называются сложными.</P>
<P>Полисиллогизмы. Может случиться, и собственно в научной мысли весьма часто бывает, что мы несколько силлогизмов
    соединяем в один, и тогда получается то, что называется цепью силлогизмов — полисиллогизм.</P>
<P>Соединение силлогизмов происходит таким образом, что заключение одного силлогизма является посылкой для другого; тот
    силлогизм, который предшествует, называется просиллогизмом; тот силлогизм, который следует после, называется
    эписилпогизмом.</P>
<P>Схема полисиллогизма будет следующая:</P>
<P>Психологизм:</P>
<P>Все <I>В</I> суть <I>А</I></P>
<P>Все <I>С</I> суть B </P>
<P>Следовательно, все <I>С</I> суть <I>А</I></P>
<P>Эписилогизм:</P>
<P>Все <I>С</I> суть <I>А</I></P>
<P>Все <I>D суть С</I></P>
<P>Следовательно, все <I>D</I> суть <I>А</I></P>
<P>Есть два типа полисиллогизмов. В первом умозаключение идет от более общего к менее общему, во втором, наоборот,
    умозаключение идёт от менее общего к более общему. Первый тип называется прогрессивным, второй — регрессивным.</P>
<P>Пример прогрессивного полисиллогизма:</P>
<P>Все позвоночные имеют красную кровь.<br>
    Все млекопитающие суть позвоночные.<br>
    Все млекопитающие имеют красную кровь.<br>
    Все млекопитающие имеют красную кровь.<br>
    Все хищные суть млекопитающие.<br>
    Все хищные имеют красную кровь.<br>
    Все хищные имеют красную кровь.<br>
    Тигры суть хищные животные.<br>
    Тигры имеют красную кровь.</P>
<P>Здесь умозаключение идёт от более общего к менее общему (позвоночные, млекопитающие, хищные, тигры), т. е. шествует
    вперёд по отношению к содержанию, так как в частных понятиях содержание больше. Пример регрессивного
    полисиллогизма:</P>
<P>Позвоночные суть животные.<br>
    Тигры суть позвоночные.<br>
    Тигры суть животные.<br>
    Животные суть организмы.<br>
    Тигры суть животные.<br>
    Тигры суть организмы.<br>
    Организмы разрушаются.<br>
    Тигры суть организмы.<br>
    Тигры разрушаются.</P>
<P>Здесь умозаключение идёт от менее общего к более общему (позвоночное, животное, организм, разрушимое).</P>
<P>Сориты. Иногда при соединении нескольких силлогизмов для плавности мысли мы можем пропускать некоторые посылки. В
    таком случае получается то, что называется соритом (от греч. qopos —куча). Существует два вида соритов: 1)
    аристотелевский, когда выбрасывается меньшая посылка каждого отдельного силлогизма, и 2) гоклениевский, когда
    выбрасывается большая посылка отдельных силлогизмов. Возьмём примеры:</P>
<P>Аристотелевский сорит.</P>
<P>Буцефал есть лошадь.<br>
    Лошадь есть четвероногое.<br>
    Четвероногое есть животное.<br>
    Животное есть субстанция.<br>
    Буцефал есть субстанция.</P>
<P>Если бы этому сориту мы придали полную форму, т. е. восстановили бы опущенные посылки, то у нас получилось бы
    следующих три силлогизма:</P>
<P>1) Лошадь есть четвероногое.</P>
<P>Буцефал есть лошадь.<br>
    Буцефал есть четвероногое</P>
<P>2) Четвероногое есть животное.</P>
<P>(Буцефал есть четвероногое).<br>
    Буцефал есть животное.</P>
<P>3) Животное есть субстанция.</P>
<P>(Буцефал есть животное).<br>
    Буцефал есть субстанция</P>
<P>2. Гоклёниёвский сорит.</P>
<P>Животное есть субстанция.<br>
    Четвероногое есть животное.<br>
    Лошадь есть четвероногое.<br>
    Буцефал есть лошадь.<br>
    Буцефал есть субстанция.</P>
<P>Это есть гоклениевский сорит, потому что выпущены большие посылки.</P>
<P>Если бы мы восстановили пропущенные посылки, то у нас получился бы следующий ряд силлогизмов:</P>
<P>1) Животное есть субстанция. Четвероногое есть животное. Четвероногое есть субстанция.</P>
<P>2) [Четвероногое есть субстанция]. Лошадь есть четвероногое. Лошадь есть субстанция.</P>
<P>3) [Лошадь есть субстанция]. Буцефал есть лошадь. Буцефал есть субстанция.</P>
<h2>Вопросы для повторения</h2>
<P>Что такое энтймема и сколько типов энтимем мы различаем? Что такое эпихейрема? Что такое полисиллогизм? Что такое
    просиллогизм и эписиллогизм? Какое различие между просиллогизмом прогрессивным и регрессивным? Что такое сорит?
    Какое различие между соритом аристотелевским и гоклениевским?.</P>

    <Table width=80%>
        <tr>
            <td align=left>
                <A href="<?php
                echo Url::to(['uchebnik']); ?>">Содержание</A>
            </td>
            <td align=right>
                <A href="<?php echo Url::to(['uchebnik-glava', 'id' => '18']); ?>">Дальше</A>
            </td>
        </tr>
    </table>


</div>