<?php
use yii\helpers\Url;

$this->title = 'Искусство спора. Глава 22.';
?>

<div class="book_site">

<h2><P>Глава 22.</P>

    <P>«Мнимые доказательства»</P>
</h2>
<h3><P>Тождесловие. «Довод слабее тезиса». Обращенное доказательство. Круг в доказательстве.</P>
</h3><P>87:</P>
<P>1. К софизмам произвольного довода относятся часто те мнимые доказательства, в которых или а) в виде довода
    приводится для доказательства тезиса тот же тезис, только в других словах, — это будет софизм тождесловия (idem per
    idem); или б) доказательство как бы «перевертывается вверх ногами». Мысль достоверную или более вероятную делают
    тезисом, а мысль менее вероятную — доводом для доказательства этого тезиса, хотя правильнее было бы сделать как раз
    наоборот. Этот софизм можно назвать «обращенным доказательством». Наконец в) в одном и том же споре, в одной и той
    же системе доказательств сперва делают тезисом мысль А и стараются доказать ее с помощью мысли Б; потом, когда
    понадобится доказать мысль Б, доказывают ее с помощью мысли А. Получается круговая порука: А верно потому, что
    истинно Б; а Б — истинно потому, что верно А. Такой софизм называется «ложным кругом» или «кругом в доказательстве»,
    или «заколдованным кругом». Иногда он бывает и в скрытой форме. А доказывается с помощью Б, но Б нельзя доказать
    иначе, как с помощью А.</P>
<P>Те же самые названия, как софизмы, носят и соответственные ошибки.</P>
<P>2. Тождесловие встречается часто, гораздо чаще, чем мы это замечаем. Уже было отмечено выше, как иногда трудно под
    разными словами отличить одну и ту же мысль. Особенно, если она выражена хотя бы в одном из случаев запутанно,
    трудно, туманно. Иногда тождесловие имеет грубую форму, например, «поверьте, нельзя не быть убежденному: это
    истина». (Гоголь. Театральный разъезд). Или другой пример, из ученических сочинений: «Это не может не быть правдой,
    потому что это истина». Но часто тождесловие скрывается под очень тонкими формами. И не всегда можно точно
    установить, две ли почти одинаковые мысли пред нами или одна и та же. Для доказательства, впрочем, и тот и другой
    случай обычно одинаково непригодны. Когда А. Пушкин жалуется на критиков: «наши критики говорят обыкновенно: это
    хорошо, потому что прекрасно; а это дурно, потому что скверно. Отселе их никак не выманишь» — он обвиняет их в
    тождесловии.</P>
<P>Или вот еще пример тождесловия: «начало вселенной без Творца немыслимо; потому что немыслимо, чтобы она возникла
    самопроизвольно, сама собою». Или — из (88:) области более отвлеченной, философской. Каждое свойство, качество и
    т.д. есть качество и свойство чего-нибудь, т.е. одно само по себе существовать не может. На этом основании многие
    философы — а в старину все — принимали существование кроме свойств еще отдельных от них «носителей свойств» (так
    наз. субстанции).</P>
<P>И вот один знаменитый философ (XVIII века) пишет: «признано…, что протяжение, движение, одним словом, все ощущаемые
    качества нуждаются в носителе, так как существовать сами по себе не могут». (Беркли. Трактат, § 91). Подчеркнутые
    мною довод и тезис есть чистейшее тождесловие. Надо заметить, между прочим, что чем отвлеченнее вопрос, тем больше
    опасности (при прочих условиях равных) впасть в тождесловие.</P>
<P>3. При произвольных доводах очень нередко случается, что приводимый довод еще менее приемлем для лица, которому
    предназначен, еще сомнительнее, чем самый тезис. Например, тезис: «Бога нет», а довод: «Бога выдумали угнетатели,
    чтобы поработить слабых». Или тезис: «в данном случае позволителен обман», а довод: «нравственности никакой не
    существует. Все это одни условности» и т.д. Обращенное доказательство соединяет эту ошибку с любопытной особенностью
    некоторых пар суждений.</P>
<P>Есть такие пары суждений, в которых любое суждение может служить доводом для другого, если это другое поставить
    тезисом, но и наоборот, второе может служить доводом для первого. Все зависит от того, какое из них мы признаем
    более вероятным и приемлемым. Например, такова следующая пара суждений: «недавно шел сильный дождь» и «на улицах
    теперь грязно». Если мы знаем, что недавно шел дождь, то можем сделать вывод, что на улицах грязно. Если же
    наоборот, знаем только, что на улицах грязно, то можем сделать вывод, что недавно шел дождь… Так что возможны из тех
    же двух мыслей два доказательства. Можно мысль А доказывать из мысли Б; можно как бы перевернуть, «обратить» это
    доказательство и мысль Б доказывать из мысли А. Смотря по тому, что мы считаем вероятнее — А. или Б.</P>
<P>И вот когда ошибка, о которой мы говорили выше, («довод слабее тезиса») происходит в связи с такой парой мыслей,
    тогда чаще всего и получается то, что можно называть обращенным доказательством (hysteron proteron). Мысль А.
    доказывается мыслью Б. Но мысль Б слабее мысли А и правильно было бы доказывать именно только наоборот: мысль Б
    основывать на мысли А. Например, скажем, кто-нибудь утверждает, что данный поступок — наш долг (довод);
    следовательно, — это поступок хороший (тезис). Но для нас довод его слабее тезиса. Мы совершенно не уверены, что
    данный поступок наш долг, а скорее согласились бы с тем, что это хороший поступок. Поэтому правильнее было бы с
    нашей точки зрения, если б тезис стал на место довода, а довод на место тезиса. Т.е. мы видим в данном случае ошибку
    или софизм обращенного доказательства.</P>
<P>Так как здесь все зависит от того, какое из двух так связанных логически суждений мы признаем сильнее, вероятнее
    другого, а подобные оценки у каждого из нас могут быть различны, то данного рода ошибка становится очень
    неопределенной и субъективной. Для одного данное доказательство совершенно правильно; для другого оно — обращенное
    доказательство. Например, дано такое доказательство: Бог существует (довод); значит, существует и нравственный закон
    (тезис). Его многие (c. 89:) считают совершенно правильным. Но для кантианца это доказательство будет ошибочным,
    обращенным доказательством. Для кантианца правильно наоборот, доказывать, что раз нравственный закон существует
    (довод), то значит, Бог есть (тезис) и т.д. Благодаря этой субъективности оценок софизм данного рода часто
    неуловим.</P>
<P>4. На такой же связи между суждениями основана чаще всего и общеизвестная ошибка — «ложный круг» в доказательстве.
    Разница обычно лишь в том, что при ней оба доказательства, и правильное и обращенное, приводятся в одном и том же
    споре (или книге и т.д.), одним и тем же лицом. Выходит, что сперва спорщик доказывал мысль А с помощью мысли Б; а
    когда потребовалось доказать Б, он стал его доказывать с помощью А. Получился заколдованный круг. Например, сначала
    Х. доказывал, что «река, должно быть, стала (тезис), потому что ночью был сильный мороз» (довод), а потом начинает
    доказывать, что «ночью, должно быть, был сильный мороз (тезис), потому что река стала» (довод). Чаще всего впадают в
    ложный круг люди, которые сами лично одинаково уверены в истинности и тезиса и довода. Поэтому, когда приходится
    доказывать мысль А, они берут в качестве довода мысль Б, связанную с нею вышеуказанной связью; но потом, когда
    потребуется доказывать мысль Б, они забывают, что пустили уже раз в ход связь между этими двумя мыслями, и приводят
    в виде довода мысль А. Ведь для них-то они одинаково достоверны. Например, для правоверного магометанина одинаково
    несомненны две мысли: «все, что написано в Коране, до последней черты, истина» и «Коран боговдохновенен». Мысли эти
    стоят в тесной логической взаимной связи. И вот, когда надо доказывать какую-нибудь из них, он не задумывается
    пустить в ход другую. Если же потребуется доказать и эту, то, забыв о только что пущенном в ход доказательстве,
    может легко пустить в ход и обращенное. Это бывает особенно часто в длинных спорах, длинных статьях, книгах и т.п.,
    где такое забвение вполне возможно. Таким образом, получится ложный круг. «В Коране все истина, потому что Коран
    боговдохновенен», и «Коран боговдохновенен потому, что в нем все истина, до последней черты» и т.д.</P>
<P>То, что впавший в такую ошибку делает по забвению, софист проделывает из доброй воли и сознательно. Сейчас он вам
    доказывал, что воля Божья совершенна, потому что абсолютно основана на нравственных принципах, которые совершенны;
    если же вы его, немного погодя, спросите, почему же он считает нравственные принципы «совершенными» и не существует
    ли нечто высшее, чем нравственные принципы? Он может ответить: нравственные принципы — принципы воли Божьей, которая
    совершенна или т.п.</P>
<P>Ложный круг, как софизм и ошибка встречаются часто, гораздо чаще, чем мы замечаем его.</P>

    <Table width=80%>
        <tr>
            <td align=left>
                <A href="<?php echo Url::to(['spor']); ?>">Содержание</A>
            </td>
            <td align=right>
                <A href="<?php echo Url::to(['spor-glava', 'id' => '23']); ?>">Дальше</A>
            </td>
        </tr>
    </table>

    </div>
