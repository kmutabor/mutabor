<?php
use yii\helpers\Url;

$this->title = 'Искусство спора. Примечания.';
?>

<div class="book_site">

<h2><P>Примечания</P>
</h2><DIV class=section ID=FbAutId_1  cs1=29><h2><P>1</P>
    </h2><P>То же относится и к термину «приватизация земли» — Прим. ред.</P>
</DIV><DIV class=section ID=FbAutId_2  cs1=29><h2><P>2</P>
    </h2><P>Изредка можно для него воспользоваться и фактом опровержения доказательства данного тезиса, — но при особых условиях, о которых говорить здесь не место.</P>
</DIV><DIV class=section ID=FbAutId_3  cs1=29><h2><P>3</P>
    </h2><P>Например, тезис: «Это — насекомоядное животное»; узкий антитезис к нему — это «травоядное животное». Наоборот, широкий или формальный антитезис, сводится. всегда только к чистому отрицанию тезиса, и потому, если тезис ложен, всегда антитезис истинен. «Тезис: „это насекомоядное животное“, широкий антитезис; „это не насекомоядное животное“.</P>
</DIV><DIV class=section ID=FbAutId_4  cs1=29><h2><P>4</P>
    </h2><P>Однако одним отечественным логиком была сделана попытка ввести их в область логики. Рассматривая «софизмы практической жизни и деятельности такого или другого лица в частности» и т.п., он говорит: «наилучшею логикой для свержения таких софизмов служа: 1) религия и закон гражданский, блюстители и исполнители их, как—то: пастыри церковные и власти государственные, 3) главы семейств и все старшие по отношению к подчиненным им… 4) Софизмы государственные могут быть разрешаемы и опровергаемы только верховными властями, через посредство дипломатических сношений и переговоров или вооруженною силою» (Коропцев, П. Руководство к начальному ознакомлению с логикой. СПб., 1861, 192). Надо сказать, что в этих словах сформулирована довольно популярная и в наше время система «разоблачения софизмов».</P>
</DIV><DIV class=section ID=FbAutId_5  cs1=29><h2><P>5</P>
    </h2><P>Впрочем, бывают времена, когда эта уловка делается «устаревшей». В моде благородная «искренность» и «откровенность». «Я вор? Ну да, вор. А кто теперь не ворует?», «теперь только дураки зевают» и т.д. и т.д. Так что софист должен с большой осторожностью применять это оружие.</P>
</DIV><DIV class=section ID=FbAutId_6  cs1=29><h2><P>6</P>
    </h2><P>Софизмами называются иногда (в узком смысле слова) сложные рассуждения, явно ошибочные, но ошибку в которых трудно найти, своего рода головоломки. Таковы древние софизмы Зенона, софизмы Эватла, крокодил и т.п.</P>
</DIV><DIV class=section ID=FbAutId_7  cs1=29><h2><P>7</P>
    </h2><P>Встречается как в начале, так и в середине спора.</P>
</DIV><DIV class=section ID=FbAutId_8  cs1=29><h2><P>8</P>
    </h2><P>Знаменитый английский биолог и зоолог 60-х и 70-х годов XIX столетия. — П.</P>


        <Table width=80%>
            <tr>
                <td align=left>
                    <A href="<?php
                    echo Url::to(['spor']); ?>">Содержание</A>
                </td>
                <td align=right>
                    конец
                </td>
            </tr>
        </table>

        </DIV>