<?php
use yii\helpers\Url;

$this->title = 'Искусство спора. Глава 2.';
?>

<div class="book_site">

<h2><P>Глава 2.</P>

    <P>О доказательствах (продолжение)</P>
</h2>
<h3><P>О доводах. «Связь в доказательстве». Ошибки: в тезисе, в доводах и в связи.</P>
</h3><BR><P>1. В доказательство истинности или ложности тезиса мы приводим другие мысли, так называемые доводы или
    основания доказательства. Это должны быть такие мысли: а) которые считаем верными не только мы сами, но и тот
    человек или те люди, кому мы доказываем и б) из которых вытекает, что тезис истинен или ложен.</P>
<P>Конечно, если мы приведем такой довод, который наш собеседник не признает верным, то это будет промах. Нужно будет
    или доказать истинность самого этого довода, а потом уже опираться на него при доказательстве тезиса; или же искать
    другого, более удачного довода. Например, если я хочу кому-нибудь доказать, что «работать надо», а в виде довода
    прибавлю, «потому что так Бог велит», то такой довод будет годиться только для верующего. Если же человек не верит в
    Бога, а я приведу ему этот довод, то, конечно, ничего ему не докажу. Затем, как сказано, надо, чтобы из довода
    вытекала истинность тезиса; надо чтобы тезис и основания (доводы) были так связаны, что кто признает верным довод,
    тот должен необходимо признать верным и тезис. Если эта связь сразу не видна, надо уметь показать, что она есть.</P>
<P>Без этого тоже доказательство — не доказательство. Например, если кто хочет доказать, что «у нас скоро хлеб
    подорожает» и приведет довод: «в Америке вчера было землетрясенье», то такое доказательство меня не убедит. Да, в
    Америке было землетрясенье. Довод верен. Но он «ничего не доказывает». Какая же связь между этим доводом и тезисом,
    что «скоро хлеб у нас подорожает»? Может быть, и связь эта есть, но я-то ее не вижу. Покажи ее — и тогда будет
    настоящее доказательство. А пока не вижу этой связи, никакой, самый верный довод, меня не убедит.</P>
<P>Таким образом, вот что нужно для доказательства, кроме тезиса: а) основания (доводы) и б) связь между ними и
    тезисом.</P>
<P>2. Каждый важный довод в доказательстве надо рассмотреть отдельно и тоже выяснить, — так же выяснить, как мы выяснили
    тезис. Ведь если мы не вполне поймем довод, то разве можем вполне уверенно сказать, что он истинен или что он ложен?
    Эта работа выяснения и здесь совершенно необходима. Надо только научиться делать ее скоро. И кто попробовал
    проделывать ее при доказательствах, тот вполне оценит, от скольких ошибок и траты времени она оберегает. Не надо
    доверяться «первому взгляду» и думать, что выяснять не требуется. Эта-то наша самая обычная человеческая ошибка, что
    многие мысли нам кажутся вполне ясными. но придет случай, затронет противник такую мысль, и окажется, что мысль эта
    для нас совсем не ясна, напротив, очень туманна и иногда даже ложно понята нами. Тогда мы можем стать в споре в
    очень нелепое положение. Иллюзия ясности мысли — самая большая опасность для человеческого ума. Типичные примеры ее
    находили в беседах Сократа (насколько они переданы в диалогах Ксенофонта и Платона). Подходит к нему какой-нибудь
    юноша или муж, которому «все ясно» в той или иной мысли. Сократ начинает ставить вопросы. В конце концов,
    оказывается, что у собеседника иллюзия ясности мысли прикрывает тьму и непроходимые туманы, в которых гнездятся и
    кроются самые грубые ошибки.</P>
<P>3. Ошибки в доказательствах бывают, главным образом, трех видов: а) или в тезисе, или б) в доводах (в основаниях),
    или в) в связи между доводами и тезисом, в «рассуждении».</P>
<P>Ошибки в тезисе состоят в том, что мы взялись доказывать один тезис, а на самом деле доказали или доказываем другой.
    Иногда это тезис, сходный с настоящим тезисом или как-нибудь с ним связанный, иногда же — и без всякой видимой
    связи. Эта ошибка называется отступлением от тезиса. Примеры ее встречаются на каждом шагу в споре. Например,
    человек хочет доказать, что православие — плохая вера, а доказывает, что православные священники часто плохи. Или
    хочет доказать, что нерассудительный человек глуп, а доказывает, что глупый человек не рассудителен. А это вовсе не
    одно и то же. Отступления от тезиса бывают самые разные. Можно вместо одной мысли доказывать похожую на нее, но
    все-таки другую мысль, а можно заменить ее и совсем не похожей другою мыслью. Бывает, что человек видит, что тезиса
    ему не защитить или не доказать — и нарочно подменивает его другим, так чтобы противник не заметил. Это называется
    подменой тезиса. Бывает и так, что прямо человек забыл свой тезис. Спрашивает потом: «с чего, бишь, мы начали
    спор?». Это будет потеря тезиса и т.д.</P>
<P>4. Ошибки в доводах бывают чаще всего две: а) ложный довод, б) произвольный довод. Ложный довод, — когда кто
    опирается на явно ложную мысль. Например, если кто в доказательство тезиса скажет, что земля держится на трех китах,
    мы, конечно, этого довода не примем, сочтем его за ложный. Произвольный же довод — такой, который хотя и не заведомо
    ложен, но еще сам требует должного доказательства. Например, если кто в доказательство тезиса приведет мысль, что
    «скоро будет конец мира» — то это будет произвольный довод. Мы можем потребовать других доводов, а этого не принять,
    или потребуем, чтобы этот довод был доказан.</P>
<P>5. Наконец, ошибки в «связи» между основаниями и тезисом («в рассуждении») состоят в том, что тезис не вытекает, не
    следует из оснований, или же не видно, как он следует из них. Например, скажем, кто-нибудь доказывает: «у нас будет
    в этом году неурожай». — Почему ты так думаешь? — «А потому, что на солнце много стало пятен». Естественно,
    большинство из нас спросит, какая же связь здесь между тезисом и основанием. Не видно, как истинность тезиса следует
    из этого основания. Или если кто заявит: «Наполеон носил серую куртку и К. носит серую куртку, значит К. —
    Наполеон». Тут мы прямо скажем, что нет связи между основаниями и тезисом; неправильно человек рассуждает.</P>
<P>6. Какие бывают ошибки в рассуждении, подробнее учит логика. Без нее в подробности входить нельзя. У кого ум
    «способен к доказательствам», тот легче, конечно, может найти в них ошибку, чем менее способный. Здравый смысл да
    навык думать оказывают при этом большие услуги. Но, в общем, часто ошибку найти трудно, если доказательство сложное.
    Иногда и чувствуешь что-то да не так, а где ошибка, определить не можешь. Вот тут-то и помогает знание логики на
    практике.</P>

    <Table width=80%>
        <tr>
            <td align=left>
                <A href="<?php echo Url::to(['spor']); ?>">Содержание</A>
            </td>
            <td align=right>
                <A href="<?php echo Url::to(['spor-glava', 'id' => '3']); ?>">Дальше</A>
            </td>
        </tr>
    </table>

    </div>