<?php
$this->title = 'Силлогизмы';
?>
<style>
    select{
        width: 150px !important;
     }
    input[type=text]{
        width: 350px !important;

    }
    input[name='s_type']{
        display: none;
    }
</style>

<div class="theme-form">

    <?php
    use app\assets\LogicAsset;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use yii\helpers\Html;

    $bundle = LogicAsset::register($this);

    $session = Yii::$app->session;
    $success_msg = $session->getFlash('success_msg');
    $error_msg = $session->getFlash('error_msg');
    if(isset($success_msg)){
        echo Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => $session->getFlash('major_sil'),
        ]);
        echo Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => $session->getFlash('minor_sil'),
        ]);
        echo Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => $success_msg,
        ]);
    }else if(isset($error_msg)){
        echo Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => $error_msg,
        ]);
    }

//    var_dump($bundle);
    $form = ActiveForm::begin(
        [
//            'options' => ['class' => 'form-vertical'],
        ]
    );
    ;
    ?>
    <div class="book_site syllogism centered">
<p>
    Возможные модусы силлогизма (распределены по фигурам).
</p>
<?= Html::img($bundle->baseUrl.'/icons/logic/moduses.png',
    [
        'class'=>'img',
    ]
); ?>
<p>
    Выберите фигуру и в соответствии с ней постройте силлогизм.
</p><br/>
<?= Html::radioList('s_type',$figura_selected,
    [
        '1'=>Html::img($bundle->baseUrl.'/icons/logic/f1_'.($figura_selected=='1'?'black':'white').'.png',['id'=>'fig1', 'class'=>'img',]),
        '2'=>Html::img($bundle->baseUrl.'/icons/logic/f2_'.($figura_selected=='2'?'black':'white').'.png',['id'=>'fig2', 'class'=>'img',]),
        '3'=>Html::img($bundle->baseUrl.'/icons/logic/f3_'.($figura_selected=='3'?'black':'white').'.png',['id'=>'fig3', 'class'=>'img',]),
        '4'=>Html::img($bundle->baseUrl.'/icons/logic/f4_'.($figura_selected=='4'?'black':'white').'.png',['id'=>'fig4', 'class'=>'img',]),
    ],
    [
        'encode'=>FALSE,
        'id'=>'figures_block',
    ]
    ); ?>
<br/>
<br/>
    <p>1 утверждение:</p>
<div class="block">
    <?= $form->field($assertion_1,'[major]suffix')->dropDownList($suffArray,[])->label(false) ?>
    <?= $form->field($assertion_1,'[major]subject')->textInput(['height'=>200])->label(false) ?>
    <?= $form->field($assertion_1,'[major]is')->dropDownList($isArray,[])->label(false) ?>
    <?= $form->field($assertion_1,'[major]predicat')->textInput()->label(false) ?>
</div>
    <p>2 утверждение:</p>
<div class="block">
    <?= $form->field($assertion_2,'[minor]suffix')->dropDownList($suffArray,[])->label(false) ?>
    <?= $form->field($assertion_2,'[minor]subject')->textInput()->label(false) ?>
    <?= $form->field($assertion_2,'[minor]is')->dropDownList($isArray,[])->label(false) ?>
    <?= $form->field($assertion_2,'[minor]predicat')->textInput()->label(false) ?>
</div>
    <br/>
    <br/>
    <br/>


<div class="form-group">
    <?= Html::submitButton('Вывести суждение', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
</div>
</div>


