<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 17.06.2016
 * Time: 8:26
 */

namespace app\assets;


use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle{

    public $basePath = '@webroot/';
    public $baseUrl = '@web/font-awesome';
//    public $sourcePath = '@web';
    public $css = [ 'css/font-awesome.css',];
}