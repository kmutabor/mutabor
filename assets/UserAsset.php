<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 10.02.2016
 * Time: 20:49
 */

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class UserAsset extends AssetBundle
{

    public $sourcePath = '@vendor/myAssets/user/';

    public $css = [
        'css/user_lk.css',
    ];
    public $publishOptions = [
        'only' => [
            '/css/*',
        ],
//        'except' => [
//            'less',
//            'scss',
//        ]
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
