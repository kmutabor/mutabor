<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11.03.2016
 * Time: 18:41
 */

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class LoginAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $css = [
        'css/login/login.css',
    ];
    public $baseUrl = '@web';
    public $js = [
//        'js/login/login.js',
    ];

    public $publishOptions = [
        'only' => [
            'css/login/*',
            'js/login/*',
            'icons/login/*',
        ],
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
} 