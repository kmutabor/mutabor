<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 15.02.2016
 * Time: 20:31
 */

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class LogicAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
//    public $sourcePath = '@web';

    public $css = [
//        'css/user_lk.css',
    ];
    public $js = [
        'js/logic/sillogism.js',
    ];
    public $publishOptions = [
        'only' => [
            'icons/logic/*',
            'icons/logic/u/*',
            'js/logic/*',
        ],
    ];
    public $jsOptions = ['position' => View::POS_END];
}
