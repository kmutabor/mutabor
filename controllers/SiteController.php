<?php

namespace app\controllers;

use app\components\Actions;
use app\components\CacheUtil;
use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'about',
                    'contact',
                    'login',
                    'info',
                    'random',
                    'bug',
                    'onlyRandom',
                    'puzzle',
                    'player',
                    'menschheit',
                    'kidala',
                    'fwgnl',
                ],
                'rules' => [
                    [
                        'actions' => ['contact','login'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'logout', 'contact', 'info', 'about',],
                        'allow' => true,
//                        'roles' => ['@'],
                    ],
//                    [
//                        'actions' => ['migrate-up', 'migrate-down',],
//                        'allow' => true,
//                        'roles' => ['admin'],
//                    ],
                    [
                        'actions' => [
                            'random',
                            'bug',
                            'onlyRandom',
                            'puzzle',
                            'player',
                            'menschheit',
                            'kidala',
                            'fwgnl',
                        ],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        'actions' => ['login',],
                        'allow' => true,
                        'roles' => ['?'],
                    ],

                ],
            ],
//            'basicAuth' => [
//                'class' => HttpBasicAuth::className(),
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionFwgnl()
    {
        return $this->render('fwgnl', []);
    }

    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionMenschheit()
    {
        return CacheUtil::renderCachedData($this,'menschheit', [], 0);
    }

    public function actionKidala()
    {
        return $this->render('kidala', []);
    }

    public function actionOnlyRandom()
    {
        $path = Actions::A_FOLDER . DIRECTORY_SEPARATOR . Actions::getRandomActionName();
        return CacheUtil::renderCachedData($this,$path);
    }

    public function actionBug()
    {
        return $this->redirect(['random', 'id' => Actions::getRandomActionName()]);
    }


//    public function actionMigrateUp()
//    {
//        echo '<pre>';
//        echo 'Выполнение миграции...<br/>';
//        // https://github.com/yiisoft/yii2/issues/1764#issuecomment-42436905
//        $oldApp = \Yii::$app;
//        new \yii\console\Application([
//            'id' => 'Command runner',
//            'basePath' => '@app',
//            'components' => [
//                'db' => $oldApp->db,
//            ],
//        ]);
//        \Yii::$app->runAction('migrate/up', ['migrationPath' => '@app/migrations/', 'interactive' => false]);
//        \Yii::$app = $oldApp;
//        echo '<br/>Говтово!';
//        echo '</pre>';
//    }
//    public function actionMigrateDown()
//    {
//        echo '<pre>';
//        echo 'Откат миграции...<br/>';
//        // https://github.com/yiisoft/yii2/issues/1764#issuecomment-42436905
//        $oldApp = \Yii::$app;
//        new \yii\console\Application([
//            'id' => 'Command runner',
//            'basePath' => '@app',
//            'components' => [
//                'db' => $oldApp->db,
//            ],
//        ]);
//        \Yii::$app->runAction('migrate/down', ['migrationPath' => '@app/migrations/', 'interactive' => false]);
//        \Yii::$app = $oldApp;
//        echo '<br/>Готово!';
//        echo '</pre>';
//    }


    public function actionInfo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        return [
            'message' => 'hello world',
            'code' => 100,
        ];
    }


    public function actionPuzzle()
    {
        return $this->render('puzzle');
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    public function actionAbout()
    {
        return CacheUtil::renderCachedData($this,'about', [], 0);
    }

    public function actionPlayer()
    {
        return $this->render('player');
    }

    public function actionRandom($id){
        $path = Actions::A_FOLDER . DIRECTORY_SEPARATOR . $id;
        return CacheUtil::renderCachedData($this,$path);
    }
}
