<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.02.2016
 * Time: 13:59
 */

namespace app\controllers;


use app\models\Assertion;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

class LogicController extends Controller
{

    private $modusArray = [
        '1' => ['AAA', 'EAE', 'AII', 'EIO',],
        '2' => ['EAE', 'AEE', 'EIO', 'AOO',],
        '3' => ['AAI', 'IAI', 'AII', 'EAO', 'OAO', 'EIO',],
        '4' => ['AAI', 'AEE', 'IAI', 'EAO', 'EIO',],
    ];
    private $suffArray = ['all' => 'Все', 'some' => 'Некоторые', 'none' => 'Ни один',];
    private $isArray = ['is' => 'Суть', 'is_not' => 'Суть не', 'not_is' => 'Не суть', 'not_is_not' => 'Не суть не',];

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','sillogism','uchebnik','uchebnikGlava','spor','sporGlava',],
                'rules' => [
                    [
                        'actions' => ['index','sillogism','uchebnik','uchebnikGlava','spor','sporGlava',],
                        'allow' => true,
                        'roles' => ['@','?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
        ];
    }

    public function actionSillogism()
    {
        $assertion_1 = new Assertion();
        $assertion_2 = new Assertion();
        $post = Yii::$app->request->post();
        $answer = '';
        $figura_selected = '1';
        if (
            $assertion_1->attributes = Yii::$app->request->post('Assertion')['major'] and
            $assertion_2->attributes = Yii::$app->request->post('Assertion')['minor']
        ) {
            $modus = '';
            $suff_1 = $assertion_1->suffix;
            $is_1 = $assertion_1->is;
            $s_1 = $assertion_1->subject;
            $p_1 = $assertion_1->predicat;

            $suff_2 = $assertion_2->suffix;
            $is_2 = $assertion_2->is;
            $s_2 = $assertion_2->subject;
            $p_2 = $assertion_2->predicat;

            $figura_selected = $post['s_type'];

            switch ($figura_selected) {
                case '1':
                    $modus = $this->getModus_1_Figure($suff_1, $is_1, $suff_2, $is_2);
                    $answer = $this->getAnswerByModus_1($modus, $p_1, $s_2);
                    break;
                case '2':
                    $modus = $this->getModus_2_Figure($suff_1, $is_1, $suff_2, $is_2);
                    $answer = $this->getAnswerByModus_2($modus, $s_1, $s_2);
                    break;
                case '3':
                    $modus = $this->getModus_3_Figure($suff_1, $is_1, $suff_2, $is_2);
                    $answer = $this->getAnswerByModus_3($modus, $p_1, $p_2);
                    break;
                case '4':
                    $modus = $this->getModus_4_Figure($suff_1, $is_1, $suff_2, $is_2);
                    $answer = $this->getAnswerByModus_4($modus, $s_1, $p_2);
                    break;
            }
            $session = Yii::$app->session;
            $session->setFlash('major_sil', $this->suffArray[$suff_1] . ' ' . $assertion_1->subject . ' ' . $this->isArray[$is_1] . ' ' . $assertion_1->predicat);
            $session->setFlash('minor_sil', $this->suffArray[$suff_2] . ' ' . $assertion_2->subject . ' ' . $this->isArray[$is_2] . ' ' . $assertion_2->predicat);
            $session->setFlash($answer != '' ? 'success_msg' : 'error_msg',
                $answer != '' ? 'Выбран модус "' . $modus . '" : ' . $answer : 'Для фигуры "' . $figura_selected . '" (' . implode(', ', $this->modusArray[$post['s_type']]) . ') выбран неподходящий модус.');
            if ($modus != '') {
                try {
                    $assertion_1->subject = $assertion_1->predicat = '';
                    $assertion_2->subject = $assertion_2->predicat = '';
                    return $this->render('sillogism', [
                        'assertion_1' => $assertion_1,
                        'assertion_2' => $assertion_2,
                        'suffArray' => $this->suffArray,
                        'isArray' => $this->isArray,
                        'answer' => $answer,
                        'figura_selected' => $figura_selected,
                    ]);
                } catch (Exception $e) {
                    throw $e;
                }

            }

        }
        return $this->render('sillogism', [
            'assertion_1' => $assertion_1,
            'assertion_2' => $assertion_2,
            'suffArray' => $this->suffArray,
            'isArray' => $this->isArray,
            'answer' => $answer,
            'figura_selected' => $figura_selected,

        ]);
    }

    private function getModus_1_Figure($suff_1, $is_1, $suff_2, $is_2 ){
        if($this->A($suff_1, $is_1) and $this->A($suff_2, $is_2)){return 'AAA';}
        if($this->E($suff_1, $is_1) and $this->A($suff_2, $is_2)){return 'EAE';}
        if($this->A($suff_1, $is_1) and $this->I($suff_2, $is_2)){return 'AII';}
        if($this->E($suff_1, $is_1) and $this->I($suff_2, $is_2)){return 'EIO';}
        return '';
    }

    private function A($suff, $is)
    {
        return ($suff == 'all' and ($is == 'is' or $is == 'not_is_not'));
    }

    private function E($suff, $is)
    {
        return ($suff == 'none' and ($is == 'not_is' or $is == 'is_not'));
    }

    private function I($suff, $is)
    {
        return ($suff == 'some' and ($is == 'is' or $is == 'not_is_not'));
    }

    private function getAnswerByModus_1($modus, $p_1, $s_2){
        $result = '';
        switch($modus){
            case 'AAA':
                $result = 'Все из '.$s_2.' суть '.$p_1;
                break;
            case 'EAE':
                $result = 'Ни один  из '.$s_2.' суть не '.$p_1;
                break;
            case 'AII':
                $result = 'Некоторые '.$s_2.' суть '.$p_1;
                break;
            case 'EIO':
                $result = 'Некоторые из '.$s_2.' суть не '.$p_1;
                break;
            default: break;
        }
        return $result;

    }

    private function getModus_2_Figure($suff_1, $is_1, $suff_2, $is_2)
    {
        if ($this->E($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'EAE';
        }
        if ($this->A($suff_1, $is_1) and $this->E($suff_2, $is_2)) {
            return 'AEE';
        }
        if ($this->E($suff_1, $is_1) and $this->I($suff_2, $is_2)) {
            return 'EIO';
        }
        if ($this->A($suff_1, $is_1) and $this->O($suff_2, $is_2)) {
            return 'AOO';
        }
        return '';
    }

    private function O($suff, $is)
    {
        return ($suff == 'some' and ($is == 'not_is' or $is == 'is_not'));
    }

    private function getAnswerByModus_2($modus, $s_1, $s_2 ){
        $result = '';
        switch($modus){
            case 'EAE':
            case 'AEE':
                $result = 'Ни один из '.$s_2.' суть не '.$s_1;
                break;
            case 'EIO':
            case 'AOO':
                $result = 'Некоторые из '.$s_2.' суть не '.$s_1;
                break;
            default: break;
        }
        return $result;
    }

    private function getModus_3_Figure($suff_1, $is_1, $suff_2, $is_2)
    {
        if ($this->A($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'AAI';
        }
        if ($this->I($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'IAI';
        }
        if ($this->A($suff_1, $is_1) and $this->I($suff_2, $is_2)) {
            return 'AII';
        }
        if ($this->E($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'EAO';
        }
        if ($this->O($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'OAO';
        }
        if ($this->E($suff_1, $is_1) and $this->I($suff_2, $is_2)) {
            return 'EIO';
        }
        return '';
    }

    private function getAnswerByModus_3($modus, $p_1, $p_2 ){
        $result = '';
        switch($modus){
            case 'AAI':
            case 'IAI':
            case 'AII':
                $result = 'Некоторые из '.$p_2.' суть '.$p_1;
                break;
            case 'EAO':
            case 'OAO':
            case 'EIO':
                $result = 'Некоторые из '.$p_2.' суть не '.$p_1;
                break;
            default: break;
        }
        return $result;
    }

    private function getModus_4_Figure($suff_1, $is_1, $suff_2, $is_2)
    {
        if ($this->A($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'AAI';
        }
        if ($this->A($suff_1, $is_1) and $this->E($suff_2, $is_2)) {
            return 'AEE';
        }
        if ($this->I($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'IAI';
        }
        if ($this->E($suff_1, $is_1) and $this->A($suff_2, $is_2)) {
            return 'EAO';
        }
        if ($this->E($suff_1, $is_1) and $this->I($suff_2, $is_2)) {
            return 'EIO';
        }
        return '';
    }

    private function getAnswerByModus_4($modus, $s_1,$p_2 ){
        $result = '';
        switch($modus){
            case 'IAI':
            case 'AAI':
                $result = 'Некоторые из '.$p_2.' суть '.$s_1;
                break;
            case 'AEE':
                $result = 'Ни один из '.$p_2.' суть не '.$s_1;
                break;
            case 'EAO':
            case 'EIO':
                $result = 'Некоторые из '.$p_2.' суть не '.$s_1;
                break;
            default: break;
        }
        return $result;
    }

    public function actionUchebnik(){
        return $this->render('uchebnik');
    }

    public function actionUchebnikGlava($id){
        return $this->render('glava/'.$id);
    }

    public function actionSpor(){
        return $this->render('spor');
    }

    public function actionSporGlava($id){
        return $this->render('spor/'.$id);
    }


    public function actionIndex()
    {
//        $session = Yii::$app->session;
//        $session->addFlash('alerts', 'You are promoted.');
        return $this->render('index');
    }


//    private function isPalindrome($str){
//            if(strlen($str)>3 and $str == strrev($str)){
//                return true;
//            }
//            return false;
//        }
//
//    private function getPalindrome($str){
//            if($this->isPalindrome($str)) {
//                return $str;
//            }
//            return false;
//        }

//        function getPalindromesArray($str, $retArray)
//        {
//            $min = 0;
//            $max = strlen($str);
//            while($min<$max){
//                $strF = substr($str, 0, -$min);
//                $palindrome = getPalindrome($strF);
//                if($palindrome!==null) {
//                    $retArray[] = $palindrome;
//                    break;
//                }
//                $min++;
//            }
//            return $retArray;
//        }
//
//        function getMaxLengthPalindrome($array){
//            $nam = '';
//            if($array){
//            $min = strlen($array[0]);
//            $nam = $array[0];
//            for ($i=1; $i < count($array); $i++)
//            {
//                $len = strlen($array[$i]);
//                if ($len > $min)
//                {
//                    $nam = $array[$i];
//                    $min = strlen($nam);
//                }
//            }
//            }
//            return $nam;
//        }

} 