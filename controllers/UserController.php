<?php

namespace app\controllers;

use app\models\search\UserSearch;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

//    public $layout = 'user_lk';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','view','delete','registration','changeUsername','changePassword','confirm',],
                'rules' => [
                    [
                        'actions' => ['index','create','update','view','delete',],
                        'allow' => true,
                        'roles' => ['admin',],
                    ],
                    [
                        'actions' => [
                            'changeUsername',
                            'changePassword',
                        ],
                        'allow' => true,
                        'roles' => ['user','admin'],
                    ],
                    [
                        'actions' => ['registration','confirm'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post',],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionChangePassword(){
        $model = $this->findModel(Yii::$app->user->getId());
        $model_new1 = new User();
        $model_new2 = new User();
        $post = Yii::$app->request->post('User');
        $p = isset($post['p']['password'])?$post['p']['password']:'';
        $p1 = isset($post['p1']['password'])?$post['p1']['password']:'';
        $p2 = isset($post['p2']['password'])?$post['p2']['password']:'';
        $model_new1->password = $p1;
        $model_new2->password = $p2;
        $session = Yii::$app->session;
        if ($p!='' and $p1!='' and $p2!=''){
            if($p1==$p2){
                if(@md5(@md5($p)) == $model->password){
                    $n_pass = @md5(@md5($p1));
                    if($n_pass == $model->password) {
                        $session->setFlash('error_msg', 'Придумайте другой пароль');
                    }else if($n_pass != $model->password){
                        $model->password = $n_pass;
                        $model->save(false);
                        $session->setFlash('success_msg', 'Вы успешно изменили пароль');
                        $model->password = $model_new1->password = $model_new2->password = '';
                        return $this->render('user_password',
                            [
                                'model' => $model,
                                'model_new1' => $model_new1,
                                'model_new2' => $model_new2,
                            ]
                        );
                    }
                }else{
                    $session->setFlash('error_msg', 'Неверно указан старый пароль');
                }
            }else{
                $session->setFlash('error_msg', 'Новые пароли не совпадают');
            }
        }else {
            if(($p!='' and ($p1=='' or $p2=='')) or ($p1!='' and ($p=='' or $p2=='')) or ($p2!='' and ($p=='' or $p1==''))) {
                $session->setFlash('error_msg', 'Необходимо заполнить все поля');
            }
        }
        $model->password = $p;
        return $this->render('user_password', [
            'model' => $model,
            'model_new1' => $model_new1,
            'model_new2' => $model_new2,
        ]);
    }


    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//            return $this->render('view', [
//                'model' => $this->findModel($id),
//            ]);
//    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $userRole = Yii::$app->authManager->getRole($_REQUEST['user_role']);
                Yii::$app->authManager->assign($userRole, $model->getId());
                $transaction->commit();
            }catch (Exception $e){
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionRegistration()
    {
        $post = Yii::$app->request->post();
        $model = new User();
        $model->load($post);
        $model->password = \Yii::$app->security->generateRandomString(6);
        $model->username = $model->email;
        if($model->validate()){
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $model->save(false);
                $userRole = Yii::$app->authManager->getRole('user');
                Yii::$app->authManager->assign($userRole, $model->getId());
                $body =  Html::a('Подтвердить регистрацию',
                    Url::to(
                        ['user/confirm',
                            'a'=>$model->getId(),
                            'b'=>$model->getAuthKey(),
                        ],true
                    )
                );
                Yii::$app->mailer->compose()
                    ->setTo($model->email)
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject("Регистрация")
                    ->setHtmlBody($body)
                    ->send();
                $transaction->commit();
            }catch (Exception $e){
                $transaction->rollBack();
                throw $e;
            }
            return $this->render('registration_success', ['model' => $model]);
        } else {
            return $this->render('registration', [
                'model' => $model,
            ]);
        }
    }

    public function actionConfirm($a, $b){
        $model=$this->findModel($a);
        if($model->getAuthKey() === $b){
            $model->password = \Yii::$app->security->generateRandomString(6);
            $pass = $model->password;
            $model->authKey = \Yii::$app->security->generateRandomString();
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $model->password =@md5(@md5($model->password));
                $model->save(false);
                $body =  'Регистрация прошла успешно. Ваш пароль для входа"'.$pass.'"';
                Yii::$app->mailer->compose()
                    ->setTo($model->email)
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject("Регистрация подтверждена. Пароль для входа.")
                    ->setHtmlBody($body)
                    ->send();
                $transaction->commit();
                return $this->redirect(['site/login']);
            }catch (Exception $e){
                $transaction->rollBack();
                throw $e;
            }
        }
        return $this->render('confirm-to-late');
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeUsername()
    {
        $model = $this->findModel(Yii::$app->user->getId());
        if ($model->load(Yii::$app->request->post()) && $model->save(true,['username'])) {
            return $this->redirect(['/site/index']);
        } else {
            return $this->render('user_name', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->can('admin') or \Yii::$app->user->can('updateSelfUser', ['user_id'=>$id])) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else throw new ForbiddenHttpException('У вас недостаточно прав для выполнения указанного действия');

    }

}
