$('input[name="s_type"]').on(
    'change',
    function(){
        var f1 = $('#fig1');
        f1.attr('src', f1.attr('src').replace('black', 'white'));

        var f2 = $('#fig2');
        f2.attr('src', f2.attr('src').replace('black', 'white'));

        var f3 = $('#fig3');
        f3.attr('src', f3.attr('src').replace('black', 'white'));

        var f4 = $('#fig4');
        f4.attr('src', f4.attr('src').replace('black', 'white'));

        var img = $(this).next('img');
        var imgSrcPath = img.attr('src');
        imgSrcPath = imgSrcPath.replace("white", "black");
        img.attr('src', imgSrcPath);

    }
);