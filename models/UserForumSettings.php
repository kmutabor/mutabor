<?php

namespace app\models;

use app\models\query\UserForumSettingsQuery;

/**
 * This is the model class for table "user_forum_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $UTC
 * @property integer $show_user_sign
 * @property integer $show_user_avatar
 * @property integer $theme_message_paginator
 * @property integer $theme_paginator
 *
 * @property User $user
 */
class UserForumSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_forum_settings';
    }

    /**
     * @inheritdoc
     * @return UserForumSettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserForumSettingsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'UTC', 'show_user_sign', 'show_user_avatar', 'theme_message_paginator', 'theme_paginator'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'UTC' => 'Utc',
            'show_user_sign' => 'Show User Sign',
            'show_user_avatar' => 'Show User Avatar',
            'theme_message_paginator' => 'Theme Message Paginator',
            'theme_paginator' => 'Theme Paginator',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
