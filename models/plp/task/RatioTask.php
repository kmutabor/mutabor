<?php

namespace app\models\plp\task;

use Yii;

/**
 * This is the model class for table "ratio_task".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $created
 * @property string $deffer
 * @property integer $type
 * @property string $task
 * @property string $action
 * @property string $data
 * @property integer $status
 * @property integer $retries
 * @property string $finished
 * @property string $result
 */
class RatioTask extends \yii\db\ActiveRecord
{

    const DEFAULT_STATE = 0;
    const DONE_DATE = 1;
    const FATAL_EXCEPTION_CODE = 2;
    const USER_EXCEPTION_CODE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ratio_task';
    }

    /**
     * @inheritdoc
     * @return RatioTaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RatioTaskQuery(get_called_class());
    }

    static function updateData($params){
        if($params != 'empty' and is_array($params)){
            $response = [];
            try{
                foreach ($params as $key => $item) {
                    $response[$key] = $item;
                }
                $model = RatioTask::findOne($response['id']);
                if($model->retries == 3)
                {
                    Yii::info("задача '$model->id $model->task/$model->action' не выполнена $model->retries раз", 'ratio');
                    throw new UserException("Превышено количество попыток обновления результата статус 3", 0);
                }
                if($model->status == 0)
                {
                    $model->created = date('Y-m-d H-h-s');
                    $model->result = json_encode($response);
                    $model->status = 1;
                }
//                if(something wrong)
//                {
//                    throw new FatalException("Фатальная ошибка. Задача помечена статусом 2 - невозможность выполнения", 1);
//                }
            }catch (UserException $e){
                Yii::info($e->getMessage(),'ratio');
                $model->result = json_encode(['err'=>$e->getMessage()]);
                $model->status = RatioTask::USER_EXCEPTION_CODE;
                return RatioTask::USER_EXCEPTION_CODE;
            }catch (FatalException $e){
                Yii::error($e->getMessage().'\\n'.$e->getTraceAsString(),'ratio');
                $model->status = RatioTask::FATAL_EXCEPTION_CODE;
                return RatioTask::FATAL_EXCEPTION_CODE;
            }finally{
                $model->retries = $model->retries<3?$model->retries+1:$model->retries;
                $model->finished = date('Y-m-d H-h-s');
                $model->save(false);
            }
            Yii::info("Успешно обновлена запись $model->id",'ratio');
            return RatioTask::DEFAULT_STATE;
        }else{
            throw new FatalException("Не удалось обработать входящие параметры", 1);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'type', 'status', 'retries'], 'integer'],
            [['created', 'deffer', 'finished'], 'safe'],
            [['data', 'result'], 'string'],
            [['task', 'action'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'created' => 'Created',
            'deffer' => 'Deffer',
            'type' => 'Type',
            'task' => 'Task',
            'action' => 'Action',
            'data' => 'Data',
            'status' => 'Status',
            'retries' => 'Retries',
            'finished' => 'Finished',
            'result' => 'Result',
        ];
    }
}
