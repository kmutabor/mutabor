<?php

namespace app\models\plp\task;

/**
 * This is the ActiveQuery class for [[RatioTask]].
 *
 * @see RatioTask
 */
class RatioTaskQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RatioTask[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RatioTask|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}