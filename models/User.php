<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
* This is the model class for table "user".
 *
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 *
 * @property UserEmailSettings[] $userEmailSettings
 * @property UserForumSettings[] $userForumSettings
 * @property UserInfo[] $userInfos
 *
*/

class User extends ActiveRecord implements IdentityInterface
{

    public $verifyCode;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['username' => $username]);
    }

//    public function getAnswers0()
//    {
//        return $this->hasMany(Answer::className(), ['answer_to' => 'id']);
//    }

    public static function findByEmail($email)
    {
        return User::findOne(['email' => $email]);
    }

    public static function findByUsernameOrEmail($data)
    {
        return User::find()->where(['email' => $data])->orWhere(['username' => $data])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['username', 'password', 'email', 'authKey', 'accessToken'], 'required'],
            [['username', 'password', 'email',], 'required'],
            [['username', 'email',], 'unique'],
            [['username', 'email'], 'string', 'max' => 50],
            [['password', 'authKey', 'accessToken'], 'string', 'max' => 255],
            ['verifyCode', 'captcha'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'verifyCode' => 'Verification Code',
        ];
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['user_id' => 'id']);
    }

    public function getThemes()
    {
        return $this->hasMany(Theme::className(), ['user_id' => 'id']);
    }

    public function getUserEmailSettings()
    {
        return $this->hasOne(UserEmailSettings::className(), ['user_id' => 'id']);
    }

    public function getUserForumSettings()
    {
        return $this->hasOne(UserForumSettings::className(), ['user_id' => 'id']);
    }

    public function getUserInfos()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5(md5($password));
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->password = md5(md5($this->password));
                $this->authKey = \Yii::$app->security->generateRandomString();
                $this->accessToken = md5(md5($this->getId().$this->username.$this->password.$this->email));
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

}
