<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for table "assertion".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $subject
 * @property string $predicat
 * @property string $suffix
 * @property string $is
 */
//class Assertion extends \yii\db\ActiveRecord
class Assertion extends Model
{

    public $suffix;
    public $subject;
    public $predicat;
    public $is;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assertion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'subject', 'predicat', 'suffix', 'is',], 'required'],
            [['user_id'], 'integer'],
            [['suffix', 'is'], 'string'],
            [['subject', 'predicat'], 'string', 'max' => 255],
            [['suffix',], 'match', 'pattern' => '/(all|some|none)/', 'message'=>'Подставлять свои значения запрещено'],
            [['is',], 'match', 'pattern' => '/(is|not_is|is_not|not_is_not)/', 'message'=>'Подставлять свои значения запрещено'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'subject' => 'Subject',
            'predicat' => 'Predicat',
            'suffix' => 'Suffix',
            'is' => 'Is',
        ];
    }

}
