<?php

namespace app\models;

use app\models\query\UserEmailSettingsQuery;

/**
 * This is the model class for table "user_email_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $hide_email
 * @property integer $admin_newsletter
 * @property integer $add_message_copy_to_answer
 * @property integer $pm_alert
 * @property integer $get_new_and_updated_themes
 *
 * @property User $user
 */
class UserEmailSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_email_settings';
    }

    /**
     * @inheritdoc
     * @return UserEmailSettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserEmailSettingsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'hide_email', 'admin_newsletter', 'add_message_copy_to_answer', 'pm_alert', 'get_new_and_updated_themes'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'hide_email' => 'Hide Email',
            'admin_newsletter' => 'Admin Newsletter',
            'add_message_copy_to_answer' => 'Add Message Copy To Answer',
            'pm_alert' => 'Pm Alert',
            'get_new_and_updated_themes' => 'Get New And Updated Themes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }



}
