<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[UserForumSettings]].
 *
 * @see UserForumSettings
 */
class UserForumSettingsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserForumSettings[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserForumSettings|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byUser()
    {
        return $this->andWhere(['user_id' => \Yii::$app->user->getId()]);
    }


}