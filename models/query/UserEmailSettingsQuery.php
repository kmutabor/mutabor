<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[UserEmailSettings]].
 *
 * @see UserEmailSettings
 */
class UserEmailSettingsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserEmailSettings[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserEmailSettings|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byUser()
    {
        return $this->andWhere(['user_id' => \Yii::$app->user->getId()]);
    }

}