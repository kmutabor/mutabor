<?php

namespace app\models;

use app\models\query\UserInfoQuery;

/**
 * This is the model class for table "user_info".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $mid_name
 * @property string $sur_name
 * @property string $birth_day
 * @property string $phone
 * @property string $advanced_email
 * @property string $sex
 * @property string $web
 * @property string $ICQ
 * @property string $city
 * @property string $hobby
 * @property string $about_us
 * @property string $subscription
 * @property string $avatar
 *
 * @property User $user
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * @inheritdoc
     * @return UserInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserInfoQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['birth_day'], 'safe'],
            [['name', 'mid_name', 'sur_name', 'city'], 'string', 'max' => 50],
            [['phone', 'about_us'], 'string', 'max' => 30],
            [['advanced_email', 'web'], 'string', 'max' => 100],
            [['sex'], 'string', 'max' => 1],
            [['ICQ'], 'string', 'max' => 10],
            [['hobby', 'subscription'], 'string', 'max' => 300],
            [['avatar'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'mid_name' => 'Mid Name',
            'sur_name' => 'Sur Name',
            'birth_day' => 'Birth Day',
            'phone' => 'Phone',
            'advanced_email' => 'Advanced Email',
            'sex' => 'Sex',
            'web' => 'Web',
            'ICQ' => 'Icq',
            'city' => 'City',
            'hobby' => 'Hobby',
            'about_us' => 'About Us',
            'subscription' => 'Subscription',
            'avatar' => 'Avatar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
