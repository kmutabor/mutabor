<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 17.09.2016
 * Time: 2:56
 */

namespace app\components;


use Yii;

class Actions {

    const A_FOLDER = 'a';

    public static $actionNames = [
        'bred', 'eto_ne_ya', 'fruhling', 'harakiri', 'june',
        'alone', 'lobotomiya', 'luchshe_v_dermo', 'mi', 'moya_oborona_orig',
        'neponyatnaya_pesenka', 'nezhnost', 'novaya_patrioticheskaya', 'optimism', 'plyazh',
        'poshli_vi_vse_na_huy', 'prevoshodnaya_pesnya', 'pulya_dura', 'pulya_vinovatogo_naydet', 'resheniya',
        'tak_daleko', 'vintovka_eto_prazdnik_orig', 'wenig', 'ya_illuzoren', 'dezertir',
        'mimikriya','poezd_ushel_remix','pops','prazdnik_konchilsya','priyatnogo_appetita',
        'russkoe_pole_eksperimentov','sem_shagov_za_gorizont_1','sem_shagov_za_gorizont_2', 'razdrazhenie','sredi_zarazhennogo_logikoy_mira',
        'entropiya','chelovek_cheloveku_volk','evangelie','iuda_budet_v_rayu','kto_sdohnet_pervim',
        'mi_led_pod_nogami_mayora','paradox','polkorolevstva','povezlo','sledi_na_snegu',
        'sodatami_ne_rozhdayutsa','totalitarizm','vershki_i_koreshki','vpered', 'zdorovo_i_vechno',
        'noviy_tridzad_sedmoy','ivan_govnov','krasniy_smeh','nikto_ne_hotel_umirat','prodolzhaya_prodolzhat',
        'skoro_nastanet_sovsem','slepite_mne_masku','v_stenu_golovoy','vechnaya_vesna','vse_kak_u_lyudej',
        'ya_sterveneyu','zaplata_na_zaplate','zveri_dlya_tebya_fingerstyle','beregis','beri_shinel',
        'dekoracii','bez_menya','gori_gori_yasno','gosudarstvo','kak_smetana',
        'na_cherniy_den','kak_listovka','obrashenie_k_tebe','pro_durochka','on_uvidel_solnce',
        'samootvod','ot_bolshogo_uma', 'stoletniy_dozhd', 'vrag','uzhas_i_moralniy_terror',
        'vse_kak_u_lyudey_fingerstyle','latin_aphorisms','poebat','tramvay','privikat',
        'pro_chervyachkov','zoopark','skvoz_diru_v_moey_golove','protiv','peremena_pogodi',
        'pro_durochka_fingerstyle','slepoe_belmo','snaruzhi_vseh_izmereniy','ya_hochu_umeret_molodim','sobaki',
        'zerno_na_melnicu','tuman','zrya_vi_eto_vse','zdravstvuy_cherniy_ponedelnik','tak_zakalyalas_stal',
        's_novim_godom','solncevorot','prostor_otkrit','ya_zhivoy','ofeliya',
        'nekrofiliya','nedobitie_tela','navazhdenie','na_nashih_glazah','moya_oborona_fingerstyle',
        'mishelovka','lubvi_ne_minovat','les','kto_silnee_tot_i_prav','konchilis_patroni',
        'kogo_to_eshe','kazhdomu_svoe','kakoe_mne_delo','kaif_ili_bolshe','igra_v_biser',
        'i_snova_temno','horosho','glina_nauchit','dolgaya_schastlivaya_chizn_fingerstyle','dolgaya_schastlivaya_chizn',
        'ck','chuzherodnim_elementom','daleko_bezhit_doroga','svoboda','v_leninskih_gorah',
        'vremya_kolokolchikov', 'caplya', 'delfin', 'dver', 'hudozhnik',
        'imya','kogda_ti_verneshsya','kran','krilya','lasty',
        'lubov','mishini_delfini','moloko','ne_ubivay_na_bumage','okno',
        'olga','ona','poslednee_slovo','poy_mne_eshe','prozrachnaya',
        'radiovolna','rezba','romans','shtempel','sinaya_lirika_1',
        'sinaya_lirika_2','tebya','telefon','transpechal','ubiyca',

        'v_posledniy_raz','velvetovaya_pesnya','vera','voyna','ya_budu_zhit',
        'ya_lublu_ludey','memento_mori','sobaka'

//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',
//        '','','','','',

    ];

    public static $randomLinkTitles = [
        '&DoubleLongLeftRightArrow;',
        '&apid;',
        '&bernou;',
        '&oplus;',
        '&exist;',
        '&#169;',
        '&#9800;',
        '&#9801;',
        '&#9802;',
        '&#9803;',
        '&#9804;',
        '&#9805;',
        '&#9806;',
        '&#9807;',
        '&#9808;',
        '&#9809;',
        '&#9810;',
        '&#9811;',
        '&#169;',
        '&#174;',
        '&#8482;',
        '&#186;',
        '&#170;',
        '&#8240;',
        '&#960;',
        '&#166;',
        '&#167;',
        '&#176;',
        '&#181;',
        '&#182;',
        '&#8230;',
        '&#8254;',
        '&#8470;',
        '&#128269;',
        '&#128270;',
        '&#215;',
        '&#247;',
        '&#60;',
        '&#62;',
        '&#177;',
        '&#185;',
        '&#178;',
        '&#179;',
        '&#172;',
        '&#188;',
        '&#189;',
        '&#190;',
        '&#8260;',
        '&#8722;',
        '&#8804;',
        '&#8805;',
        '&#8776;',
        '&#8800;',
        '&#8801;',
        '&#8730;',
        '&#8734;',
        '&#8721;',
        '&#8719;',
        '&#8706;',
        '&#8747;',
        '&#8704;',
        '&#8707;',
        '&#8709;',
        '&#216;',
        '&#8712;',
        '&#8713;',
        '&#8727;',
        '&#8834;',
        '&#8835;',
        '&#8836;',
        '&#8838;',
        '&#8839;',
        '&#8853;',
        '&#8855;',
        '&#8869;',
        '&#8736;',
        '&#8743;',
        '&#8744;',
        '&#8745;',
        '&#8746;',
        '&#8364;',
        '&#162;',
        '&#163;',
        '&#164;',
        '&#165;',
        '&#402;',
        '&#8226;',
        '&#9675;',
        '&#8224;',
        '&#8225;',
        '&#9824;',
        '&#9827;',
        '&#9829;',
        '&#9830;',
        '&#9674;',
        '&#9999;',
        '&#9998;',
        '&#10000;',
        '&#9997;',
        '&#8592;',
        '&#8593;',
        '&#8594;',
        '&#8595;',
        '&#8596;',
        '&#8597;',
        '&#8629;',
        '&#8656;',
        '&#8657;',
        '&#8658;',
        '&#8659;',
        '&#8660;',
        '&#8661;',
        '&#9650;',
        '&#9660;',
        '&#9658;',
        '&#9668;',
        '&#9731;',
        '&#10052;',
        '&#10053;',
        '&#10054;',
        '&#9733;',
        '&#9734;',
        '&#10026;',
        '&#10027;',
        '&#10031;',
        '&#9885;',
        '&#9898;',
        '&#9899;',
        '&#9913;',
        '&#10037;',
        '&#10057;',
        '&#10059;',
        '&#10042;',
        '&#10041;',
        '&#10040;',
        '&#10038;',
        '&#10039;',
        '&#10036;',
        '&#10035;',
        '&#10034;',
        '&#10033;',
        '&#10023;',
        '&#10022;',
        '&#9055;',
        '&#8859;',
        '&#9200;',
        '&#8986;',
        '&#8987;',
        '&#9203;',
        '&alpha;',
        '&Alpha;',
        '&beta;',
        '&Beta;',
        '&gamma;',
        '&Gamma;',
        '&delta;',
        '&Delta;',
        '&epsilon;',
        '&Epsilon;',
        '&zeta;',
        '&Zeta;',
        '&eta;',
        '&Eta;',
        '&theta;',
        '&Theta;',
        '&iota;',
        '&Iota;',
        '&kappa;',
        '&Kappa;',
        '&lambda;',
        '&Lambda;',
        '&mu;',
        '&Mu;',
        '&nu;',
        '&Nu;',
        '&xi;',
        '&Xi;',
        '&omicron;',
        '&Omicron;',
        '&pi;',
        '&Pi;',
        '&rho;',
        '&Rho;',
        '&sigma;',
        '&Sigma;',
        '&sigmaf;',
        '&tau;',
        '&Tau;',
        '&upsilon;',
        '&Upsilon;',
        '&phi;',
        '&Phi;',
        '&chi;',
        '&Chi;',
        '&psi;',
        '&Psi;',
        '&omega;',
        '&Omega;',
        '&#9999;',	// ✏
        '&#9998;',	// ✎
        '&#10000;',	// ✐
        '&#10001;',	// ✑
        '&#10002;',	// ✒
        '&#10003;', //	✓
        '&#10004;', //	✔
        '&#10006;', //	✖
        '&#10007;', //	✗
        '&#10008;', //	✘
        '&#9747;', //	☓
        '&#9675;', //	○
        '&#9679;', //	●
        '&#9633;', //	□

        '&#10017;', //	✡
        '&#10026;', //	✪
        '&#10027;', //	✫
        '&#10028;',
        '&#10028',
        '&#10031',
        '&#10031',
        '&#10032;', //	✰
        '&#9733;', //	★
        '&#9734;', //	☆
        '&#9728;', //	☀
        '&#9788;', //	☼
        '&#9729;', //	☁
        '&#10052;', //	❄
        '&#10053;', //	❅
        '&#10054;', //	❆
        '&#9730;', //	☂
        '&#9731;', //	☃
        '&#10047;', //	✿
        '&#10045;', //	✽
        '&#10046;', //	✾
        '&#10048;', //	❀
        '&#9789;', //	☽
        '&#9790;', //	☾
        '&#9742;', //	☎
        '&#9743;', //	☏
        '&#9990;', //	✆
        '&#10041;', //	✹
        '&#10040;', //	✸
        '&#10039;', //	✷
        '&#10038;', //	✶
        '&#9744;', //	☐
        '&#9745;', //	☑
        '&#9746;', //	☒
        '&#9760;', //	☠
        '&#9770;', //	☪
        '&#9773;', //	☭
        '&#9993;', //	✉
        '&#9775;', //	☯
        '&#9997;', //	✍
        '&#9785;', //	☹
        '&#9786;', //	☺
        '&#9787;', //	☻
        '&#9988;', //	✄
        '&#9986;', //	✂
        '&#9792;', //	♀
        '&#9794;', //	♂
        '&#10085;', //	❥
        '&#10086;', //	❦
        '&#9833;', //	♩
        '&#9834;', //	♪
        '&#9835;', //	♫
        '&#9836;', //	♬
        '&#9837;', //	♭
        '&#9838;', //	♮
        '&#9839;', //	♯
        '&#10061;', //	❍
        '&#10066;', //	❒


        '¯\_(ツ)_/¯',
        '( ⚆ _ ⚆ )',
        '(▰˘◡˘▰)',
        '(✿´‿`)',
        '≧☉_☉≦',
        'ᕦ(ò_óˇ)ᕤ',
        '~(˘▾˘~)',
        '(;´༎ຶД༎ຶ`)',
        'ಠ╭╮ಠ',

        '( .-. )',
        '( .o.)',
        '( `·´ )',
        '( ° ͜ ʖ °)',
        '( ͡° ͜ʖ ͡°)',
        '( ⚆ _ ⚆ )',
        '( ︶︿︶)',
        '( ﾟヮﾟ)',
        '(\\/)(°,,,°)(\\/)',
        '(¬_¬)',
        '(¬º-°)¬',
        '(¬‿¬)',
        '(°ロ°)☝',
        '(´・ω・)っ',
        '(ó ì_í)',
        '(ʘ‿ʘ)',
        '(̿▀̿ ̿Ĺ̯̿̿▀̿ ̿)̄',
        '(͡° ͜ʖ ͡°)',
        '(ಠ_ಠ)',
        '(ಠ‿ಠ)',
        '(ಠ⌣ಠ)',
        '(ಥ_ಥ)',
        '(ಥ﹏ಥ)',
        '(ง ͠° ͟ل͜ ͡°)ง',
        '(ง ͡ʘ ͜ʖ ͡ʘ)ง',
        '(ง •̀_•́)ง',
        '(ง°ل͜°)ง',
        '(ง⌐□ل͜□)ง',
        '(ღ˘⌣˘ღ)',
        '(ᵔᴥᵔ)',
        '(•ω•)',
        '(•◡•)/',
        '(⊙ω⊙)',
        '(⌐■_■)',
        '(─‿‿─)',
        '(╯°□°）╯',
        '(◕‿◕)',
        '(☞ﾟ∀ﾟ)☞',
        '(❍ᴥ❍ʋ)',
        '(っ◕‿◕)っ',
        '(づ｡◕‿‿◕｡)づ',
        '(ノಠ益ಠ)ノ',
        '(ノ・∀・)ノ',
        '(；一_一)',
        '(｀◔ ω ◔´)',
        '(｡◕‿‿◕｡)',
        '(ﾉ◕ヮ◕)ﾉ',
        '*<{:¬{D}}}',
        '=^.^=',
        't(-.-t)',
        '| (• ◡•)|',
        '~(˘▾˘~)',
        '¬_¬',
        '¯(°_o)/¯',
        '°Д°',
        'ɳ༼ຈل͜ຈ༽ɲ',
        'ʅʕ•ᴥ•ʔʃ',
        'ʕ´•ᴥ•`ʔ',
        'ʕ•ᴥ•ʔ',
        'ʕ◉.◉ʔ',
        'ʕㅇ호ㅇʔ',
        'ʕ；•`ᴥ•´ʔ',
        'ʘ‿ʘ',
        '͡° ͜ʖ ͡°',
        'ζ༼Ɵ͆ل͜Ɵ͆༽ᶘ',
        'Ѱζ༼ᴼل͜ᴼ༽ᶘѰ',
        'ب_ب',
        '٩◔̯◔۶',
        'ಠ_ಠ',
        'ಠoಠ',
        'ಠ~ಠ',
        'ಠ‿ಠ',
        'ಠ⌣ಠ',
        'ಠ╭╮ಠ',
        'ರ_ರ',
        'ง ͠° ل͜ °)ง',
        '๏̯͡๏﴿',
        '༼ ºººººل͟ººººº ༽',
        '༼ ºل͟º ༽',
        '༼ ºل͟º༼',
        '༼ ºل͟º༽',
        '༼ ͡■ل͜ ͡■༽',
        '༼ つ ◕_◕ ༽つ',
        '༼ʘ̚ل͜ʘ̚༽',
        'ლ(´ڡ`ლ)',
        'ლ(́◉◞౪◟◉‵ლ)',
        'ლ(ಠ益ಠლ)',
        'ᄽὁȍ ̪őὀᄿ',
        'ᔑ•ﺪ͟͠•ᔐ',
        'ᕕ( ᐛ )ᕗ',
        'ᕙ(⇀‸↼‶)ᕗ',
        'ᕙ༼ຈل͜ຈ༽ᕗ',
        'ᶘ ᵒᴥᵒᶅ',
        '‎‎(ﾉಥ益ಥ）ﾉ',
        '≧☉_☉≦',
        '⊙▃⊙',
        '⊙﹏⊙',
        '┌( ಠ_ಠ)┘',
        '╚(ಠ_ಠ)=┐',
        '◉_◉',
        '◔ ⌣ ◔',
        '◔̯◔',
        '◕‿↼',
        '◕‿◕',
        '☉_☉',
        '☜(⌒▽⌒)☞',
        '☼.☼',
        '♥‿♥',
        '⚆ _ ⚆',
        '✌(-‿-)✌',
        '〆(・∀・＠)',
        'ノ( º _ ºノ)',
        'ノ( ゜-゜ノ)',
        'ヽ( ͝° ͜ʖ͡°)ﾉ',
        'ヽ(`Д´)ﾉ',
        'ヽ༼° ͟ل͜ ͡°༽ﾉ',
        'ヽ༼ʘ̚ل͜ʘ̚༽ﾉ',
        'ヽ༼ຈل͜ຈ༽ง',
        'ヽ༼ຈل͜ຈ༽ﾉ',
        'ヽ༼Ὸل͜ຈ༽ﾉ',
        'ヾ(⌐■_■)ノ',
        '꒰･◡･๑꒱',
        '﴾͡๏̯͡๏﴿',
        '｡◕‿◕｡',
        'ʕノ◔ϖ◔ʔノ',
        'ಠ_ರೃ',
        '(ꈨຶꎁꈨຶ)۶”',
        '(ꐦ°᷄д°᷅)',
        '(۶ૈ ۜ ᵒ̌▱๋ᵒ̌ )۶ૈ=͟͟͞͞ ⌨',
        '₍˄·͈༝·͈˄₎◞ ̑̑ෆ⃛',
        '(*ﾟ⚙͠ ∀ ⚙͠)ﾉ❣',
        '٩꒰･ัε･ั ꒱۶',
        'ヘ（。□°）ヘ',
        '˓˓(ृ　 ु ॑꒳’)ु(ृ’꒳ ॑ ृ　)ु˒˒˒',
        '꒰✘Д✘◍꒱',
        '૮( ᵒ̌ૢཪᵒ̌ૢ )ა',
        '“ψ(｀∇´)ψ',
        'ಠﭛಠ',
        '(๑>ᴗ<๑)',
        '(۶ꈨຶꎁꈨຶ )۶ʸᵉᵃʰᵎ',
        '٩(•̤̀ᵕ•̤́๑)ᵒᵏᵎᵎᵎᵎ',
        '(oT-T)尸',
        '(✌ﾟ∀ﾟ)☞',
        'ಥ‿ಥ',
        'ॱ॰⋆(˶ॢ‾᷄﹃‾᷅˵ॢ)',
        '┬┴┬┴┤  (ಠ├┬┴┬┴',
        '( ˘ ³˘)♥',
        'Σ (੭ु ຶਊ ຶ)੭ु⁾⁾',
        '(⑅ ॣ•͈ᴗ•͈ ॣ)',
        'ヾ(´￢｀)ﾉ',
        '(•̀o•́)ง',
        '(๑•॒̀ ູ॒•́๑)',
        '⚈้̤͡ ˌ̫̮ ⚈้̤͡',
        '=͟͟͞͞ =͟͟͞͞ ﾍ( ´Д`)ﾉ',
        '(((╹д╹;)))',
        '•̀.̫•́✧',
        '(ᵒ̤̑ ₀̑ ᵒ̤̑)',


    ];

    public static $suffixes = [
        '☣','☻','☀','☢','☭','☮','☯',
    ];

    public static function getRandomSuffix(){
        return '.'.self::$suffixes[rand(0,count(self::$suffixes)-1)];
    }

    public static function getRandomLinkTitle(){
        return self::$randomLinkTitles[rand(0,count(self::$randomLinkTitles)-1)];
    }

    public static function getRandomActionName(){
        return self::$actionNames[rand(0,count(self::$actionNames)-1)];
    }

    public static function getHash(){
        return md5(Yii::$app->security->generateRandomString(5));
    }



} 