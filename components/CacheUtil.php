<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 10.12.16
 * Time: 1:11
 */

namespace app\components;


use Yii;

class CacheUtil
{
    /**
     * @param $cacheKey
     * @return mixed
     */
    public static function getCachedData($cacheKey){
        return Yii::$app->cache->get($cacheKey);
    }

    /**
     * @param $cacheKey
     * @param $data
     * @param int $expireTime
     */
    public static function setCachedData($cacheKey, $data, $expireTime = 60*60){
        Yii::$app->cache->add($cacheKey, $data, $expireTime);
    }

    /**
     * @param $controller \yii\web\Controller
     * @param $pathToView
     * @param array $params
     * @param int $expireTime
     * @return mixed|null
     */
    public static function renderCachedData($controller, $pathToView, $params = [], $expireTime = 60*60){
        $key = md5($pathToView);
        if($cachedData = CacheUtil::getCachedData($key)){
            return $cachedData;
        }
        $cachedData = $controller->render($pathToView,$params);
        CacheUtil::setCachedData($key, $cachedData, $expireTime);
        return CacheUtil::getCachedData($key);
    }

}