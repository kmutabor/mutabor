<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.06.2016
 * Time: 6:48
 */

namespace app\commands\plp\task;

use app\models\plp\task\RatioTask;
use yii\console\Controller;

class DomainController  extends Controller{


    public static function actionAddzone($params = 'empty'){
        $response=RatioTask::updateData($params);
        return $response;
    }

} 