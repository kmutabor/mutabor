<?php
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.06.2016
 * Time: 6:20
 */
namespace app\commands\plp\task;

use app\models\plp\task\RatioTask;
use Yii;
use yii\console\Controller;

class RatioController extends Controller{


    public function actionIndex()
    {
        $this->stdout("Run infinite loop. Start time".date('d-m-Y H:h:s')."\n") ;
        $this->stdout("Press CTRL + C for interrupt.\n\n") ;
        while (true){
            Yii::trace(date('d-m-Y H:h:s')."\n", 'ratio') ;
            $work = RatioTask::find()->where(['status'=>
                [
                    RatioTask::DEFAULT_STATE,
                ]
            ])->all();
            if($work){
                $this->stdout("-------------------------BEGIN-------------------------\n") ;
                foreach($work as $value){
                    $params['data'] = json_decode($value->data);
                    $params['id'] = $value->id;
                    $params['status'] = $value->status;
                    $response = \Yii::$app->runAction("plp/task/$value->task/$value->action", [$params,'interactive' => false]);
                    $this->stdout(date('d-m-Y H:h:s')."\t $value->id \t$value->task \t$value->action");
                    switch($response){
                        case 0:
                            $this->stderr("\t ok\n");
                            break ;
                        case 1:
                            $this->stderr("\t error\n");
                            break ;
                        case RatioTask::FATAL_EXCEPTION_CODE:
                            $this->stderr("\t fatal exception\n");
                            break ;
                        case RatioTask::USER_EXCEPTION_CODE:
                            $this->stderr("\t user exception\n");
                            break ;
                        default:
                            $this->stdout("\t undefined error.\n");
                            break ;
                    }
                    sleep(1);
                }
                $this->stdout("--------------------------END--------------------------\n") ;
            }
            Yii::$app->db->close();
            sleep(15);
        }
    }



} 