<?php

namespace app\modules\nsign;

class Nsign extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\nsign\controllers';


    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
