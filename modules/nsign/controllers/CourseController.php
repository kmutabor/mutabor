<?php

namespace app\modules\nsign\controllers;

use app\modules\nsign\models\NsignComponent;
use app\modules\nsign\models\NsignCourse;
use app\modules\nsign\models\NsignCourseSearch;
use app\modules\nsign\models\NsignRecipe;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CourseController implements the CRUD actions for NsignCourse model.
 */
class CourseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','view','delete','filt','filt-do'],
                'rules' => [
                    [
                        'actions' => ['index','create','update','view','delete',],
                        'allow' => true,
                        'roles' => ['admin',],
                    ],
                    [
                        'actions' => ['index','create', 'view', 'filt', 'filt-do'],
                        'allow' => true,
                        'roles' => ['@',],
                    ],
                ],
            ],
//            'basicAuth' => [
//                'class' => HttpBasicAuth::className(),
//            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Lists all NsignCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NsignCourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider = new ActiveDataProvider([
//            'query' => NsignCourse::find(),
//        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single NsignCourse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the NsignCourse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NsignCourse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NsignCourse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new NsignCourse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NsignCourse();
        $modelRecipe = new NsignRecipe();
        $components = NsignComponent::find()->all();
        if ($model->load(Yii::$app->request->post()) and $modelRecipe->load(Yii::$app->request->post())) {
            $componentsPost = Yii::$app->request->post('NsignRecipe')['component_id'];
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $model->save();
                foreach($componentsPost as $element){
                    $nmodel = new NsignRecipe();
                    $nmodel->course_id = $model->id;
                    $nmodel->component_id = $element;
                    $nmodel->save(false);
                }
                $transaction->commit();
            }catch(Exception $e){
                $transaction->rollBack();
                throw $e;
            }
           return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'components'=>$components,
                'modelRecipe'=>$modelRecipe,
            ]);
        }

    }

//    public function actionFiltDo($data){
//
//    }

    /**
     * Updates an existing NsignCourse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $components = NsignComponent::find()->all();

        if ($model->load(Yii::$app->request->post()) and Yii::$app->request->post('NsignRecipe')) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $model->save();
                NsignRecipe::deleteAll(['course_id'=>$model->id]);
                $componentsPost = Yii::$app->request->post('NsignRecipe')['component_id'];
                foreach($componentsPost as $element){
                    $nmodel = new NsignRecipe();
                    $nmodel->course_id = $model->id;
                    $nmodel->component_id = $element;
                    $nmodel->save(false);
                }
                $transaction->commit();
            }
            catch(Exception $e){
                $transaction->rollBack();
                throw $e;
            }
            return $this->redirect(['update', 'id' => $id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'components'=>$components,
                'recipeModel'=>new NsignRecipe(),
            ]);
        }
    }

    public function actionFilt(){
        $session = Yii::$app->session;
        $selectedItems = Yii::$app->request->post('NsignComponent')['id'];
        $itemsCount = !empty($selectedItems)?count($selectedItems):0;
        if($itemsCount > 5){
            $session->setFlash('type', 'danger');
            $session->setFlash('msg', 'Можно выбрать не более 5 ингридиентов для поиска!');
            $result = 'Много данных для поиска';
            return $this->render('_filter_form',
                [
                    'selectedItems'=>$selectedItems,
                    'result'=>$result,
                ]
            );
        }else if($itemsCount > 0 and $itemsCount < 2){
            $session->setFlash('type', 'danger');
            $session->setFlash('msg', 'Выберите больше ингредиентов');
            $result = 'Мало данных для поиска';
            return $this->render('_filter_form',
                [
                    'selectedItems'=>$selectedItems,
                    'result'=>$result,
                ]
            );
        }else{
            $result = $this->renderPartial('filter_result_table',
                [
                    'courses'=>NsignCourse::find()->byNsingComponents($selectedItems),
                ]
            );
        }

        return $this->render('_filter_form',
            [
                'selectedItems'=>$selectedItems,
                'result'=>$result,
            ]
        );
    }

    /**
     * Deletes an existing NsignCourse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        try{
            NsignRecipe::deleteAll(['course_id'=>$model->id]);
            $model->delete();
            $transaction->commit();
        }
        catch(Exception $e){
            $transaction->rollBack();
            throw $e;
        }
        return $this->redirect(['index']);
    }
}
