<?php

use yii\helpers\Html;

//$selectedItems;
//var_dump($selectedItems);
$session = Yii::$app->session;
function sortArrayByElementCount($f1,$f2)
{
    if(count($f1[array_keys($f1)[0]]) < count($f2[array_keys($f2)[0]])){
        return 1;
    }
    else if(count($f1[array_keys($f1)[0]]) > count($f2[array_keys($f2)[0]])) {
        return -1;
    }
    else {
        return 0;
    }
}
?>

<div class="nsign-component-index">
        <h2><?= Html::encode($this->title) ?></h2>
        <?php if($courses['all']){
            $session->setFlash('type', 'success');
            $session->setFlash('msg', 'Найдено '.count($courses['all']).' блюд с полным совпадением ингредиентов');
            ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Название блюда</th>
                <th>Ингридиенты</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($courses['all'] as $value){
                foreach($value as $name => $course){?>
                    <tr>
                        <td>
                            <?= $name;?>
                        </td>
                        <td>
                            <ul>
                                <?php
                                foreach($course as $recipe){
                                    echo '<li>';
                                    echo $recipe;
                                    echo '</li>';
                                }
                                ?>
                            </ul>
                        </td>
                    </tr>
                <?php }}?>

            </tbody>
        </table>
        <?php }else if ($courses['part']){
            $session->setFlash('type', 'success');
            $session->setFlash('msg', 'Найдено '.count($courses['part']).' блюд с частичным совпадением ингредиентов');
            usort($courses['part'], 'sortArrayByElementCount');
            ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Название блюда</th>
                <th>Ингридиенты</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($courses['part'] as $value){
                    foreach($value as $name => $course){?>
                <tr>
                    <td>
                        <?= $name;?>
                    </td>
                    <td>
                        <ul>
                            <?php
                            foreach($course as $recipe){
                                echo '<li>';
                                echo $recipe;
                                echo '</li>';
                            }
                            ?>
                        </ul>
                    </td>
                </tr>
            <?php }}?>

            </tbody>
        </table>

        <?php }else if ($courses['min']){
                $session->setFlash('type', 'danger');
                $session->setFlash('msg', 'Не найдено блюд по заданным ингредиентам');
            }?>
    </div>

