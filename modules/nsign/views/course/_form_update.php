<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\NsignCourse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nsign-course-form">

    <?php
    $recipeModel->course_id = $model->id;
    $recipeModel->component_id = ArrayHelper::getColumn($model->nsignRecipes, 'component_id');
    $form = ActiveForm::begin();
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($recipeModel, 'component_id')->checkboxList(ArrayHelper::map($components, 'id','name'),[]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
