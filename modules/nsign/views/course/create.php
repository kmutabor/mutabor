<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\NsignCourse */

$this->title = 'Добавить блюдо';
$this->params['breadcrumbs'][] = ['label' => 'Блюда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nsign-course-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'components'=>$components,
        'modelRecipe'=>$modelRecipe,
    ]) ?>

</div>
