<?php
use app\modules\nsign\models\NsignComponent;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
    <?php
    $session = Yii::$app->session;
    if($session->getFlash('msg')){
        echo Alert::widget([
            'options' => [
                'class' => 'alert-'.$session->getFlash('type'),
            ],
            'body' => $session->getFlash('msg'),
        ]);
        ;
    }
    ?>
</div>

    <?php
    $model = new NsignComponent();
    $model->id = $selectedItems;
    $activeComponents = $model->find()->where(['is_active'=>1])->all();
    $form = ActiveForm::begin();
    ?>

<?= $form->field($model,'id')->checkboxList(ArrayHelper::map($activeComponents,'id','name'))->label(false);?>

<div class="form-group">
        <?= Html::submitButton("Найти блюда",
            [
                'class'=>'btn btn-success',
                'onClick'=>'checkSelectedCount();',
            ]);
        ?>
    </div>
    <?php ActiveForm::end(); ?>

    <div><?php echo $result;?></div>
