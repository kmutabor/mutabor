<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блюда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nsign-course-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить блюдо', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
//                'template' => '{view} {update} {delete}',
//                'buttons' => [
//                    'view' => function ($url,$model) {
//                        return Html::a(
//                            '<span class="glyphicon glyphicon-eye-open"></span>',
//                            [
//                                $url,
//                                'id'=>$model->id,
//                            ]
//                        );
//                    },
//                    'update' => function ($url,$model) {
//                        return Html::a(
//                            '<span class="glyphicon glyphicon-pencil"></span>',
//                            [
//                                Url::to(['/nsign/recipe/update']),
//                                'id'=>$model->id,
//                            ]
//                        );
//                    },
//                    'delete',
//                ],
            ],

        ],
    ]); ?>

</div>
