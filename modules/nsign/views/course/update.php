<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\NsignCourse */

$this->title = 'Редактировать блюдо: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блюда', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="nsign-course-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
        'components'=>$components,
        'recipeModel'=>$recipeModel,
    ]) ?>

</div>
