<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\NsignComponent */

$this->title = 'Активировать/Деактивировать ингридиенты';
$this->params['breadcrumbs'][] = ['label' => 'Ингридиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nsign-component-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_activate', []) ?>

</div>
