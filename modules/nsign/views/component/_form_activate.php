<?php
use app\modules\nsign\models\NsignComponent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="nsign-component-form">

    <?php
    $model = new NsignComponent();
    $components = $model->find()->all();
    $activeComponents = $model->find()->where(['is_active'=>1])->all();
    $model->id = ArrayHelper::getColumn($activeComponents, 'id');
    ?>
<?= Html::activeCheckboxList($model, 'id', ArrayHelper::map($components,'id','name'), []) ?>

</div>
<script>
    var url = "<?php echo Url::to(['activate']);?>";
    $(':checkbox').on('change',function(elem){
        var checked = $(this).prop('checked');
        $.ajax({
            url: url,
            type: 'get',
            data: {
                id: this.value,
                status: checked?1:0,
                _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {},
            error: function (data) {
                alert("ПРоизошла ошибка. Смотри логи.")
            }
        });
    });
</script>