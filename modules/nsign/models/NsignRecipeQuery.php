<?php

namespace app\modules\nsign\models;

/**
 * This is the ActiveQuery class for [[NsignRecipe]].
 *
 * @see NsignRecipe
 */
class NsignRecipeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function byCourseId($id)
    {
        return $this->andWhere(['course_id' => $id])->all();
    }

    /**
     * @inheritdoc
     * @return NsignRecipe[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NsignRecipe|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}