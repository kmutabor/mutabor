<?php

namespace app\modules\nsign\models;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[NsignCourse]].
 *
 * @see NsignCourse
 */
class NsignCourseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NsignCourse[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NsignCourse|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byNsingComponents($selectedItems){
        $receiptsData = NsignCourse::find()
            ->joinWith(
                [
                    'nsignRecipes.component' => function ($query) {
                        $query->where('nsign_component.is_active != 0');
                    }
                ]
            )
            ->andWhere(['nsign_recipe.component_id'=>$selectedItems])
            ->asArray()
            ->orderBy('id')
            ->batch(20)
//    ->all()
        ;
        $conditionOneResultArray = [];
        $conditionTwoResultArray = [];
        $conditionThreeResultArray = [];
        foreach($receiptsData as $recipeElement){
            foreach($recipeElement as $elem){
                $component_not_actual = false;// Если компонент null, то есть не атуален(Отключен), то пропускаем запист о блюде.
                foreach($elem['nsignRecipes'] as $recipe){
                    if($recipe['component']==null){
                        $component_not_actual = true;
                        break;
                    }
                }
                if($component_not_actual){continue;}
                //Массив для ключей компонентов. Будет сравниваться с массивом присланных id компонентов
                $componentIdsArray = ArrayHelper::getColumn($elem['nsignRecipes'],'component_id');
                //счетчики компонентов для проверки количества на совпадение с количесвом присланных компонентов
                $selectedItemsCount = count($selectedItems);
                $componentIdsArrayCount = count($componentIdsArray);
                if($componentIdsArrayCount==1){continue;}
                //Разница меджду присланным и найденным массивом
                $componentsDiff1 = array_diff($selectedItems, $componentIdsArray);
                //Разница меджду найденным и присланным массивом
                $componentsDiff2= array_diff($componentIdsArray,$selectedItems);
                //Если количество присланных элементов равно количеству найденных ингредиентов, то
                if($selectedItemsCount == 5 and $selectedItemsCount == $componentIdsArrayCount){
                    if(empty($componentsDiff1) and  empty($componentsDiff2)){
                        $conditionOneResultArray[]=$this->convertCourseToArray($elem);
                        continue;
                    }
                }
//                if($selectedItemsCount > 1 and $selectedItemsCount < 6 and $componentIdsArrayCount > 1 and $componentIdsArrayCount < 5){
                if(isset($conditionOneResultArray) or $selectedItemsCount < 6 ){
                    //Общие жлементы массива
                    $componentsIntersect = array_intersect($selectedItems, $componentIdsArray);
                    if(count($componentsIntersect) > 1 and count($componentsIntersect) < 5 ){
                        $conditionTwoResultArray[]=$this->convertCourseToArray($elem);
                        continue;
                    }
                    if(count($componentsIntersect) == 1){
                        $conditionThreeResultArray[]=$this->convertCourseToArray($elem);
                        continue;
                    }
                }

            }
        }
        $returnArray = [
            'all'=>$conditionOneResultArray,
            'part'=>$conditionTwoResultArray,
            'min'=>$conditionThreeResultArray,
        ];
        return $returnArray;

    }

    private function convertCourseToArray($courseElem)
    {
        $returnArr = [];
        foreach ($courseElem['nsignRecipes'] as $r) {
            $returnArr[$courseElem['name']][] = $r['component']['name'];
        }
        return $returnArr;
    }


}