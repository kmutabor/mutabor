<?php

namespace app\modules\nsign\models;

/**
 * This is the model class for table "nsign_recipe".
 *
 * @property integer $course_id
 * @property integer $component_id
 *
 * @property NsignComponent $component
 * @property NsignCourse $course
 */
class NsignRecipe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nsign_recipe';
    }

    /**
     * @inheritdoc
     * @return NsignRecipeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NsignRecipeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'component_id'], 'required'],
            [['course_id', 'component_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course ID',
            'component_id' => 'Component ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(NsignComponent::className(), ['id' => 'component_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(NsignCourse::className(), ['id' => 'course_id']);
    }

}
