<?php

namespace app\modules\nsign\models;

/**
 * This is the model class for table "nsign_component".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_active
 *
 * @property NsignRecipe[] $nsignRecipes
 */
class NsignComponent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nsign_component';
    }

    public static function find()
    {
        return new NsignComponentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['is_active'], 'integer'],
            ['is_active', 'default', 'value' => 1],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNsignRecipes()
    {
        return $this->hasMany(NsignRecipe::className(), ['component_id' => 'id']);
    }
}
