<?php

namespace app\modules\nsign\models;

/**
 * This is the model class for table "nsign_course".
 *
 * @property integer $id
 * @property string $name
 *
 * @property NsignRecipe[] $nsignRecipes
 */
class NsignCourse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nsign_course';
    }

    /**
     * @inheritdoc
     * @return NsignCourseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NsignCourseQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNsignRecipes()
    {
        return $this->hasMany(NsignRecipe::className(), ['course_id' => 'id']);
    }




}
