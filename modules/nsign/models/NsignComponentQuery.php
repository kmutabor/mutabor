<?php

namespace app\modules\nsign\models;

/**
 * This is the ActiveQuery class for [[NsignComponent]].
 *
 * @see NsignComponent
 */
class NsignComponentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NsignComponent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NsignComponent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}