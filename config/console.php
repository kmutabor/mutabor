<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'flushInterval' => 1,
            'traceLevel' => YII_DEBUG ? 10 : 0,
            'targets' => [
               [
                   'class' => 'yii\log\FileTarget',
                   'exportInterval' => 1,
                   'levels'=>['error','info','trace',],
                   'logVars' => [],
                   'categories' => ['ratio'],
                   'logFile' => '@app/runtime/logs/appConsole.log',
                   'maxFileSize' => 1024 * 2,
                   'maxLogFiles' => 20,
               ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
