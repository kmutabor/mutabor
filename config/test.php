<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\jui\JuiAsset' => [
                    'css' => [
//                        'themes/flick/jquery-ui.css',
                        'themes/south-street/jquery-ui.css',
//                        'themes/mint-choc/jquery-ui.css',
                    ],
                ],
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
//            'enableStrictParsing' => true,
            'rules' => [
                'receipts'=>'nsign',
                'change-password'=>'user/change-password',
                'receipts/ffffuuuu'=>'nsign/course/filt',
                'receipts/ffffuuuu/do'=>'nsign/course/filt-do',
                'site/migrate-up'=>'site/migrate-up',
                'receipts/<controller>'=>'nsign/<controller>',
                'receipts/<controller>/<action>'=>'nsign/<controller>/<action>',
                [
                    'pattern'=>'<controller>/<action>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'.html',
                ],
                [
                    'pattern'=>'<controller>/<action>/<id:\d+>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'',
                ],
                [
                    'pattern'=>'<module>/<controller>/<action>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'.html',
                ],
                [
                    'pattern'=>'<module>/<controller>/<action>/<id:\d+>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'',
                ],

            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'oRjcx0SkpfJxpk9xl5mv9a9Rw2rKF4b3',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 10 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning',],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'nsign' => [
            'class' => 'app\modules\nsign\Nsign',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
