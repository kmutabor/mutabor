<?php

$suffixes = [
    'BIOHAZARD'=> '☣',
    'SMILE' => '☻',
    'SUN' => '☀',
    'RADIOACTIVE' => '☢',
    'USSR' => '☭',
    'PEACE' => '☮',
    'DAO' => '☯',
    'SKULL_AND_CROSSBONES' => '☠',
    'CADUCEUS' => '☤',
    'WHEEL_OF_DHARMA' => '☸',
    'SATURN' => '♄',
    'JUPITER' => '♃',
    'NEPTUNE' => '♆',
    'URANUS' => '♅',
    'PLUTO' => '♇',
    'ARIES' => '♈',
    'TAURUS' => '♉',
    'GEMINI' => '♊',
    'CANCER' => '♋',
    'LEO' => '♌',
    'VIRGO' => '♍',
    'LIBRA' => '♎',
    'SCORPIUS' => '♏',
    'SAGITTARIUS' => '♐',
    'CAPRICORN'=> '♑',
    'AQUARIUS' => '♒',
    'PISCES' => '♓',
];

function suffix($suffixes, $key){
    return '('.$suffixes[$key].')';
}

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\jui\JuiAsset' => [
                    'css' => [
//                        'themes/flick/jquery-ui.css',
                        'themes/south-street/jquery-ui.css',
//                        'themes/mint-choc/jquery-ui.css',
                    ],
                ],
            ]
        ],
        'urlManager' => [
            'suffix'=> suffix($suffixes, 'WHEEL_OF_DHARMA'),
            'enablePrettyUrl' => true,
            'showScriptName' => false,
//            'enableStrictParsing' => true,
            'rules' => [
                'change-password'=>'user/change-password',
                'contacts'=>'site/contact',
                'about'=>'site/about',
                'login'=>'site/login',
                'exit'=>'site/logout',
                'registration'=>'user/registration',
                [
                    'pattern'=>'kidala',
                    'route'=>'site/kidala',
                    'suffix'=>suffix($suffixes, 'TAURUS'),
                ],
                [
                    'pattern'=>'fwgnl',
                    'route'=>'site/fwgnl',
                    'suffix'=>suffix($suffixes, 'PISCES'),
                ],
                [
                    'pattern'=>'hist',
                    'route'=>'site/menschheit',
                    'suffix'=>suffix($suffixes, 'PEACE'),
                ],
                [
                    'pattern'=>'space',
                    'route'=>'site/player',
                    'suffix'=>suffix($suffixes, 'SUN'),
                ],
                [
                    'pattern'=>'puzzle',
                    'route'=>'site/puzzle',
                    'suffix'=>suffix($suffixes, 'URANUS'),
                ],
                [
                    'pattern'=>'bug',
                    'route'=>'site/bug',
                    'suffix'=>suffix($suffixes, 'SKULL_AND_CROSSBONES'),
                ],
                [
                    'pattern'=>'haos',
                    'route'=>'site/only-random',
                    'suffix'=>suffix($suffixes, 'RADIOACTIVE'),
                ],
                [
                    'pattern' => 'la',
                    'route' => 'site/random',
                    'defaults' => array('id' => 'latin_aphorisms'),
                    'suffix'=>suffix($suffixes, 'LEO'),
                ],
                [
                    'pattern'=>'<id:[\w_\/-]+>',
//                    'pattern'=>'sein/<id:[\w_\/-]+>',
                    'route'=>'site/random',
                    'suffix'=>suffix($suffixes, 'SMILE'),
                ],

                [
                    'pattern'=>'logic/<id:[\w_\/-]+>',
                    'route'=>'logic/uchebnik-glava',
                    'suffix'=>suffix($suffixes, 'BIOHAZARD'),
                ],
                [
                    'pattern'=>'logic',
                    'route'=>'logic/uchebnik',
                    'suffix'=>suffix($suffixes, 'PEACE'),
                ],
                [
                    'pattern'=>'spor',
                    'route'=>'logic/spor',
                    'suffix'=>suffix($suffixes, 'LIBRA'),
                ],
                [
                    'pattern'=>'spor/<id:[\w_\/-]+>',
                    'route'=>'logic/spor-glava',
                    'suffix'=>suffix($suffixes, 'DAO'),
                ],
                [
                    'pattern'=>'sillogism',
                    'route'=>'logic/sillogism',
                    'suffix'=>suffix($suffixes, 'CADUCEUS'),
                ],

                [
                    'pattern'=>'<controller>/<action>',
                    'route'=>'<controller>/<action>',
                ],
                [
                    'pattern'=>'<controller>/<action>/<id:\d+>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'',
                ],
                [
                    'pattern'=>'<module>/<controller>/<action>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'',
                ],
                [
                    'pattern'=>'<module>/<controller>/<action>/<id:\d+>',
                    'route'=>'<controller>/<action>',
                    'suffix'=>'',
                ],

            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'oRjcx0SkpfJxpk9xl5mv9a9Rw2rKF4b3',
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'useMemcached' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 10 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace',],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
//    'modules' => [
//        'nsign' => [
//            'class' => 'app\modules\nsign\Nsign',
//        ],
//    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
