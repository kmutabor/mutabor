<?php

use yii\db\Schema;
use yii\db\Migration;

class m160616_010446_ratio_test extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand('SET NAMES utf8')->execute();
        $this->createTable('ratio_task', [
            'id' => $this->primaryKey(11),
            'account_id' => $this->integer(11)->defaultValue(null),
            'created' => $this->dateTime()->defaultValue(null),
            'deffer' => $this->dateTime()->defaultValue(null),
            'type' => $this->smallInteger(2)->defaultValue(null),
            'task' => $this->string(45)->defaultValue(null),
            'action' => $this->string(45)->defaultValue(null),
            'data' => $this->text()->defaultValue(null),
            'status' => $this->smallInteger(2)->defaultValue(null),
            'retries' => $this->smallInteger(2)->defaultValue(null),
            'finished' => $this->dateTime()->defaultValue(null),
            'result' => $this->text(),
        ]);
        $this->createIndex('status', 'ratio_task', 'status');
        $this->createIndex('deffer', 'ratio_task', 'deffer');

        $sql = "INSERT INTO `ratio_task` (`id`, `account_id`, `created`, `deffer`, `type`, `task`, `action`, `data`, `status`, `retries`, `finished`, `result`) VALUES
    (2971220, 70748,'2016­02­14 13:09:15', NULL, NULL, 'integration', 'process', '{\"integration_id\":3312,\"lead_id\":\"2999670\"}', 0, 0, NULL, NULL),
(2971206, 80034,'2016­02­14 13:08:16', NULL, NULL, 'message', 'sms', '{\"number\":\"89111111119\",\"message\":\"Заявка с ru.ru\\nвячеслав \\n\"}', 0, 0, NULL, NULL),
(2971187, 81259,'2016­02­14 13:06:42', NULL, NULL, 'account', 'bill', '{\"bill_id\":\"82029\"}',0, 0, NULL, NULL),
(2971123, 9608, '2016­02­14 13:01:58', NULL, NULL, 'integration', 'process', '{\"integration_id\":2845,\"lead_id\":\"2999571\"}', 0, 0, NULL, NULL),
(2971122, 9608, '2016­02­14 13:01:53', NULL, NULL, 'integration', 'process', '{\"integration_id\":2987,\"lead_id\":\"2999570\"}', 0, 0, NULL, NULL),
(2971107, 83992,'2016­02­14 13:01:03', NULL, NULL, 'domain', 'addzone', '{\"domain\":\"mydomain.ru\"}', 0, 0, NULL, NULL);";
        $this->execute($sql);
        echo "done.\n";
    }

    public function down()
    {
        echo "m160616_010446_ratio_task reverted.\n";
        $this->dropTable('ratio_task');
        echo "done.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
