<?php

use yii\db\Migration;

class m160612_005703_nsing_init extends Migration
{
    public function up()
    {
        $this->createTable('nsign_component', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(50)->notNull()->unique(),
            'is_active' => $this->smallInteger(1)->notNull()->defaultValue(1),
        ]);

        $this->createTable('nsign_course', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(50)->notNull()->unique(),
            'is_active' => $this->smallInteger(1)->notNull()->defaultValue(1),
        ]);

        $this->createTable('nsign_recipe', [
            'course_id' => $this->integer(11)->notNull(),
            'component_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex('component_id', 'nsign_recipe', 'component_id');
        $this->createIndex('course_id', 'nsign_recipe', 'course_id');

        $this->addForeignKey('component_id', 'nsign_recipe', 'component_id', 'nsign_component', 'id', 'CASCADE');
        $this->addForeignKey('course_id', 'nsign_recipe', 'course_id', 'nsign_course', 'id', 'CASCADE');

        $sql = "INSERT INTO `nsign_component` (`id`, `name`, `is_active`) VALUES
    (1, 'Соль', 1),
	(2, 'Перец', 0),
	(3, 'Вода', 1),
	(4, 'Укроп', 0),
	(5, 'Петрушка', 1),
	(6, 'Телятина', 1),
	(7, 'Говядина', 1),
	(8, 'Сайра', 1),
	(9, 'Капуста', 1),
	(10, 'Свекла', 1),
	(11, 'Сахар', 1),
	(12, 'Грибы', 1),
	(13, 'Горошек', 1),
	(14, 'Фасоль', 0),
	(15, 'Лук', 1),
	(16, 'Чеснок', 1),
	(17, 'Картофель', 1),
	(18, 'Морковь', 1),
	(19, 'Хлеб', 1),
	(20, 'Масло', 0),
	(21, 'Колбаса', 1),
	(22, 'Сыр', 1),
	(23, 'Молоко', 0),
	(24, 'Мак', 0);";
        $this->execute($sql);


   $sql = "INSERT INTO `nsign_course` (`id`, `name`, `is_active`) VALUES
    (4, 'Борщ', 1),
	(5, 'Щи', 1),
	(6, 'Бульон диетический', 1),
	(7, 'Гороховый суп', 1),
	(8, 'Грибной суп', 1),
	(9, 'Рыбный суп', 1),
	(10, 'Бутерброды с колбасой', 1),
	(11, 'Бутерброды с маслом', 1),
	(12, 'Булочка с маком', 1),
	(13, 'Состоит ровно из пяти ингредиентов', 1);";
        $this->execute($sql);


        $sql = "INSERT INTO `nsign_recipe` (`course_id`, `component_id`) VALUES
    (4, 1),
	(4, 2),
	(4, 3),
	(4, 4),
	(4, 5),
	(4, 7),
	(4, 9),
	(4, 10),
	(4, 15),
	(4, 16),
	(4, 17),
	(4, 18),
	(5, 1),
	(5, 2),
	(5, 3),
	(5, 6),
	(5, 9),
	(5, 11),
	(5, 15),
	(5, 16),
	(5, 17),
	(6, 1),
	(6, 3),
	(6, 15),
	(6, 16),
	(6, 18),
	(7, 1),
	(7, 2),
	(7, 3),
	(7, 4),
	(7, 5),
	(7, 13),
	(7, 15),
	(7, 16),
	(8, 1),
	(8, 3),
	(8, 5),
	(8, 12),
	(8, 15),
	(8, 16),
	(8, 17),
	(9, 1),
	(9, 2),
	(9, 3),
	(9, 8),
	(9, 15),
	(9, 17),
	(9, 18),
	(10, 19),
	(10, 21),
	(11, 1),
	(11, 19),
	(11, 20),
	(12, 19),
	(12, 24),
	(13, 1),
	(13, 2),
	(13, 3),
	(13, 4),
	(13, 5);";

        $this->execute($sql);
        echo "done.\n";
    }

    public function down()
    {
        echo "m160612_005703_nsing_init reverted.\n";
        $this->dropTable('nsign_recipe');
        $this->dropTable('nsign_component');
        $this->dropTable('nsign_course');
//        return false; // if not realized
        echo "done.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
